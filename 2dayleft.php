<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
include("./Globals.php");

$query = 'SELECT id_vquestion FROM voice_question WHERE DATEDIFF(deadline,CURRENT_TIMESTAMP)<3 AND DATEDIFF(deadline,CURRENT_TIMESTAMP)>-1 LIMIT 1';
$sth = $pdo->prepare($query);
$sth->execute();
$vquestion=$sth->fetch(PDO::FETCH_ASSOC);
if (isset($vquestion['id_vquestion'])) {
	include("./Notification.php");
}