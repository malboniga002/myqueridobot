<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
require __DIR__ . '/vendor/autoload.php';

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Language;

include("./Globals.php");

try{
$telegram = new Telegram($API_KEY, $BOT_NAME);

$query = 'SELECT student.*,student_group.id_prof FROM student,student_group WHERE student_group.id_group=student.id_group;';
$sth = $pdo->prepare($query);
$sth->execute();
$student=$sth->fetchAll(PDO::FETCH_ASSOC);
$i=0;
//for all stuent
while(isset($student[$i])){
	$message = '';
	$data['chat_id'] = $student[$i]['id_student'];
	$query = 'SELECT language FROM professor WHERE id_prof='.$student[$i]['id_prof'].';';
	$sth = $pdo->prepare($query);
	$sth->execute();
	$language = $sth->fetch(PDO::FETCH_ASSOC);
	$messages = Language::getNotificationMessages($language['language']);
	$j = 0;
	$flag = FALSE;
	$query = 'SELECT * FROM group_question;';
	$sth = $pdo->prepare($query);
	$sth->execute();
	$questions = $sth->fetchAll(PDO::FETCH_ASSOC);
	//search if the student has any voice question to answer
	while (isset($questions[$j])  && !$flag){
		$query = 'SELECT deadline FROM voice_question WHERE id_vquestion='.$questions[$j]['id_vquestion'].' AND deadline>=CURRENT_TIMESTAMP;';
		$sth = $pdo->prepare($query);
		$sth->execute();
        $question = $sth->fetch(PDO::FETCH_ASSOC);
        //first it checks if theres any question that the deadline hasn't pass
        if(isset($question['deadline'])){
            //here it checks that the student hasn't answer the question
            $query = 'SELECT selected FROM voice_answer WHERE id_vquestion='.$questions[$j]['id_vquestion'].' AND id_student='.$student[$i]['id_student'].' LIMIT 1;';
			$sth = $pdo->prepare($query);
			$sth->execute();
        	$num_answers = $sth->fetch(PDO::FETCH_ASSOC);
            if(!isset($num_answers['selected'])){
            	// if he/she has a question a message is stored in a variable to sen it later
               	$message .= $messages['voice'];
               	$flag = TRUE;
            }
       	}
        $j++;
    }

    if($message != ''){
    	$message .= "\n";
    }

	$j = 0;
	$flag = FALSE;
	$query = 'SELECT group_evaluation.*,`voice_question`.vquestion FROM group_evaluation,`voice_question` WHERE `group_evaluation`.id_student_evaluator='.$student[$i]['id_student'].' AND `group_evaluation`.id_vquestion=`voice_question`.id_vquestion AND `voice_question`.deadline<CURRENT_TIMESTAMP;';
	$sth = $pdo->prepare($query);
	$sth->execute();
	$questions = $sth->fetchAll(PDO::FETCH_ASSOC);
	//here it search if the student has any evaluation to do
    while (isset($questions[$j])  && !$flag){
        //first it checks if the other student who is going to be evaluated has answer the question to evaluate him/her
        $query = 'SELECT selected FROM voice_answer WHERE id_vquestion='.$questions[$j]['id_vquestion'].' AND id_student='.$questions[$j]['id_student_evaluated'].' LIMIT 1;';
		$sth = $pdo->prepare($query);
		$sth->execute();
        $num_answers = $sth->fetch(PDO::FETCH_ASSOC);
        if (isset($num_answers['selected'])) {
            //then it checks that the student hasn't done the evaluation
            $query = 'SELECT voice_grades.grade FROM voice_grades WHERE voice_grades.id_eval='.$questions[$j]['id_eval'].' LIMIT 1;';
			$sth = $pdo->prepare($query);
			$sth->execute();
        	$grade = $sth->fetch(PDO::FETCH_ASSOC);
            if(!isset($grade['grade'])){
            	//if the student has an evaluation to do it will send him/her a message
                $message .= $messages['eval'];
               	$flag = TRUE;
            }
        }
        $j++;
    }
    //if theres a message to send it sends it
	if($message != ''){
		$data['text'] = $message;
		$ch = curl_init("https://api.telegram.org/bot".$API_KEY.'/sendMessage');
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
	}
$i++;
}
$pdo = null;
}catch(Exception $e){
	print_r($e->getMessage());
}
