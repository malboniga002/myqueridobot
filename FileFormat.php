<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
require __DIR__ . '/vendor/autoload.php';

use FFMpeg\FFMpeg;
use FFMpeg\Format\Audio\Mp3;

include("./Globals.php");
//get all the oga files from the given answers
$query = 'SELECT student.id_group,voice_answer.* FROM student,voice_answer WHERE student.id_student=voice_answer.id_student AND voice_answer.change_to_mp3_at IS NULL;';
$sth = $pdo->prepare($query);
$sth->execute();
$student=$sth->fetchAll(PDO::FETCH_ASSOC);
$i=0;

//while theres a oga file
while ( isset($student[$i])) {

	//this part makes the conversion of the .oga file to .ogg
	// create a ffmpeg instance to convert the .oga files into .ogg files
	$ffmpeg = FFMpeg::create(array(
	    'ffmpeg.binaries'  => $ffmpegbin,
	    'ffprobe.binaries' => $ffprobebin,
	    'timeout'          => 3600, // The timeout for the underlying process
	    'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
	));
	$audio = $ffmpeg->open($student[$i]['file_path']);
	//the .ogg files have Vorbis codification and have 2 audio channels
	$format = new Mp3();
	$format-> setAudioChannels(2);//
	//here the folders are created with 755 permisons if not exists
	if (!file_exists('./voice_answers')) {
		mkdir('./voice_answers');
	}
	if (!file_exists('./voice_answers/'.$student[$i]['id_group'])) {
		mkdir('./voice_answers/'.$student[$i]['id_group']);
	}
	if (!file_exists('./voice_answers/'.$student[$i]['id_group'].'/'.$student[$i]['id_student'])) {
		mkdir('./voice_answers/'.$student[$i]['id_group'].'/'.$student[$i]['id_student']);
	}
	//the next line makes the conversion and save the file in the folder specified in the second parameter
	$file_location='./voice_answers/'.$student[$i]['id_group'].'/'.$student[$i]['id_student'].'/'.$student[$i]['id_file'].".mp3";
	//save the .ogg file
	$audio->save($format, $file_location);
	//delete the .oga file
	//unlink($student[$i]['file_path']);
	//and save in the database the new file
	$query = 'UPDATE voice_answer SET file_path="'.$file_location.'",change_to_ogg_at=CURRENT_TIMESTAMP WHERE id_file='.$student[$i]['id_file'].';';
	$sth = $pdo->prepare($query);
	$sth->execute();
	$i++;
}

//get all the oga files from the given voice explanation at evaluations
$query = 'SELECT * FROM voice_evaluation';
$sth = $pdo->prepare($query);
$sth->execute();
$explain=$sth->fetchAll(PDO::FETCH_ASSOC);
$i=0;

//while theres a oga file
while ( isset($explain[$i])) {
	if(substr($explain[$i]['file_path'], -3)=="oga"){
		//this part makes the conversion of the .oga file to .ogg
		// create a ffmpeg instance to convert the .oga files into .ogg files
		$ffmpeg = FFMpeg::create(array(
		    'ffmpeg.binaries'  => $ffmpegbin,
		    'ffprobe.binaries' => $ffprobebin,
		    'timeout'          => 3600, // The timeout for the underlying process
		    'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
		));
		$audio = $ffmpeg->open($explain[$i]['file_path']);
		//the .ogg files have Vorbis codification and have 2 audio channels
		$format = new Mp3();
		$format-> setAudioChannels(2);//
		//here the folders are created with 755 permisons if not exists
		if (!file_exists('./voice_evaluation')) {
			mkdir('./voice_evaluation');
		}
		if (!file_exists('./voice_evaluation/'.$explain[$i]['id_student_evaluator'])) {
			mkdir('./voice_evaluation/'.$explain[$i]['id_student_evaluator']);
		}
		//the next line makes the conversion and save the file in the folder specified in the second parameter
		$file_location='./voice_evaluation/'.$explain[$i]['id_student_evaluator'].'/'.$explain[$i]['id_file'].".mp3";
		//save the .ogg file
		$audio->save($format, $file_location);
		//delete the .oga file
		//unlink($explain[$i]['file_path']);
		//and save in the database the new file
		$query = 'UPDATE voice_evaluation SET file_path="'.$file_location.'" WHERE file_path="'.$explain[$i]['file_path'].'"';
		$sth = $pdo->prepare($query);
		$sth->execute();
	}
	$i++;
}