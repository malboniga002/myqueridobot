<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Evaluations</title>
    <?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstableevaluation.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
  </head>
  <body>
  	<?php include("include/header.html") ?>
  	<div><center>
  		<div>
  			<h2>Evaluation</h2>
  		</div>
      <!--this datatable data is loaded from functionstableevaluation.js-->
  		<section>
  			<table id="tablevalprof">
	  			<thead>
	  				<tr>
              <th>ID</th>
	  					<th>Answer</th>
	  					<th>Question</th>
	  					<th>Creation date</th>
	  					<th>Number of evaluations</th>
              <th>Criterions to evaluate</th>
	  				</tr>
	  			</thead>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Answer</th>
              <th>Question</th>
              <th>Creation date</th>
              <th>Number of evaluations</th>
              <th>Criterions to evaluate</th>
            </tr>
        </tfoot>
	  		</table>
  		</section>
  	</center></div>
  </body>
</html>