<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
	<meta charset="utf-8">
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //if theres no gid parameter at the url this sends the user to professor_group.php
      if(!isset($_GET['gid'])){
        header('Location: professor_group.php');
      }
      echo '<script>var idg="'.$_GET['gid'].'";</script>';
      require_once "include/mydb.php";
      //this checks if the group was created by the user, if thats not the case it sends the user to professor_group.php
      $gname=getGroupName($_GET['gid'],$_SESSION['userid']);
      if (!isset($gname["group_name"])) {
        header('Location: professor_group.php');
      }
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablegroupstudents.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/optionsbuttonstable.js"></script>
</head>
<body>
<?php include("include/header.html") ?>
<div><center>
  <!--this is the name of the group-->
<h2>Group: <?php echo $gname['group_name'];?></h2><br>
<div>
  <input type="button" name="addstudent" value="Add students" onclick='addStudents()'>&nbsp;&nbsp;&nbsp;
  <input type="button" name="deletestudent" value="Delete students" onclick='deleteStudents()'>&nbsp;&nbsp;&nbsp;
  <input type="button" name="select" value="Select all" onclick='selectAll()'>&nbsp;&nbsp;&nbsp;
  <input type="button" name="deselect" value="Unselect all" onclick='deselectAll()'><br><br>
</div>
<section>
  <!--the data of this table is loaded from functionstablegroupstudents.js-->
  	<table id="tablegroup">
	  	<thead>
	  		<tr>
          <th>ID</th>
          <th>Student</th>
	  			<th>Questions to answer</th>
	  			<th>Answers to evaluate</th>
          <th>Selected</th>
	  		</tr>
	  	</thead>
	  </table>
</section>
</center></div>
</body>
</html>