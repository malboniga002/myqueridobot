<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
</head>
<body>
<script>
//this function closes this iframe
function no(){
parent.document.getElementById('optionChange').remove();
}
</script>
<?php
//if the session has not the user id it sends the user to the login
session_start(); 
if(!isset($_SESSION['userid'])){
    header('Location: index.php');
}
//if this page gets the data from the form at the bottom of this file
if((isset($_POST["enviado"])) && ($_POST["enviado"]== "form1")){	
	include_once "include/mydb.php";
  //it modifys the options of this professor at the database
	$change=updateOptions($_SESSION['userid'],$_POST['language'],$_POST['autoeval'],$_POST['random'],$_POST['maxeval'],$_POST['scale']);
  //if the modification was succesful it sends feedback to the user
  if ($change) {?>
    <div><center>
    <h2>Options changed</h2>
    <label>¡The modification was succesful!</label><br><br>
    <input type="button" value="Accept" onclick='no()'/> 
  </center></div>
  <?php }
  else{ 
    //if there was an error it tells it to the user
    ?>
    <div><center>
    <h2>Error</h2>
    <label>There was an error while the changes were made. Try saving the changes again please.</label><br><br>
    <input type="button" value="Accept" onclick='no()'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </center></div>
  <?php }
	
 }
else
{?>
<script>
//this function sends the data to this file after checking that the text input wasnt empty
function sendChanges(){
  if (parent.document.getElementById('maxeval').value!="") {
var form=document.getElementById("form1");
var newInput1 = document.createElement('input');
newInput1.type = 'hidden';
newInput1.name = 'language';
newInput1.value=parent.document.querySelector('input[name="language"]:checked').value;
var newInput2 = document.createElement('input');
newInput2.type = 'hidden';
newInput2.name = 'autoeval';
if (parent.document.getElementById('autoeval').checked) {
  newInput2.value='T';
}
else{
  newInput2.value='F';
}
var newInput3 = document.createElement('input');
newInput3.type = 'hidden';
newInput3.name = 'random';
if (parent.document.getElementById('random').checked) {
  newInput3.value='T';
}
else{
  newInput3.value='F';
}
var newInput4 = document.createElement('input');
newInput4.type = 'hidden';
newInput4.name = 'maxeval';
newInput4.value=parent.document.getElementById('maxeval').value;
var newInput5 = document.createElement('input');
newInput5.type = 'hidden';
newInput5.name = 'scale';
newInput5.value=parent.document.querySelector('input[name="scale"]:checked').value;
form.appendChild(newInput1);
form.appendChild(newInput2);
form.appendChild(newInput3);
form.appendChild(newInput4);
form.appendChild(newInput5);
form.submit();
}
//if the input was empty it tells it to the user
else{parent.alert('Empty text at maximum evaluations');}

}
</script>
<!--this form is a question with 2 buttons(yes/no)-->
<form action="options.php" method="post" enctype="multipart/form-data" id="form1">
  <div><center>
  	<h2>Change options</h2>
  	<label>¿Are you sure about making this changes in your options?</label><br><br>
    <input type="button" value="No" onclick='no()'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" name="button" id="button" onclick="sendChanges()" value="Yes" />
  </center></div>
  <input type="hidden" name="enviado" value="form1"/>
</form>
<?php }?>
</body>
</html>