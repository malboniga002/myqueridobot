<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      include_once "include/mydb.php";
      //after this  it calls to plotlys js librarys 
    ?>
    <title>Graphics</title>
	<!-- Latest compiled and minified plotly.js JavaScript -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.min.js"></script>

<!-- OR use a specific plotly.js release (e.g. version 1.5.0) -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-1.5.0.min.js"></script>

<!-- OR an un-minified version is also available -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.js"></script>
</head>
<body>
<center>
  <h2>Graphic of the quizz answers given by students during the last 12 months</h2>
<div id="myDiv"></div>
</center>
	<script>
  document.getElementById("myDiv").style.height=window.window.innerHeight-70;
  document.getElementById("myDiv").style.width=window.innerWidth-40;
  <?php
  //this php code prepares the data for plotly
  $i=11;
  //the data will be in js arrays. This makes strings of those arrays
  $x="[";
  $y="[";
  while($i>=0){
    //get the data from the database
    $date=getGraphic3Data($i);
    //conversion of the month and adding to the string
    if($date['month']=='12' || $date['month']=='0'){$x.='"December",';}
    if($date['month']=='11' || $date['month']=='-1'){$x.='"November",';}
    if($date['month']=='10' || $date['month']=='-2'){$x.='"October",';}
    if($date['month']=='9' || $date['month']=='-3'){$x.='"September",';}
    if($date['month']=='8' || $date['month']=='-4'){$x.='"August",';}
    if($date['month']=='7' || $date['month']=='-5'){$x.='"July",';}
    if($date['month']=='6' || $date['month']=='-6'){$x.='"June",';}
    if($date['month']=='5' || $date['month']=='-7'){$x.='"May",';}
    if($date['month']=='4' || $date['month']=='-8'){$x.='"April",';}
    if($date['month']=='3' || $date['month']=='-9'){$x.='"March",';}
    if($date['month']=='2' || $date['month']=='-10'){$x.='"February",';}
    if($date['month']=='1'){$x.='"January",';}
    //adding the data to the second string
    $y.=$date['count'].',';
    $i--;
  }
  //after that it will have a ',' at the end. With this that coma desapears and after that it adds the end of the array(']')
  $x=substr($x, 0,strlen($x)-1);
  $y=substr($y, 0,strlen($y)-1);
  $x.="]";
  $y.="]";
  //here are created the variables for plotly to make the graphic
  echo "var data = [{
    type: 'scatter',
    x: ".$x.",
    y: ".$y.",
    name: 'Monthly answers',
  }];

  ";

  ?>
//sets the type of layout
var layout = {
  width: window.innerWidth-40,
  height: window.window.innerHeight-70
};
//and creates the graphic and put it in the div with id='myDiv'
Plotly.newPlot('myDiv', data, layout);
</script>
</body>
</html>
