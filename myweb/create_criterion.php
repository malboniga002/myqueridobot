<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
</head>
<body>
<?php
//if theres no session it sends the user to the login
session_start(); 
if(!isset($_SESSION['userid'])){
    header('Location: index.php');
}
//if theres elements from the form at the bottom of this file
if((isset($_POST["enviado"])) && ($_POST["enviado"]== "form1")){	
	include_once "include/mydb.php";
	//insert the criterion on the database and it adds the criterion to the list of criterions, after that it removes the iframe of this window
	$crit=insertCriterion($_POST["crit"],$_SESSION['userid']);
	echo "<script>".
	"var option = document.createElement('option');".
	"option.text='".$crit['id_criterion'].".-".substr($_POST["crit"], 0,25)."';option.id='crit-".$crit['id_criterion']."';".
	"option.title='".$_POST["crit"]."';".
	"parent.document.getElementById('criterions').add(option);".
	"parent.multi(parent.select);".
	"parent.document.getElementById('createCrit').remove();</script>";}
else
{?>
<script>
//this method removes the iframe of this window
function cancel(){
parent.document.getElementById('createCrit').remove();
}

//this method checks the data and sends the information to this file to insert it in the database
function sendCrit(){
//if the user has write something in the text area it sends the data
if (document.getElementById("crit").value!="") {
var form=document.getElementById("form1");
form.submit();
}
//else it sends an alert telling the problem to the user
else{
parent.alert('You havent write any criterion');
}
}
</script>
<!-- this is the form in html-->
<form action="create_criterion.php" method="post" enctype="multipart/form-data" id="form1">
  <div><center>
  	<h2>Create criterion</h2>
  	<label>Criterion:</label><br>	
    <textarea cols=60 rows=5 id="crit" name="crit"></textarea><br><br>
    <input type="button" value="Cancel" onclick='cancel()'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" name="button" id="button" onclick="sendCrit()" value="Create criterion" />
  </center></div>
  <input type="hidden" name="enviado" value="form1"/>
</form>
<?php }?>
</body>
</html>