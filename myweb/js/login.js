/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this function checks that the user dont write some strange characters at the login,
// but it does not realy mind because the connection to the database is made with PDO
function logincheck(){
	var usuario = document.form_login.usuario.value;
	var pass = document.form_login.pass.value;
	var flag=true;
	for (var i = 0; i < pass.length; i++) {
		if(isNaN(pass[i])){
			flag=false;
		}
	}
	if (usuario.indexOf(";")>-1 || usuario.indexOf("'")>-1 || usuario.indexOf('"')>-1) {
		flag=false;
	}
	if(flag){
		document.form_login.submit();
	}
	else{
		window.alert("Wrong user or password.");
	}
}

//http://stackoverflow.com/questions/905222/enter-key-press-event-in-javascript
//when enter is pressed the this calls logincheck to submit the form with the user and password
function runScript(e) {
    if (e.keyCode == 13) {
        logincheck();
        return false;
    }
    else{
    	return true;
    }
}
