/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this method is called to set all the checkbox of the datatables to true
function selectAll(){
	i=0;
	while(document.getElementById(i)){
		document.getElementById(i).checked=true;
		i++;
	}
}

//this method is called to set all the checkbox of the datatables to false
function deselectAll(){
	i=0;
	while(document.getElementById(i)){
		document.getElementById(i).checked=false;
		i++;
	}
}