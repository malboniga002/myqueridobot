/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

// this method fills the datatable with the data returned from a json 
$(document).ready(function(){
	$.ajax({
		url: './include/processvquestiongroupprof.php',
		type: 'post',
		data: { tag: 'getData'},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				//if all was ok this fills all the rows adding them identifiers
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr id='group-"+record.Idgroup+"'> </>");
						$("<td id='id-"+i+"' class='id' onclick='checking("+i+")'>"+record.Idgroup+"</>").appendTo(row);
						$("<td class='id' title='"+record.Groupname.replace(/'/g,"\`")+"' onclick='checking("+i+")'> </>").text(record.Groupname.slice(0,50)+'...').appendTo(row);
						$("<td class='id' onclick='checking("+i+")'> </>").text(record.Members).appendTo(row);
						$("<td class='id' onclick='group("+record.Idgroup+")'> </>").text("Click here to check the group members").appendTo(row);
						$('<td class="id" onclick="checking('+i+')"> <input id="'+i+'" onclick="checking('+i+')" type="checkbox" /></>').appendTo(row);
						row.appendTo('#tablequestgroup');
						i=i+1;
					};
				});
				if (idvq) {
					loadgroup();
				}
			}

			//this are the options of the datatable
			dtable=$('#tablequestgroup').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				'columns': [ 
					null,
					null,
					null,
					null,
					{ "orderable": false }
				],
				"scrollY":        "300px",
        		"scrollCollapse": true,
        		"paging": false
			});

		}
	});
});

//this method opens an iframe with a datatable filled with the information of the students who are in the group with the id of the parameter
function group(idgroup){
	if (!document.getElementById("studentList")) {
	    var frame = document.createElement("IFRAME");
	    frame.style.borderColor="#000000";
	    frame.style.background="#ffffff";
	    frame.id="studentList";
	    frame.style.position="absolute";
	    frame.style.width='650px';
	    frame.style.height='620px';
	    frame.style.top=(window.innerHeight)+"px";
	    frame.style.left=(window.innerWidth-650)/2+"px";
	    frame.src="professor_vquestion_group.php?gid="+idgroup;
	    document.body.appendChild(frame);
	}
	else{
		alert("Close the other evaluator list to see this list please");
	}
}

//this method checks or unchecks the checkbox of the row. It depends of the state of the checkbox
function checking(id){
	if (document.getElementById(id).checked) {
		document.getElementById(id).checked=false;
	}
	else{
		document.getElementById(id).checked=true;
	}
}