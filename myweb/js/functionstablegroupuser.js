/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this fills the datatable with the data returned in a json
$(document).ready(function(){
	$.ajax({
		url: './include/processgroupsuserprof.php',
		type: 'post',
		data: { tag: 'getData'},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				//if all was ok this fills the rows of the datatable
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr id='stud-"+record.Idstudent+"'></>");
						$("<td id='id-"+i+"' class='id' onclick='checking("+i+")'>"+record.Iduser+"</>").appendTo(row);
						$("<td class='id' id='first-"+i+"' onclick='checking("+i+")'></>").text(record.Userfirstname).appendTo(row);
						$("<td class='id' id='last-"+i+"' onclick='checking("+i+")'></>").text(record.Userlastname).appendTo(row);
						$('<td class="id" onclick="checking('+i+')"> <input onclick="checking('+i+')" id="'+i+'" type="checkbox" /></>').appendTo(row);
						row.appendTo('#tablegroupuserprof');
						i=i+1;
					};
				});
			}

			//this are the options of the datatable
			dtable=$('#tablegroupuserprof').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				'columns': [ 
					null,
					null,
					null,
					{ "orderable": false }
				],
				"scrollY":        (screen.height/2)-25+'px',
        		"scrollCollapse": true,
        		"paging": false
			});


		}
	});
	
});

//this method removes the iframe
function cancelChanges(){
	parent.document.getElementById('userList').remove();
}

//this method sends the data of the iframe to save it in the database
function saveChanges(){
	//first it deletes the searches done at the table to make all the rows of the table appear
	document.getElementById("tablegroupuserprof_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablegroupuserprof_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
	var theForm, newInput1;
	theForm = document.createElement('form');
	theForm.action = "professor_group_user.php?idg="+idg;
	theForm.method = 'post';
	var ids="";
	var i=0;
	//this creates a string with the ids of the users separated with comas to make an array then to get the data easily
	while(document.getElementById(i)){
		if (document.getElementById(i).checked) {
			ids=ids+document.getElementById("id-"+i).innerHTML+",";
		}
		i++;
	}
	if (ids[1]) {
		ids=ids.slice(0,ids.length-1);
	}
	// Next create the <input>s in the form and give them names and values
	newInput1 = document.createElement('input');
	newInput1.type = 'hidden';
	newInput1.name = 'ids';
	newInput1.value = ids;
	theForm.appendChild(newInput1);
	// ...and it to the DOM...
	document.getElementById('hidden_form_container').appendChild(theForm);
	// ...and submit it
	theForm.submit();
}

//this method checks or unchecks the checkbox of one row depending of its state
function checking(id){
	if (document.getElementById(id).checked) {
		document.getElementById(id).checked=false;
	}
	else{
		document.getElementById(id).checked=true;
	}
}