/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//*******IMPORTANT: WHEN IM SAYING STUDENT DURING THE EXPLANATIONS IM REFERRING TO THE STUDENTS TELEGRAM ID********
//this variable contains the datatable
var dtable;
//this variable contains the list of lists of students who have to answer the question and his/her evaluators
var students;
//this variable contains a list with the eveluations to make per student in this question(to control the maximum number of evaluations)
var evaluators;
//this variable contains a list with the probability of an student to answer the question. To equilibrate the number of questions per student
var studentrandom=[];
//this two limits are for the limit of calls(in recursive method) the methods evalRandomSt and ansRandomSt
var limiteval=0;
var limitrand=0;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this method is to select random evaluators for the student at the parameter of the method 
function evalRandomSt(student){
	var j=0;
	var r;
	var std;
	r=0;
	//first, this search the student in the list of lists, the first student of the list of lists is the student who has to answer the questio
	//(but thats only if that list contains more than one student, the first student is who answers the question and the following students in that list are his/her evaluators)
	while(students[r][0]!=student){
		r++;
	}
	//while theres a student in the list of the table
	while(document.getElementById(j)){
		//get the student and if he has no evaluation to do it initialize his evaluations 
		std=document.getElementById("id-"+j).innerHTML;
		if(!evaluators[std]){
			evaluators[std]=0;
		}
		//if the student hasnt reach the maximum of evaluations for this question
		if(evaluators[std]<maxeval){
			//if the user has not the autoevaluation option selected 
			if(autoeval=='F'){
				//first it checks that the evaluator and student at the parameters of this method arent the same
				if(std!=student){
					//this is the random selection, if the student is selected it adds an evalutor to the student at the parameters 
					//and it adds an evaluation to the evaluator 
					if (Math.random()<0.5) {
						evaluators[std]++;
						students[r].push(std);
					}
				}
			}
			//if the autoevaluation isnt selected it does the same but doesnt check anything
			else{
				if (Math.random()<0.5) {
					evaluators[std]++;
					students[r].push(std);
				}
			}
		}
		j++;
	}
	//if the random method doesn't select any evaluator for the student(all the Math.random>0.5) this method becomes recursive
	//but it has a limit of calls to control the stack overflow of calls to a method
	if(limiteval<6){
		if (!students[r][1]) {
			limiteval++;
			return evalRandomSt(student);
		}
		else{
			limiteval=0;
			return true;
		}
	}
	else{
		limiteval=0;
		if (students[r][1]) {
			return true;
		}
		else{
			return false;
		}
	}
}

//this method is the random selector of student to answer the question
function ansRandomSt(){
	deselectAll();
	var y=0;
	var std;
	students=[];
	evaluators=[];
	flag=true;
	//while theres a student in the datatable
	while(document.getElementById(y)){
		//adds them to the students to a new array in the array of students
		std=document.getElementById("id-"+y).innerHTML;
		students[y]=[];
		students[y].push(std);
		//this is the random selector. As i said at the top of this file the studentrandom is the variable which contains the probabilities of the students to be selected for answering the question
		//The calculations of the probabilities are explained later
		if(Math.random()>studentrandom[std]){
			//if the student is selected this checks if he/she has evaluators. If theres no evaluator for him/her it doesn't select it to answer the question.
			//this is to control the case of every student have reach the maximum number of evaluations(if theres no evaluator left for this student it doesn't select it to answer the question).
			if(evalRandomSt(std)){
				flag=false;
				document.getElementById(y).checked=true;
			}
		}
		y++;
	}
	//if it doesn't select any student to answer the question this method becomes recursive, but thats only if this is a new question(if the user is modifying it it doesn't become recursive)
	if (idvq<1) {
		//if it hasn't reach the maximum number of recursive calls it can call himself again else it ends
		if(limitrand<6){
			if (flag) {
				limitrand++;
				ansRandomSt();
			}
			else{
				limitrand=0;
			}
		}
		else{
			limitrand=0;
		}
	}
}

//this function is called when the "Random selection" button is pressed
function btRandom(){
	//while theres a student in the table it checks if he/she is selected to answer. If he/she is selected it unselect him/her
	deselectAll();
	//after unselecting all it calls to the method of random student selection(the method above)
	ansRandomSt();
}

//with this it gets the data for the datatable with ajax
$(document).ready(function(){
	$.ajax({
		url: './include/processvquestionstudentprof.php',
		type: 'post',
		data: { tag :'getData', pgroups:groups, pidvq:idvq},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						//if all was ok this fills the datatable with the data of the returned json
						var row = $("<tr id='stud-"+record.Idstudent+"'></>");
						$('<td class="id" onclick="checking('+i+')"> <input onclick="checking('+i+')" id="'+i+'" type="checkbox" /></>').appendTo(row);
						$("<td id='id-"+i+"' onclick='checking("+i+")' class='id'>"+record.Idstudent+"</>").appendTo(row);
						$("<td class='id' onclick='checking("+i+")'></>").text(record.Stname).appendTo(row);
						$("<td class='id' onclick='checking("+i+")'></>").text(record.Questdone+'/'+record.Quest).appendTo(row);
						$("<td class='id' onclick='checking("+i+")'></>").text(record.Questionisdone).appendTo(row);
						$("<td class='id' onclick='evallist("+record.Idstudent+',"'+record.Stname+'"'+")'></>").text("Click here to see the evaluator list of this student").appendTo(row);
						row.appendTo('#tablevqueststudentprof');
						i=i+1;
						//here the probabilities of a student to be selected are calculated
						//if the diference of number of questions answered by the student are less than 4 it sets 50% to all the students
						if (data.maxmin<=3) {
							studentrandom[record.Idstudent]=0.5;
						}
						//else the probabilities are calculated like (( number of questions this student has to answer - min number of questions)*0.95)/(max number of questions - min number of questions)
						//the max is the number of questions of a student who had more than the other students and the min is the same but with the min number of questions. The student with the max number has a probability of the 5% of being selected and the min has a 100%
						else{
							studentrandom[record.Idstudent]=((record.Quest-data.min)*0.95)/data.maxmin;
						}
					};
				});
				// first this calls to the method of random selection to have all the arrays initialized and select random student to answer the question
				ansRandomSt();
				//if this is a moification of a question this loads the students who were selected. It gets all the data from the database
				if (idvq>0) {
					loadstudent();
				}
			}

			//this are the options of the datatable(scrollable, not paging and some coloumns not orderable)
			dtable=$('#tablevqueststudentprof').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				'columns': [ 
					{ "orderable": false },
					null,
					null,
					null,
					null,
					{ "orderable": false }
				],
				"scrollY":        (screen.height/2)-60+'px',
        		"scrollCollapse": true,
        		"paging": false
			});


		}
	});
	
});

//this method opens the iframe with the list of evaluators for one student
function evallist(idstudent,stname){
	if (!document.getElementById("evalLists")) {
	    var frame = document.createElement("IFRAME");
	    frame.style.borderColor="#000000";
	    frame.style.background="#ffffff";
	    frame.id="evalLists";
	    frame.style.position="absolute";
	    frame.style.width='720px';
	    frame.style.height='620px';
	    frame.style.top=window.outerHeight -window.innerHeight+"px";
	    frame.style.left=(window.innerWidth-650)/2+"px";
	    frame.src="professor_vquestion_selecstudenteval.php?stdid="+idstudent+"&stdname="+stname;
	    document.body.appendChild(frame);
	}
	//this tells the user to close the first iframe before opening another
	else{
		alert("Close the first window to open another please");
	}
}

//this method checks the checkboxes of the datatable
function checking(id){
	//if the checkbox with the id of the parameters is checked
	if (document.getElementById(id).checked) {
		var i=0;
		//it uncheck it, delete the evaluators for the student of that row and to those evaluators it substract one evaluation
		document.getElementById(id).checked=false;
		while(students[i][0]!=document.getElementById('id-'+id).innerHTML){
			i++;
		}
		var j=1;
		while(students[i][j]){
			evaluators[students[i][j]]--;
			j++;
		}
		students[i]=[document.getElementById('id-'+id).innerHTML];
	}
	//if it wasn't checked it checks it and search evaluators for the student of that row
	else{
		if (evalRandomSt(document.getElementById('id-'+id).innerHTML)) {
			document.getElementById(id).checked=true;
		}
	}
}

//this method selects all the student(if it can, because of the maximum number of evaluations per student) to answer the question
function selectAll(){
	//first it deletes the searches done at the table to make all the rows of the table appear
	document.getElementById("tablevqueststudentprof_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablevqueststudentprof_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
	var i=0;
	//while theres a row, it checks if the checkbox is checked. If it wasn't checket it checks it and search evaluators for the student of that row
	while(document.getElementById(i)){
		if (!document.getElementById(i).checked) {
			checking(i);
		}
		i++;
	}
}

//this method unselect all the students of the datatable
function deselectAll(){
	if (document.getElementById("tablevqueststudentprof_filter")) {
		//first it deletes the searches done at the table to make all the rows of the table appear
		document.getElementById("tablevqueststudentprof_filter").children[0].children[0].id="searchtext";
		document.getElementById("tablevqueststudentprof_filter").children[0].children[0].value="";
		$("#searchtext").trigger("search");
	}	
	var i=0;
	//while theres a row in the table it unchecks the checkbox. If the checkbox was selecter it unchecks it and after that it deletes the evaluators of that student
	while(document.getElementById(i)){
		if (document.getElementById(i).checked) {
			checking(i);
		}
		i++;
	}
}

//this method sends the data to another file(to create or modificate the question) to write the data in the database
//http://stackoverflow.com/questions/8638984/send-post-data-to-php-without-using-an-html-form
function sendData(where){
	var theForm, newInput1, newInput2,newInput3,newInput4,newInput5,newInput6;
	// Start by creating a <form>
	theForm = document.createElement('form');
	theForm.action = where;
	theForm.method = 'post';
	// Next create the <input>s in the form and give them names and values
	newInput1 = document.createElement('input');
	newInput1.type = 'hidden';
	newInput1.name = 'question';
	newInput1.value = question.replace(/<br>/g,"\\n");
	newInput2 = document.createElement('input');
	newInput2.type = 'hidden';
	newInput2.name = 'deadline';
	newInput2.value = deadline;
	newInput3 = document.createElement('input');
	newInput3.type = 'hidden';
	newInput3.name = 'uploadedfile';
	newInput3.value = uploadedfile;
	newInput4 = document.createElement('input');
	newInput4.type = 'hidden';
	newInput4.name = 'selecriterions';
	newInput4.value = selecriterions;
	newInput5 = document.createElement('input');
	newInput5.type = 'hidden';
	newInput5.name = 'students';
	var i=0;
	var text="";
	//if there are students it makes a string separated with comas to send it as data and change it to an array in other file
	while(students[i]){
		text= text+""+students[i].toString()+";";
		i++;
	}
	if (text[1]) {
		newInput5.value = text.slice(0,(text.length)-1);
	}
	newInput6 = document.createElement('input');
	newInput6.type = 'hidden';
	newInput6.name = 'maxans';
	newInput6.value = maxans;
	//if it is a modification it adds the id of the question to the form
	if (where=='modify_vquestion.php') {
		newInput7 = document.createElement('input');
		newInput7.type = 'hidden';
		newInput7.name = 'idvq';
		newInput7.value = idvq;
		theForm.appendChild(newInput7);
	}
	// Now put everything together...
	theForm.appendChild(newInput1);
	theForm.appendChild(newInput2);
	theForm.appendChild(newInput3);
	theForm.appendChild(newInput4);
	theForm.appendChild(newInput5);
	theForm.appendChild(newInput6);
	// ...and it to the DOM...
	document.getElementById('hidden_form_container').appendChild(theForm);
	// ...and submit it
	theForm.submit();
}