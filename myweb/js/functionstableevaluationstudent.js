/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this fills the datatable with the data returned in a json
$(document).ready(function(){
	$.ajax({
		url: './include/processevaluationstudentprof.php',
		type: 'post',
		data: { tag: 'getData',getidva:idva,crit:crits.slice(0,crits.length-1)},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				var j;
				var criterion="";
				//if all was ok this fills the rows of the datatable
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr />");
						$("<td class='id'  > </>").text(record.Idstd).appendTo(row);
						$("<td class='id'  > </>").text(record.STname).appendTo(row);
						criterion=record.grade.split(',');
						j=0;
						//this adds data to a coloumn per criterion evaluated
						while(criterion[j]){
							$("<td class='id'> </>").text(criterion[j]).appendTo(row);
							j++;
						}
						//if theres a voice explanation to the grades it adds it to a coloumn as an audio file
						if (record.explain!="-") {
							$("<td class='id' ><audio controls><source src='"+record.explain+"' type='audio/ogg'></audio></>").appendTo(row);
						}
						else{
							$("<td class='id' > </>").text(record.explain).appendTo(row);
						}
						row.appendTo('#tablevals');
						i=i+1;
					};
				});
			}

			//this are the options of the datatable
			dtable=$('#tablevals').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				"scrollY":        "300px",
				"scrollX":        "800px",
        		"scrollCollapse": true,
        		"paging": false
			});

		}
	});
});


