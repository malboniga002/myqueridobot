/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this fills the datatable with the data returned in a json
$(document).ready(function(){
	$.ajax({
		url: './include/processevaluationprof.php',
		type: 'post',
		data: { tag: 'getData'},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				//if all was ok this fills the rows of the datatable
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr />");
						$("<td class='id' onclick='location.href="+'"'+'professor_evaluation_vanswer.php?idva='+record.Ideval+'"'+"'>"+record.Ideval+"</>").appendTo(row);
						$("<td class='id' onclick='location.href="+'"'+'professor_evaluation_vanswer.php?idva='+record.Ideval+'"'+"'>"+record.STName+"<audio controls><source src='."+record.Path+"' type='audio/ogg'></></>").appendTo(row);
						$("<td title='"+record.Question.replace(/'/g,"\`")+"' onclick='location.href="+'"'+'professor_evaluation_vanswer.php?idva='+record.Ideval+'"'+"'> </>").text(record.Question.slice(0,50)+"...").appendTo(row);
						$("<td class='id' onclick='location.href="+'"'+'professor_evaluation_vanswer.php?idva='+record.Ideval+'"'+"'> </>").text(record.Creationdate).appendTo(row);
						$("<td class='id' onclick='location.href="+'"'+'professor_evaluation_vanswer.php?idva='+record.Ideval+'"'+"'> </>").text(record.Evaluations).appendTo(row);
						$("<td title='"+record.Criterions.replace(/'/g,"\`")+"' onclick='location.href="+'"'+'professor_evaluation_vanswer.php?idva='+record.Ideval+'"'+"'> </>").text(record.Criterions.slice(0,50)+"...").appendTo(row);
						row.appendTo('#tablevalprof');
						i=i+1;
					};
				});
			}

			// Setup - add a text input to each footer cell
		    $('#tablevalprof tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" size="17" placeholder="Search '+title+'" />' );
		    } );

		    //this are the options of the datatable
			dtable=$('#tablevalprof').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				"order":[[0,'desc']]
			});

			//this method makes the search when something is written at search boxes at the footer of the table
			dtable.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );

		}
	});
});
