/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this fills the datatable with the data returned in a json
$(document).ready(function(){
	$.ajax({
		url: './include/processgroupsprof.php',
		type: 'post',
		data: { tag: 'getData'},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				//if all was ok this fills the rows of the datatable
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr />");
						$("<td id='id-"+i+"' class='id' onclick="+'"location.href='+"'professor_group_students.php?gid="+record.Idgroup+"'"+'">'+record.Idgroup+"</>").appendTo(row);
						$("<td id='name-"+i+"' title='"+record.Groupname.replace(/'/g,"\`")+"' onclick="+'"location.href='+"'professor_group_students.php?gid="+record.Idgroup+"'"+'"> </>').text(record.Groupname+"...").appendTo(row);
						$("<td class='id' onclick="+'"location.href='+"'professor_group_students.php?gid="+record.Idgroup+"'"+'"> </>').text(record.Members).appendTo(row);
						$("<td class='id' onclick="+'"location.href='+"'professor_group_students.php?gid="+record.Idgroup+"'"+'"> </>').text(record.Questions).appendTo(row);
						$("<td class='id' onclick="+'"location.href='+"'professor_group_students.php?gid="+record.Idgroup+"'"+'"> </>').text(record.Creationdate).appendTo(row);
						$('<td class="id" onclick="checking('+i+')"> <input onclick="checking('+i+')" id="'+i+'" type="checkbox" /></>').appendTo(row);
						row.appendTo('#tablegroupprof');
						i=i+1;
					};
				});
			}

			// Setup - add a text input to each footer cell
		    $('#tablegroupprof tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" size="17" placeholder="Search '+title+'" />' );
		    } );

		    //this are the options of the datatable
			dtable=$('#tablegroupprof').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				'columns': [ 
					null,
					null,
					null,
					null,
					null,
					{ "orderable": false }
				],
				"order":[[0,'desc']]
			});

			//this method makes the search when something is written at search boxes at the footer of the table
			dtable.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );

		}
	});
});

//this function opens  the iframe to delete groups
function deleteGroups(){
	var frame = document.createElement("IFRAME");
    frame.style.borderColor="#000000";
    frame.style.background="#ffffff";
    frame.id="deleteGroup";
    frame.style.position="absolute";
    frame.style.width='500px';
    frame.style.height='180px';
    frame.style.top=(window.innerHeight-300)/2+"px";
    frame.style.left=(window.innerWidth-500)/2+"px";
    frame.src="delete_group.php";
    document.body.appendChild(frame);
}

//this method checks or unchecks the checkbox of one row depending of its state
function checking(id){
	if (document.getElementById(id).checked) {
		document.getElementById(id).checked=false;
	}
	else{
		document.getElementById(id).checked=true;
	}
}

//this function opens  the iframe to add groups
function createGroup(){
	var frame = document.createElement("IFRAME");
    frame.style.borderColor="#000000";
    frame.style.background="#ffffff";
    frame.id="createGroup";
    frame.style.position="absolute";
    frame.style.width='500px';
    frame.style.height='210px';
    frame.style.top=(window.innerHeight-300)/2+"px";
    frame.style.left=(window.innerWidth-500)/2+"px";
    frame.src="create_group.php";
    document.body.appendChild(frame);
}

//it deletes the searches done at the table to make all the rows of the table appear
function removeSearch(){
	document.getElementById("tablegroupprof_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablegroupprof_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
}