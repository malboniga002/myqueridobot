/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this function opens an iframe in the options window to ask to the user if he/she is sure about the changes.
function saveChanges(){
	var frame = document.createElement("IFRAME");
    frame.style.borderColor="#000000";
    frame.style.background="#ffffff";
    frame.id="optionChange";
    frame.style.position="absolute";
    frame.style.width='500px';
    frame.style.height='130px';
    frame.style.top=(window.innerHeight)/2+"px";
    frame.style.left=(window.innerWidth-500)/2+"px";
    frame.src="options.php";
    document.body.appendChild(frame);
}