/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//the functions in this class are to open iframes at the voice question creation and modification windows.
//this opens the iframe for uploading an image for the voice question
function selectImage(){
    var frame = document.createElement("IFRAME");
    frame.style.borderColor="#000000";
    frame.style.background="#ffffff";
    frame.id="uploadImage";
    frame.style.position="absolute";
    frame.style.width='400px';
    frame.style.height='150px';
    frame.style.top=(window.innerHeight-200)/2+"px";
    frame.style.left=(window.innerWidth-400)/2+"px";
    frame.src="uploadfile.php";
    document.body.appendChild(frame);
}

//this method opens the iframe to create a new criterion
function createCrit(){
	var frame = document.createElement("IFRAME");
    frame.style.borderColor="#000000";
    frame.style.background="#ffffff";
    frame.id="createCrit";
    frame.style.position="absolute";
    frame.style.width='500px';
    frame.style.height='210px';
    frame.style.top=(window.innerHeight+50)/2+"px";
    frame.style.left=(window.innerWidth-500)/2+"px";
    frame.src="create_criterion.php";
    document.body.appendChild(frame);
}

//this method opens the window to delete one of the criterions
function deleteCrit(){
	var frame = document.createElement("IFRAME");
    frame.style.borderColor="#000000";
    frame.style.background="#ffffff";
    frame.id="deleteCrit";
    frame.style.position="absolute";
    frame.style.width='500px';
    frame.style.height='250px';
    frame.style.top=(window.innerHeight)/2+"px";
    frame.style.left=(window.innerWidth-500)/2+"px";
    frame.src="delete_criterion.php";
    document.body.appendChild(frame);
}