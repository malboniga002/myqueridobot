/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//https://www.verkia.com/foro/javascript/57-permitir-solo-numeros-en-un-campo-de-texto.html
function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //this if permits the use of the key of retrocess
    if (tecla==8){
        return true;
    }
        
    //this only permits the keys from 0 to 9, if the user presses othere key this method returns false
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}