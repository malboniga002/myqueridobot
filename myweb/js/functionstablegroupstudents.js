/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this fills the datatable with the data returned in a json
$(document).ready(function(){
	$.ajax({
		url: './include/processvquestionevalstudentprof.php',
		type: 'post',
		data: { tag: 'getData',pgroups:idg,stid:'0',pidvq:'0'},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				//if all was ok this fills the rows of the datatable
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr id='stud-"+record.Idstudent+"'></>");
						$("<td id='id-"+i+"' class='id' onclick='checking("+i+")'>"+record.Idstudent+"</>").appendTo(row);
						$("<td class='id' onclick='checking("+i+")' id='name-"+i+"'></>").text(record.Stname).appendTo(row);
						$("<td class='id' onclick='checking("+i+")'></>").text(record.Quest).appendTo(row);
						$("<td class='id' onclick='checking("+i+")'></>").text(record.Evals).appendTo(row);
						$('<td class="id" onclick="checking('+i+')"> <input onclick="checking('+i+')" id="'+i+'" type="checkbox" /></>').appendTo(row);
						row.appendTo('#tablegroup');
						i=i+1;
					};
				});
			}

			//this are the options of the datatable
			dtable=$('#tablegroup').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				'columns': [ 
					null,
					null,
					null,
					null,
					{ "orderable": false }
				],
				"scrollY":        (screen.height/2)-25+'px',
        		"scrollCollapse": true,
        		"paging": false
			});


		}
	});
	
});

//this function opens  the iframe to add students to the group
function addStudents(){
	var frame = document.createElement("IFRAME");
	frame.style.borderColor="#000000";
	frame.style.background="#ffffff";
	frame.id="userList";
	frame.style.position="absolute";
	frame.style.width='650px';
	frame.style.height='600px';
	frame.style.top="50px";
	frame.style.left=(window.innerWidth-650)/2+"px";
	frame.src="professor_group_user.php?idg="+idg;
	document.body.appendChild(frame);
	
}

//this function opens  the iframe to delete students from the group
function deleteStudents(){
	var frame = document.createElement("IFRAME");
    frame.style.borderColor="#000000";
    frame.style.background="#ffffff";
    frame.id="deleteStudent";
    frame.style.position="absolute";
    frame.style.width='500px';
    frame.style.height='180px';
    frame.style.top=(window.innerHeight-300)/2+"px";
    frame.style.left=(window.innerWidth-500)/2+"px";
    frame.src="delete_student.php";
    document.body.appendChild(frame);
}

//this method checks or unchecks the checkbox of one row depending of its state
function checking(id){
	if (document.getElementById(id).checked) {
		document.getElementById(id).checked=false;
	}
	else{
		document.getElementById(id).checked=true;
	}
}

//it deletes the searches done at the table to make all the rows of the table appear
function removeSearch(){
	document.getElementById("tablegroup_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablegroup_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
}