/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this fills the datatable with the data returned in a json
$(document).ready(function(){
	$.ajax({
		url: './include/processevaluatorsprof.php',
		type: 'post',
		data: { tag: 'getData'},
		dataType: 'json',
		success: function(data){
			if(data.success){
				//if all was ok this fills the rows of the datatable
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr></>");
						$("<td class='id' >"+record.Idstudent+"</>").appendTo(row);
						$("<td class='id' ></>").text(record.Stname).appendTo(row);
						$("<td class='id' ></>").text(record.Groupname).appendTo(row);
						$("<td class='id' onclick='evallist("+record.Idstudent+',"'+record.Stname+'"'+")'>Click here to check <br>who is going to be evaluated by this student</>").appendTo(row);
						$("<td class='id' onclick='evaluatorlist("+record.Idstudent+',"'+record.Stname+'"'+")'>Click here to check <br>who is going to evaluate this student</>").appendTo(row);
						row.appendTo('#tableevaluatorprof');
					};
				});
			}

			//this are the options of the datatable
			dtable=$('#tableevaluatorprof').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				"scrollY":        (screen.height/2)-25+'px',
        		"scrollCollapse": true,
        		"paging": false
			});


		}
	});
	
});


//this method opens the iframe with the list of evaluated student
function evallist(idstudent,stname){
	if (!document.getElementById("evaluatorLists") && !document.getElementById("evalLists") && !document.getElementById("checkAllEval")) {
	    var frame = document.createElement("IFRAME");
	    frame.style.borderColor="#000000";
	    frame.style.background="#ffffff";
	    frame.id="evalLists";
	    frame.style.position="absolute";
	    frame.style.width='720px';
	    frame.style.height='620px';
	    frame.style.top=60+"px";
	    frame.style.left=(window.innerWidth-650)/2+"px";
	    frame.src="professor_evaluator_evaluator.php?stdid="+idstudent+"&stdname="+stname;
	    document.body.appendChild(frame);
	}
	//this tells the user to close the first iframe before opening another
	else{
		alert("Close the first window to open another please");
	}
}

//this method opens the iframe with the list of evaluators of the student at the parameters
function evaluatorlist(idstudent,stname){
	if (!document.getElementById("evaluatorLists") && !document.getElementById("evalLists") && !document.getElementById("checkAllEval")) {
	    var frame = document.createElement("IFRAME");
	    frame.style.borderColor="#000000";
	    frame.style.background="#ffffff";
	    frame.id="evaluatorLists";
	    frame.style.position="absolute";
	    frame.style.width='720px';
	    frame.style.height='620px';
	    frame.style.top=60+"px";
	    frame.style.left=(window.innerWidth-650)/2+"px";
	    frame.src="professor_evaluator_evaluated.php?stdid="+idstudent+"&stdname="+stname;
	    document.body.appendChild(frame);
	}
	//this tells the user to close the first iframe before opening another
	else{
		alert("Close the first window to open another please");
	}
}

//opens the iframe to check if the user realy wants to put as checked all the information of evaluations and evaluators
function checkAll(){
	if (!document.getElementById("evaluatorLists") && !document.getElementById("evalLists") && !document.getElementById("checkAllEval")) {
	    var frame = document.createElement("IFRAME");
	    frame.style.borderColor="#000000";
	    frame.style.background="#ffffff";
	    frame.id="checkAllEval";
	    frame.style.position="absolute";
	    frame.style.width='720px';
	    frame.style.height='130px';
	    frame.style.top=window.outerHeight -window.innerHeight+"px";
	    frame.style.left=(window.innerWidth-650)/2+"px";
	    frame.src="professor_check_inforstudent.php";
	    document.body.appendChild(frame);
	}
	//this tells the user to close the first iframe before opening another
	else{
		alert("Close the first window to open another please");
	}
}