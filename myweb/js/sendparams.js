/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//http://stackoverflow.com/questions/8638984/send-post-data-to-php-without-using-an-html-form
function nextStep(where){
	//first it deletes the searches done at the table to make all the rows of the table appear
	document.getElementById("tablequestgroup_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablequestgroup_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
	var theForm, newInput1, newInput2,newInput3,newInput4,newInput5,newInput6;
	var flag=true;
	// Start by creating a <form>
	theForm = document.createElement('form');
	theForm.action = where;
	theForm.method = 'post';
	// Next create the <input>s in the form and give them names and values
	newInput1 = document.createElement('input');
	newInput1.type = 'hidden';
	newInput1.name = 'question';
	newInput1.value = document.getElementById('question').value.replace(/\n/g,"<br>").replace(/'/g,"\\'");
	newInput2 = document.createElement('input');
	newInput2.type = 'hidden';
	newInput2.name = 'deadline';
	newInput2.value = document.getElementById("deadline").value;
	newInput3 = document.createElement('input');
	newInput3.type = 'hidden';
	newInput3.name = 'uploadedfile';
	newInput3.value = document.getElementById("imagepath").innerHTML;
	newInput4 = document.createElement('input');
	newInput4.type = 'hidden';
	newInput4.name = 'selecriterions';
	//while theres a criterion id it adds it to a string separating them with comas
	var r=0;
	var seltext="";
	while(select.options[r]){
		if(select.options[r].selected){
			seltext=seltext+""+select.options[r].id.slice(5)+",";
		}
		r++;
	}
	//after creating the list this line removes the last coma
	newInput4.value = seltext.slice(0,(seltext.length)-1);
	newInput5 = document.createElement('input');
	newInput5.type = 'hidden';
	newInput5.name = 'selecgroups';
	//while theres a group id it adds it to a string separating them with comas
	r=0;
	seltext="";
	while(document.getElementById(""+r)){
		if(document.getElementById(""+r).checked){
			seltext=seltext+""+document.getElementById("id-"+r).innerHTML+",";
		}
		r++;
	}
	//after creating the list this line removes the last coma
	newInput5.value = seltext.slice(0,seltext.length-1);
	newInput6 = document.createElement('input');
	newInput6.type = 'hidden';
	newInput6.name = 'maxans';
	var x=document.getElementById("maxans").value;
	//if theres nothing written in the maximun answer input this sets it to 10 as default
	if(x==""){
		x="10";
	}
	newInput6.value = x;
	//if theres an empty input, this sends an allert to the user
	var msg="";
	if (newInput4.value=="") {
		msg="Por favor, seleccione como mínimo un criterio\n";
		flag=false;
	}
	if(newInput5.value==""){
		msg=msg+"Por favor, seleccione un grupo como mínimo\n";
		flag=false;
	}
	if(newInput1.value==""){
		msg=msg+"Por favor, escriba la pregunta que desea hacer a sus alumnos"
		flag=false;
	}
	if(flag){
	// Now put everything together...
	theForm.appendChild(newInput1);
	theForm.appendChild(newInput2);
	theForm.appendChild(newInput3);
	theForm.appendChild(newInput4);
	theForm.appendChild(newInput5);
	theForm.appendChild(newInput6);
	// ...and it to the DOM...
	document.getElementById('hidden_form_container').appendChild(theForm);
	// ...and submit it
	theForm.submit();
	}
	else{
		alert(msg);
	}
}




