/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this fills the datatable with the data returned in a json
$(document).ready(function(){
	$.ajax({
		url: './include/processvquestionevalstudentprof.php',
		type: 'post',
		data: { tag: 'getData',pgroups:idg,stid:'0',pidvq:'0'},
		dataType: 'json',
		success: function(data){
			if(data.success){
				//if all was ok this fills the rows of the datatable
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr id='stud-"+record.Idstudent+"'></>");
						$("<td class='id' >"+record.Idstudent+"</>").appendTo(row);
						$("<td class='id' ></>").text(record.Stname).appendTo(row);
						$("<td class='id' ></>").text(record.Quest).appendTo(row);
						$("<td class='id' ></>").text(record.Evals).appendTo(row);
						row.appendTo('#tablegroup');
					};
				});
			}

			//this are the options of the datatable
			dtable=$('#tablegroup').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				"scrollY":        (screen.height/2)-25+'px',
        		"scrollCollapse": true,
        		"paging": false
			});


		}
	});
	
});

//this method closes the iframe
function closeGroup(){
	parent.document.getElementById('studentList').remove();
}