/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
var dtable;
var r;

//option to not use the cache when this file use ajax
$.ajaxSetup({ cache:false });

//this function loads the students who have to evaluate other student
function loadingEval(){
	var i=1;
	var j;
	r=0;
	//first it search the student evaluated
	while(parent.students[r][0]!=idstd){
		r++;
	}
	//after that it search his/her evaluators and checks the checkbox of that row
	while(j=parent.students[r][i]){
		document.getElementById('stud-'+j).children[5].children[0].checked=true;
		i++;
	}
}

//this fills the datatable with the data returned from a json
$(document).ready(function(){
	$.ajax({
		url: './include/processvquestionevalstudentprof.php',
		type: 'post',
		data: { tag: 'getData',pgroups:parent.groups,stid:idstd,pidvq:parent.idvq},
		dataType: 'json',
		success: function(data){
			if(data.success){
				var i=0;
				//if all was ok it fills all the rows
				$.each(data, function(index, record){
					if ($.isNumeric(index)) {
						var row = $("<tr id='stud-"+record.Idstudent+"'></>");
						$("<td id='id-"+i+"' class='id' onclick='checking("+i+")'>"+record.Idstudent+"</>").appendTo(row);
						$("<td class='id' onclick='checking("+i+")' id='name-"+i+"'></>").text(record.Stname).appendTo(row);
						$("<td class='id' onclick='checking("+i+")'></>").text(record.Quest).appendTo(row);
						$("<td class='id' onclick='checking("+i+")'></>").text(record.Evals).appendTo(row);
						$("<td class='id' id='ans-"+i+"' onclick='checking("+i+")'></>").text(record.Questionisdone).appendTo(row);
						$('<td class="id" onclick="checking('+i+')"> <input onclick="checking('+i+')" id="'+i+'" type="checkbox" /></>').appendTo(row);
						row.appendTo('#tablevquestevalstudentprof');
						i=i+1;
					};
				});
			loadingEval();
			}

			//this are the options of the datatable
			dtable=$('#tablevquestevalstudentprof').DataTable({
				"bJQueryUI":true,
				"sPaginationType":"full_numbers",
				'columns': [ 
					null,
					null,
					null,
					null,
					null,
					{ "orderable": false }
				],
				"scrollY":        (screen.height/2)-25+'px',
        		"scrollCollapse": true,
        		"paging": false
			});


		}
	});
	
});

//this method checks or unchecks the checbox with the id in the parameter
function checking(id){
	//if it was checked it unchecks it and substracts one evaluation to the evaluator of that row
	if(document.getElementById(id).checked){
		document.getElementById(id).checked=false;
	}
	//if it wasn't checked it checks it the student of that row hasn't reach the maximum of evaluations
	else{
		//if the student hasn't reach the maximum number of evaluations, it checks it 
		if(parent.evaluators[parseInt(document.getElementById('id-'+id).innerHTML)]<parseInt(parent.maxeval)){
			document.getElementById(id).checked=true;
		}
		//if thats not the case it alerts the user that the student of that row has reach the maximum evaluations permited for the question
		else{
			alert(document.getElementById('name-'+id).innerHTML+" has reach the maximum number of evaluations");
		}
	}
}

//this method selects all the students at the datatable(if it can)
function selectAll(){
	//first it deletes the searches done at the table to make all the rows of the table appear
	document.getElementById("tablevquestevalstudentprof_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablevquestevalstudentprof_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
	var i=0;
	//while theres a row at the table and it isn't checked it checks it and adds a evaluation to the tudent of that row
	while(document.getElementById(i)){
		if (!document.getElementById(i).checked) {
			checking(i);
		}
		i++;
	}
}

//saves the number of calls the functions selectRandom and selectAnsweredRandom does.
var max=0;

//this method unselects all the students in the tables
function deselectAll(){
	//first it deletes the searches done at the table to make all the rows of the table appear
	document.getElementById("tablevquestevalstudentprof_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablevquestevalstudentprof_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
	var i=0;
	//while theres a row at the table and it is selected it unselects it
	while(document.getElementById(i)){
		if (document.getElementById(i).checked) {
			checking(i);
		}
		i++;
	}
}

//this method selects random students in the table of evaluators
function selectRandom(){
	deselectAll();
	var i=0;
	var flag=false;
	//while theres a row at the table 
	while(document.getElementById(i)){
		//it checks if the student has passed the maximum number of evaluations, if he/she hasnt reach tha maximum
		if (parent.evaluators[document.getElementById('id-'+i).innerHTML]<parseInt(parent.maxeval)) {
			//it selects a student to make an evaluation the 50% of the times
			if (Math.random()<0.5) {
				flag=true;
				checking(i);
			}
		}
		i++;
	}
	//if theres no student selected for evaluartion, this makes the method recursive 6 times as the maximum
	if (!flag && max<6) {
		selectRandom();
		max++;
	}
	else{
		max=0;
	}
}

function selectAnsweredRandom(){
	deselectAll();
	var flag=false;
	var i=0;
	//while theres a row at the table 
	while(document.getElementById(i)){
		//it checks if the student has passed the maximum number of evaluations, if he/she hasnt reach tha maximum
		if (parent.evaluators[document.getElementById('id-'+i).innerHTML]<parseInt(parent.maxeval)) {
			if (document.getElementById("ans-"+i).innerHTML=="Yes") {
				//it selects a student to make an evaluation the 50% of the times
				if (Math.random()<0.5) {
					flag=true;
					checking(i);
				}
			}
		}
		i++;
	}
	//if theres no student selected for evaluartion, this makes the method recursive 6 times as the maximum
	if (!flag && max<6) {
		selectAnsweredRandom();
		max++;
	}
	else{
		max=0;
	}
}

//this method closes this iframe
function cancelChanges(){
	parent.document.getElementById('evalLists').remove();
}

//this method saves the changes at the table(checks the students selected and unselected to see if they have changed)
function saveChanges(){
	//first it deletes the searches done at the table to make all the rows of the table appear
	document.getElementById("tablevquestevalstudentprof_filter").children[0].children[0].id="searchtext";
	document.getElementById("tablevquestevalstudentprof_filter").children[0].children[0].value="";
	$("#searchtext").trigger("search");
	var i=0;
	parent.students[r]=[];
	//while theres a student selected it adds to the evaluated students array as his/her evaluator
	while(document.getElementById(i)){
		if (document.getElementById(i).checked) {
			if (parent.evaluators[document.getElementById('id-'+i).innerHTML]<parseInt(parent.maxeval)) {
				parent.students[r].push(document.getElementById('id-'+i).innerHTML);
				parent.evaluators[document.getElementById('id-'+i).innerHTML]++;
			}
		}
		//if the student isn't selected it search if it was modificated. If it was it deletes it from the evaluator list of the student evaluated and substracts a evaluation to the student
		else if(parent.students[r].indexOf(document.getElementById('id-'+i).innerHTML)>=0){
				parent.evaluators[parseInt(document.getElementById('id-'+i).innerHTML)]--;
				parent.students[r].splice(parent.students[r].indexOf(document.getElementById('id-'+i).innerHTML),1);
		}
		i++;
	}
	parent.students[r].unshift(idstd.toString());
	//if this student has one evaluator it selects it at the parent window
	if (parent.students[r][1]) {
		parent.document.getElementById("stud-"+idstd).children[0].children[0].checked=true;
	}
	//if theres no evaluator for that student it unselects it at the parent window
	else{
		parent.document.getElementById("stud-"+idstd).children[0].children[0].checked=false;
	}
	//after that it removes this iframe
	parent.document.getElementById('evalLists').remove();
}