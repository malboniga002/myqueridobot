<!doctype html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Questions - Select students</title>
    <?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //if the data was sended correctly to this page from professor_create_vquestion.php or professor_vquestion_modification.php
      if(isset($_POST['selecgroups'])){
        require_once "include/mydb.php";
        //gets the options of the professor to make possible the autoevaluation or not and to know the maximun number of evaluations a student can do in a question
        $options=getProfOptions($_SESSION['userid'],"../Globals.php");
        //and passes all the data to js varibles
        echo "<script>var groups='".$_POST['selecgroups'].
        "';var autoeval='".$options['autoeval']."';var maxeval='".$options['max_eval'].
        "';var question='".$_POST['question']."';var deadline='".$_POST['deadline'].
        "';var uploadedfile='".$_POST['uploadedfile']."';var selecriterions='".$_POST['selecriterions'].
        "';var maxans='".$_POST['maxans']."';";
        echo "</script>";
      }
      //if this file didnt get any data it sends the user to professor_vquestion.php
      else{
        header('Location:professor_vquestion.php');
      }
    ?>
    <?php
      /**if theres data in the parameter idvq(id of the voice question) at the url that means this file was called from professor_vquestion_modification.php,
        wich means that this page has to load the users who were selected to answer the question and those selected to evaluate the students who have to answer
        Before the explanation of how to load let me explain how does the selection system works.
        The ids of students of all selected groups are in an array of arrays like the next example:
        students=[['id of an student who has to answer the question','id of a student who has to evaluate the first student','another evaluators id',...],['id of a student'],...]
        Te first array in students(students[0]) has a student who was selected to answer the question, and the rest of this array is comleted by the evaluators of that student.
        Now the important difference. Have you notice that the second array(students[1]) has only one student? that should be another student who has to answer the question,
        but thats not the case. HE/SHE HAS NO EVALUATORS. If thats the case it means that this student DOESNT have to answer the question.
        Another variable to explain is the evaluators variable. Its an array of all the students participationg in the question, but its an array with the next format:
        evaluators=['id of the student'=>'number of evaluations he/she has to do at the moment',...]
        This array is to have the control of the number of evaluations the students have to do and to control that they dont pass the maximun of evaluations wich was set by the user(professor)
      **/

      if (isset($_GET['idvq'])) {
          echo '<script>var idvq='.$_GET['idvq'].';';
          ?>
            function loadstudent(){
              //after loading the datatable there are random selected students, thats why the first step is to unselect them
              deselectAll();
              var r;
              var done;
              <?php
                //this gets the students who have to answer the question and his evaluator(a pear of evaluated and evaluator but one evaluated student has more than one evaluators)
                $students=getStudentSelected($_GET['idvq']);
                //when it gets the data from the array it gets it duplicated rows because of the criterions(one evaluator can evaluate more than one criterion)
                $numcrits=0;
                if (isset($students[$numcrits]['id_student_evaluator'])) {
                  $evaluatorsame=$students[$numcrits]['id_student_evaluator'];
                  //with this while it gets the number of rows it has to skip to not load any duplicated row
                  while (isset($students[$numcrits]) and $students[$numcrits]['id_student_evaluator']==$evaluatorsame) {
                    $numcrits++;
                  }
                }
                $i=0;
                $st="";
                //while theres an evaluated student(those who have to answer the question) 
                while (isset($students[$i])) {
                  //if the student evaluated changes
                  if($st!=$students[$i]['id_student_evaluated']){
                    //this saves the students id in a varible and search the position of this student in students array(what is explained at the paragraph above)
                    $st=$students[$i]['id_student_evaluated'];
              ?>  
                    r=0;
                    done=false;
                    while(students[r] && !done){
                      if(students[r][0]==<?php echo '"'.$students[$i]['id_student_evaluated'].'"'?>){
                        r--;
                        done=true;
                      }
                      r++;
                    }
                    
              <?php
                  }
                  //if theres a student with the position in the array(read the last commentary) this sets to true the checkbox at the datatable of the student in that position, adds him/her evaluators to the array( the positions students[r][from 1 to last position])
                  //and adds one to that evaluator in evaluators array(explained above)
              ?>  if(students[r] && evaluators[<?php echo $students[$i]['id_student_evaluator'];?>]>-1){
                    document.getElementById(<?php echo '"stud-'.$students[$i]['id_student_evaluated'].'"'?>).children[0].children[0].checked=true;
                    students[r].push(<?php echo '"'.$students[$i]['id_student_evaluator'].'"';?>);
                    evaluators[<?php echo '"'.$students[$i]['id_student_evaluator'].'"';?>]++;
                  }
              <?php
                  //with this it skips the duplicated rows(the duplicated rows are because the array returned by PDO is duplicated because of the number of criterions to evaluate)
                  $i=$i+$numcrits;
                }
              ?>
            }
          </script>
          <?php
        }
        else{
          //if the question is a new question it sets a provisional id(id=0)
          echo '<script>var idvq=0;</script>';
        }
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablevquestionstudent.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
  </head>
  <body>
  	<?php include("include/header.html") ?>
  	<div><center>
  		<div>
        <!--the question is wrote here-->
  			<h2><?php if (!isset($_GET['idvq'])){echo 'Create question: Select student';}else{echo 'Modify question: Select students';}?></h2>
        <h4>Question: <?php echo str_replace("\\'", "'", $_POST['question']); ?></h4>
  		</div>
  		<div>
        <!--some buttons to select students. The functions the call are in functionstablevquestionstudent.js-->
        <input type="button" name="random" value="Random selection" onclick='btRandom()'>&nbsp;&nbsp;&nbsp;
        <input type="button" name="select" value="Select all" onclick='selectAll()'>&nbsp;&nbsp;&nbsp;
        <input type="button" name="deselect" value="Unselect all" onclick='deselectAll()'><br><br>
  		</div>
      <?php if (isset($_GET['idvq'])){echo "<div><b>**IMPORTANT**: If you unselect a student who has answered the question and you save changes, thats going to delete his/her answer. If you change a evaluator and press the button ".'"save changes"'."(at the bottom of this message), his/her evaluators given grades are going to be deleted. If you don't press the button ".'"Save changes"'." no change will be saved, even if you press ".'save'." at a evaluator list that won't be saved untill you press the button below.Stay sure of what you are doing before saving modifications please.</b></div><br>";} ?>
      <!--this button sends the data to modify_vquestion.php if it has to be modified or to insert_vquestion.php if its a new question. The function to send data is in functionstablevquestionstudent.js-->
      <div><input type="button" name="save" value=<?php if (!isset($_GET['idvq'])){echo '"Save and create question"';}else{echo '"Save changes"';}?> onclick=<?php if (!isset($_GET['idvq'])){echo '"sendData('."'insert_vquestion.php'".')"';}else{echo '"sendData('."'modify_vquestion.php'".')"';}?>></div>
      <br>
  		<section>
        <!--this tables bodys data is loaded at functionstablevquestionstudent.js-->
  			<table id="tablevqueststudentprof">
	  			<thead>
	  				<tr>
              <th>Select</th>
              <th>ID</th>
	  					<th>Student</th>
              <th>Number of questions to answer <br>(Unanswered questions/Assigned questions)</th>
              <th>Answered this question</th>
	  					<th>Evaluator list</th>
	  				</tr>
	  			</thead>
	  		</table>
  		</section>
  		<footer></footer>
  	</center></div>
    <div id="hidden_form_container" style="display:none;"></div>
  </body>
</html>