<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //this gets the data for the graphic
      include_once "include/mydb.php";
      $std=getGraphic1Data1($_SESSION['userid']);
      if (!isset($std[0])) {
        header('Location: professor_graphics.php');
      }
      //after this  it calls to plotlys js librarys 
    ?>
    <title>Graphics</title>
	<!-- Latest compiled and minified plotly.js JavaScript -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.min.js"></script>

<!-- OR use a specific plotly.js release (e.g. version 1.5.0) -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-1.5.0.min.js"></script>

<!-- OR an un-minified version is also available -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.js"></script>
</head>
<body>
<center>
  <h2>Graphic of correct/wrong answers in quizzes per student</h2>
<div id="myDiv"></div>
</center>
	<script>
  document.getElementById("myDiv").style.height=window.window.innerHeight-70;
  document.getElementById("myDiv").style.width=window.innerWidth-40;
  <?php
  //this php code prepares the data for plotly
  $i=0;
  //the data will be in js arrays. This makes strings of those arrays
  $x="[";
  $y1="[";
  $y2="[";
  while(isset($std[$i])){
    $status=getGraphic1Data2($std[$i]['id_student']);
    // x contains the name of the student and his id
    $x.='"'.$std[$i]['first_name']." ".$std[$i]['last_name'].' id='.$std[$i]['id_student'].'",';
    if ($status['SUM(IF(result="right",1,0))']!=NULL) {
      // y1 has the number of right answers given by the student in quiz questions
      $y1.=$status['SUM(IF(result="right",1,0))'].',';
    }
    else{
      //if theres no right answer adds a 0
      $y1.='0,';
    }
    if ($status['SUM(IF(result="wrong",1,0))']!=NULL) {
      // y1 has the number of wrong answers given by the student in quiz questions
      $y2.=$status['SUM(IF(result="wrong",1,0))'].',';
    }
    else{
      //if theres no wrong answer adds a 0
      $y2.='0,';
    }
    $i++;
  }
  //after that it will have a ',' at the end. With this that coma desapears and after that it adds the end of the array(']')
  $x=substr($x, 0,strlen($x)-1);
  $y1=substr($y1, 0,strlen($y1)-1);
  $y2=substr($y2, 0,strlen($y2)-1);
  $x.="]";
  $y1.="]";
  $y2.="]";
  //here are created the variables for plotly to make the graphic
  echo "var trace1 = {
    x: ".$x.",
    y: ".$y1.",
    name: 'Right',
    type: 'bar'
  };

  var trace2 = {
    x: ".$x.",
    y: ".$y2.",
    name: 'Wrong',
    type: 'bar'
  };

  ";

  ?>
//it puts the data in an array
var data = [trace1, trace2];
//sets the type of layout
var layout = {barmode: 'stack'};
//and creates the graphic and put it in the div with id='myDiv'
Plotly.newPlot('myDiv', data, layout);
</script>
</body>
</html>
