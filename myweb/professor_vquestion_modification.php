<!doctype html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Questions - modify</title>
    <?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //if the url has no idvq(id of the voice question) parameter this sends the user to professor_vquestion.php
      if (!isset($_GET['idvq'])) {
        header('Location: professor_vquestion.php');
      }
      //this part gets the questions data from the database
      include_once "include/mydb.php";
      $question=getVQuestionData($_GET['idvq'],$_SESSION['userid']);
      //if the querys result is emty this sends the user to professor_vquestion.php(this means that the question wasnt made by this user)
      if (!isset($question['vquestion'])) {
        header('Location: professor_vquestion.php');
      }
      echo '<script>var idvq="'.$_GET['idvq'].'";</script>';
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/multi.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablevquestiongroup.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/multi.js"></script>
    <script src="js/optionsbuttonstable.js"></script>
    <script src="js/sendparams.js"></script>
    <script src="js/numericvaliadtion.js"></script>
    <script src="js/imageuploadcreatecriterions.js"></script>
    <script> 
    //this method loads the selected groups and select them at the datatable of groups
    function loadgroup(){
      <?php
        $groups=getTableGroupsWithVQ($_SESSION['userid'],$_GET['idvq']);
        $i=0;
        //first of all, this makes an array(in a string to give it to js)
        $x='[';
        // every row at the datatable at the bottom has an id like id='group-*id of the group*'
        //this while makes an array for js with the ids of the groups selected to answer/evaluate the question the user is modifying
        while(isset($groups[$i])){
          $x.='"group-'.$groups[$i]['id_group'].'",';
          $i++;
        }
        //this is to take in care the case of no group selected. It can be if the group was deleted or if no student was selected to answer the question
        //this deletes the last coma added at the last while and finishes the string(array in it) adding the ']' character
        if ($i>0) {
          $x=substr($x, 0,strlen($x)-1).']';
        }
        else{
          $x.=']';
        }
        //after the preparation of the variable this sets it
        echo 'var loadgrup='.$x.';';
      ?>
      var i=0;
      //and while theres a group at the array this makes the checkbox of the selected group checked
      while(loadgrup[i]){
        document.getElementById(loadgrup[i]).children[4].children[0].checked=true;
        i++;
      }

    }</script>
  </head>
  <body>
    <?php include("include/header.html") ?>
    <div><center>
      <div>
        <h2>Modify question</h2>
      </div>
      <div>
        <!--this loads the question in the text area. The replacement of the \n is needed because it would appear in the text as '/n' and not as a line break-->
        <label>Write the question:</label><br>
        <textarea cols=60 rows=5 id="question" name="question" ><?php echo ''.str_replace("\\n", "\n", $question['vquestion']);?></textarea><br><br>
      </div>
      <div>
        <div>
          <!--in this part the data of the question is represented in inputs and the old image is showed to the user with the path in a label
          but the user can change it pressing the button. It calls a function wich opens an iframe to select the file and upload it(at imageuploadcreatecriterions.js)-->
          <label>Maximun number of answers per student for this question:</label><br>
          <input type="text" id='maxans' value=<?php echo "'".$question['max_answer']."'"; ?> class="id" size="2" onkeypress="return valida(event)" ><br><br>
          <label>Deadline:</label><br>
          <input type="datetime-local" id="deadline" name="deadline" min=<?php echo '"'.date('Y-m-d').'T'.date('H:i').'"';?> value=<?php echo '"'.substr($question['deadline'], 0,10).'T'.substr($question['deadline'], 11,5).'"';?> required><br><br>
          <label>Image path:</label><label id="imagepath"><?php echo ''.$question['image_path'];?></label><br>
          <?php if ($question['image_path']!=NULL) { ?>
          <label>Old image:</label><br>
          <img src=<?php $pos=strpos($question['image_path'], '/myweb/');
          echo "'..".substr($question['image_path'], $pos)."'";?> width="300" height="300"><br><?php } ?>
          <input type="button" value="Upload image" onclick='selectImage()'>
        </div>
      </div>
        <div class="container">
          <h3>Select criterion</h3>
          <!--the functions of creation and deletion of criterions is made at iframes opened in those functions at onclick(they are at imageuploadcreatecriterions.js)-->
          <input type="button" value="Create criterion" onclick='createCrit()'>&nbsp;&nbsp;&nbsp;
          <input type="button" value="Delete criterion" onclick='deleteCrit()'>
          <br><br>
            <select multiple="multiple" name="criterions" id="criterions">
              <?php 
                //this gets the criterions created by the user and gets the criterions selected to evaluate by the students (remember that this is the modification of a voice question)
                $criterion=getVQCriterion($_SESSION['userid']);
                $criterionselected=getVQCriterionSelected($_GET['idvq']);
                $i=0;
                while(isset($criterion[$i])){
                  $j=0;
                  $flag=false;
                  //while theres a criterion this searches for the selected criterions
                  while (isset($criterionselected[$j]) && !$flag) {
                    //of it finds a selected criterion it puts it as selected and it makes to end this while seting flag=true
                    if ($criterion[$i]['id_criterion']==$criterionselected[$j]['id_criterion']) {
                      echo '<option id="crit-'.$criterion[$i]['id_criterion'].'" title="'.$criterion[$i]['criterion'].'"  selected="selected">'.$criterion[$i]['id_criterion'].".- ".substr($criterion[$i]['criterion'], 0,25).'...</option>';
                      $flag=true;
                    }
                    $j++;
                  }
                  //if the criterion wasnt selected(flag==false) it creates the option of the criterion as not selected
                  if(!$flag){
                    echo '<option id="crit-'.$criterion[$i]['id_criterion'].'" title="'.$criterion[$i]['criterion'].'">'.$criterion[$i]['id_criterion'].".- ".substr($criterion[$i]['criterion'], 0,25).'...</option>';
                  }
                  $i++;
                }
              ?>
            </select>
            <br>
        </div>
        <script>
            //after creating the criterions this gives the select box that cool appearence
            var select = document.getElementById('criterions');
            multi( select );
        </script>

        <div>
          <h3>Groups of students</h3>
          <!--the functions of this buttons are at optionsbuttonstable.js-->
          <input type="button" name="selectallgroup" value="Select all" onclick='selectAll()'>&nbsp;&nbsp;&nbsp;
          <input type="button" name="deselectallgroup" value="Unselect all" onclick='deselectAll()'><br><br>
        </div>
        <!--this button calls a function at sendparams.js sending the id of the question and where to go. That function adds information to the hidden form at the bottom -->
        <input type="button" name="tostudent" value="Next step" onclick=<?php echo'"nextStep('."'".'professor_vquestion_selecstudent.php?idvq='.$_GET['idvq']."'".')"';?>>
        <br><br>
        <section>
          <!--this tables body is loaded at functionstablevquestiongroup.js-->
          <table id="tablequestgroup">
            <thead>
              <tr>
                <th>ID</th>
                <th>Group name</th>
                <th>Number of students</th>
                <th>Check group members</th>
                <th>Select</th>
              </tr>
            </thead>
          </table>
        </section>
    </center></div>
    <div id="hidden_form_container" style="display:none;"></div>
  </body>
</html>