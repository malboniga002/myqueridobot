<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //this gets the data for the graphic
      include_once "include/mydb.php";
      $std=getGraphic1Data1($_SESSION['userid']);
      if (!isset($std[0])) {
        header('Location: professor_graphics.php');
      }
      //after this  it calls to plotlys js librarys
    ?>
    <title>Graphic</title>
	<!-- Latest compiled and minified plotly.js JavaScript -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.min.js"></script>

<!-- OR use a specific plotly.js release (e.g. version 1.5.0) -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-1.5.0.min.js"></script>

<!-- OR an un-minified version is also available -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.js"></script>
</head>
<body>
<center>
  <h2>Graphic of answered questions in quizzes per student</h2>
<div id="myDiv"></div>
</center>
	<script>
  document.getElementById("myDiv").style.height=window.window.innerHeight-70;
  document.getElementById("myDiv").style.width=window.innerWidth-40;
  <?php
  //the data will be in js arrays. This makes strings of those arrays
  $i=0;
  $x="[";
  //first it gets all the quizzes
  $quiz=getQuizzes();
  $j=0;
  $y=[];
  //for every quizz it makes an array of string
  while (isset($quiz[$j])) {
    $y[$j]='[';
    $j++;
  }
  //for all the student it searchs the number of questions of a specific quizz he/she has answered and adds the data to de y array
  // something like: y=[[2,3,4,1,5],[0,2,1,5,0]]<- the student[0] has answered 2 questions of the first quizz and 0 from the second
  //the student[1] has answered 3 questions of the first quizz and 2 from the second...
  while(isset($std[$i])){
    $x.='"'.$std[$i]['first_name']." ".$std[$i]['last_name'].' id='.$std[$i]['id_student'].'",';
    $j=0;
    while (isset($quiz[$j])) {
      $questions=getDoneQuizQuestions($std[$i]['id_student'],$quiz[$j]['id_quiz']);
      $y[$j].=$questions['question'].',';
      $j++;
    }
    $i++;
  }
  $x=substr($x, 0,strlen($x)-1);
  $x.="]";
  //after that this makes all the variables to make the graphic, it makes a variable trace per quizz and after that it adds that variable to a array(in string to pass it to js)
  //in all this it removes the last coma and adds the ']' character (to end the array)
  $j=0;
  $trace='[';
  while (isset($quiz[$j])) {
    $y[$j]=substr($y[$j], 0,strlen($y[$j])-1);
    $y[$j].=']';
    echo "var trace".$j." = {
      x: ".$x.",
      y: ".$y[$j].",
      name: '".utf8_encode($quiz[$j]['theme'])."',
      type: 'bar'
    };";
    $trace.="trace".$j.",";
    $j++;
  }
  $trace=substr($trace, 0,strlen($trace)-1);
  $trace.=']';
  //after that it creates the variable data in js with all the data, ready to give it to plotlys library to make the graphic
  echo "var data =".$trace.";";

  ?>
  //sets the type of layout
var layout = {barmode: 'stack'};
//and creates the graphic and put it in the div with id='myDiv'
Plotly.newPlot('myDiv', data, layout);
</script>
</body>
</html>
