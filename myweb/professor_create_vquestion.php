<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Questions - Create</title>
    <?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      include_once "include/mydb.php";
      
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/multi.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablevquestiongroup.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/multi.js"></script>
    <script src="js/optionsbuttonstable.js"></script>
    <script src="js/sendparams.js"></script>
    <script src="js/numericvaliadtion.js"></script>
    <script src="js/imageuploadcreatecriterions.js"></script>
    <script>var idvq=0;</script>
  </head>
  <body>
  	<?php include("include/header.html") ?>
  	<div><center>
  		<div>
  			<h2>Create question</h2>
  		</div>
      <div>
        <div>
          <!--here theres a text area to write the question, the input of the deadline with the next week as default value...-->
          <label>Write the question:</label><br>
          <textarea cols=60 rows=5 id="question" name="question"></textarea><br><br>
        </div>
        <div>
          <label>Maximun number of answers per student:</label><br>
          <input type="text" id='maxans' value="10" class="id" size="2" onkeypress="return valida(event)" ><br><br>
          <label>Deadline:</label><br>
          <input type="datetime-local" id="deadline" name="deadline" min=<?php echo '"'.date('Y-m-d').'T'.date('H:i').'"';?> value=<?php echo '"'.date('Y-m-d',strtotime("+1 week")).'T'.date('H:i').'"';?> required><br><br>
          <!--here theres a label that is changed by the iframe that te user opens when he/she want to upload an image for the voice question(the button is what opens the iframe)-->
          <label>Image:</label><label id="imagepath"></label><br>
          <input type="button" value="Upload image" onclick='selectImage()'>
        </div>
      </div>
      <!--with the buttons of create and delete the user opens the iframes to ask for the data to create or elete a criterion-->
        <div class="container">
          <h3>Select criterion</h3>
          <input type="button" value="Create criterion" onclick='createCrit()'>&nbsp;&nbsp;&nbsp;
          <input type="button" value="Delete criterion" onclick='deleteCrit()'>
          <br><br>
            <select multiple="multiple" name="criterions" id="criterions">
              <?php 
                //while there is a criterion created by the user(professor) this makes an option. The user only can select his/her criterions
                $criterion=getVQCriterion($_SESSION['userid']);
                $i=0;
                while(isset($criterion[$i])){
                  echo '<option id="crit-'.$criterion[$i]['id_criterion'].'" title="'.$criterion[$i]['criterion'].'">'.$criterion[$i]['id_criterion'].".- ".substr($criterion[$i]['criterion'], 0,25).'</option>';
                  $i++;
                }
              ?>
            </select>
            <br>
        </div>
        <script>
        //this script part is to make the select box like it is. Its from the multi library made by Fabianlindfors,harrykiselev,okon3 and mystrdat
        // https://github.com/Fabianlindfors/multi.js
        //its cool, right? XD
            var select = document.getElementById('criterions');
            multi( select );
        </script>

    		<div>
          <!--this is the part of the table, the data is loaded  by a js(functionstablevquestiongroup.js) and the buttons functions are in optionsbuttonstable.js-->
          <h3>Group of students</h3>
    			<input type="button" name="selectallgroup" value="Select all" onclick='selectAll()'>&nbsp;&nbsp;&nbsp;
    			<input type="button" name="deselectallgroup" value="Unselect all" onclick='deselectAll()'><br><br>
    		</div>
        <input type="button" name="tostudent" value="Next step" onclick='nextStep("professor_vquestion_selecstudent.php")'>
        <br><br>
    		<section>
    			<table id="tablequestgroup">
  	  			<thead>
  	  				<tr>
  	  					<th>ID</th>
  	  					<th>Group name</th>
  	  					<th>Number of students</th>
                <th>Check group members</th>
  	  					<th>Select</th>
  	  				</tr>
  	  			</thead>
  	  		</table>
    		</section>
      </center></div>
    <div id="hidden_form_container" style="display:none;"></div>
  </body>
</html>