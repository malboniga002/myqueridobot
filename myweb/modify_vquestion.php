<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<?php
    //start the sesion. if there isnt the attribute of the session it resends the user to the login 
	session_start(); 
    if(!isset($_SESSION['userid'])){
    	header('Location: index.php');
    }
    //if it isnt set one of the attributes that this file needs it sends the user to professor_vquestion.php
    if (!isset($_POST['question'])) {
    	header('Location:professor_vquestion.php');
    }
    require_once "include/mydb.php";
    //after including the file with the methods to connect to the database it calls one of those methods to modify the voice question
    modifyVQuestion($_POST['idvq'],$_POST['question'],$_POST['deadline'],$_POST['uploadedfile'],$_POST['maxans'],$_SESSION['userid']);
    //some of the received data are arrays, but to send them to this page they are in strings separated by ; or ,
    //the students variable is an array of arrays, thats why after this explode there is another one
    //the real format of that array is: array=[['student evaluated id','the first student evaluators id','the first student evaluators id',...],['student evaluated id'],...]
    //the first position of the array(array[0][0] in the example) after the explode has the id of the student who has to answer the question
    //the rest are his/her evaluators, but if theres no evaluator(the second position of the example array[1]), that means that the student there doesnt have to answer the question(he/she wasnt selected in the list to answer the question)
    $students=explode(";", $_POST['students']);
    $criterions=explode(",", $_POST['selecriterions']);
    //while there are students
    $i=0;
    while(isset($students[$i])){
    	$j=1;
        //if they were selected to answer the voice question it inserts it in the database(it doesn't mind if the student has assigned the question because a student only can have a question once(the keys in the database can't be repeated))
        $evaluators=explode(",", $students[$i]);
        //this deletes the students evaluators
        deleteGEvaluation($_POST['idvq'],$evaluators[0]);
        if (isset($evaluators[$j])) {
            insertGQuestion($_POST['idvq'],$evaluators[0]);
            //it inserts students evaluators and the criterion the evaluators have to evaluate
        	while (isset($evaluators[$j])) {
        		$r=0;
        		while (isset($criterions[$r])) {
                    //this inserts the student evaluators and the criterions they have to evaluate
        			 insertGEvaluation($evaluators[0],$evaluators[$j],$criterions[$r],$_POST['idvq']);
        			$r++;
        		}
        		$j++;
        	}
        }
        //if theres no evaluator it deletes the relation of the question and the student if the student was selected to answer the question before the modification
        else{
            deleteGQuestion($_POST['idvq'],$evaluators[0]);
        }
    	$i++;
    }
    //after that it sends the user to professor_vquestion.php
    header('Location:professor_vquestion.php');
?>
