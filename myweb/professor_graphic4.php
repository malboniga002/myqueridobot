<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //get the users students
      include_once "include/mydb.php";
      $std=getGraphic1Data1($_SESSION['userid']);
      //after this  it calls to plotlys js librarys 
    ?>
    <title>Graphics</title>
	<!-- Latest compiled and minified plotly.js JavaScript -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.min.js"></script>

<!-- OR use a specific plotly.js release (e.g. version 1.5.0) -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-1.5.0.min.js"></script>

<!-- OR an un-minified version is also available -->
<script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.js"></script>
</head>
<body>
<center>
  <h2>Graphic of the quizz answers given per student during the last 12 months</h2>
<div id="myDiv"></div>
</center>
	<script>
  document.getElementById("myDiv").style.height=window.window.innerHeight-70;
  document.getElementById("myDiv").style.width=window.innerWidth-40;
  <?php
  //this php code prepares the data for plotly
  
  //the data will be in js arrays. This makes strings of those arrays
  $trace="[";
  $j=0;
  while (isset($std[$j])) {
    $x="[";
    $y="[";
    $i=11;
    while($i>=0){
      //get the data from the database
      $date=getGraphic4Data($i,$std[$j]['id_student']);
      //conversion of the month and adding to the string
      if($date['month']=='12' || $date['month']=='0'){$x.='"December",';}
      if($date['month']=='11' || $date['month']=='-1'){$x.='"November",';}
      if($date['month']=='10' || $date['month']=='-2'){$x.='"October",';}
      if($date['month']=='9' || $date['month']=='-3'){$x.='"September",';}
      if($date['month']=='8' || $date['month']=='-4'){$x.='"August",';}
      if($date['month']=='7' || $date['month']=='-5'){$x.='"July",';}
      if($date['month']=='6' || $date['month']=='-6'){$x.='"June",';}
      if($date['month']=='5' || $date['month']=='-7'){$x.='"May",';}
      if($date['month']=='4' || $date['month']=='-8'){$x.='"April",';}
      if($date['month']=='3' || $date['month']=='-9'){$x.='"March",';}
      if($date['month']=='2' || $date['month']=='-10'){$x.='"February",';}
      if($date['month']=='1'){$x.='"January",';}
      //adding the data of the student to the second string
      $y.=$date['count'].',';
      $i--;
    }
    //after having all the data this removes the last coma in the data and adds the ']' character to end the array
    $x=substr($x, 0,strlen($x)-1);
    $y=substr($y, 0,strlen($y)-1);
    $x.="]";
    $y.="]";
    //now the variable of the data of one line(of this student) is created. This variable will be sent to plotly in the next string variable(array for js)
    echo "var trace".$j." = {
      type: 'scatter',
      x: ".$x.",
      y: ".$y.",
      name: '".$std[$j]['first_name']." ".$std[$j]['last_name']." id=".$std[$j]['id_student']."',
    };
    ";
    $trace.='trace'.$j.",";
    $j++;
  }
  //after having the data of all the students this removes the last coma and adds the end character of the array and set that string(array) in a js variable for plotly
  $trace=substr($trace, 0,strlen($trace)-1)."]";
  echo "var data=".$trace.";";

  ?>
//sets the type of layout
var layout = {
  width: window.innerWidth-40,
  height: window.window.innerHeight-70
};
//and creates the graphic and put it in the div with id='myDiv'
Plotly.newPlot('myDiv', data, layout);
</script>
</body>
</html>
