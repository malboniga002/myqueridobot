<html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<head>
	<meta charset="utf-8">
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //if there isnt the data that this iframe have to load it closes
      if(!isset($_GET['gid'])){
        echo "parent.document.getElementById('studentList').remove();";
      }
      echo '<script>var idg="'.$_GET['gid'].'";</script>';
      require_once "include/mydb.php";
      $gname=getGroupName($_GET['gid'],$_SESSION['userid']);
      if (!isset($gname["group_name"])) {
        echo "parent.document.getElementById('studentList').remove();";
      }
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablegroup.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
</head>
<body>
<div><center>
  <!--this is the name of the group-->
<h2>Group: <?php echo $gname['group_name'];?></h2><br>
<section>
  <!--the data of this table is loaded from functionstablegroup.js-->
  	<table id="tablegroup">
	  	<thead>
	  		<tr>
          <th>ID</th>
          <th>Student</th>
	  			<th>Questions to answer</th>
	  			<th>Answers to evaluate</th>
	  		</tr>
	  	</thead>
	  </table>
</section>
<div><br>
    <!--this button closes the iframe-->
    <input type="button" name="close" value="Close" onclick='closeGroup()'>
</div>
</center></div>
</body>
</html>