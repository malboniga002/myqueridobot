<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
	<meta charset="utf-8">
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //if the parameters arent set this closes this window
      if(!isset($_GET['stdname']) || !isset($_GET['stdid'])){
        echo '<script>self.close();</script>';
      }
      echo '<script>var idstd="'.$_GET['stdid'].'";</script>'
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablevquestionevalstudent.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
</head>
<body>
<div><center>
  <!--this writes the student name at the top of this iframe-->
<h2>Evaluators of <b><?php echo $_GET['stdname'];?></b></h2>
<div>
  <!--this buttons functions are at functionstablevquestionevalstudent.js-->
    <input type="button" name="random" value="Random selection" onclick='selectRandom()'>&nbsp;&nbsp;&nbsp;
    <input type="button" name="random" value="Random selection of students who have already answered" onclick='selectAnsweredRandom()'>&nbsp;&nbsp;&nbsp;
    <input type="button" name="select" value="Select all" onclick='selectAll()'>&nbsp;&nbsp;&nbsp;
    <input type="button" name="deselect" value="Unselect all" onclick='deselectAll()'><br><br>
</div>

<section>
  <!--this tables bodys data is loaded from functionstablevquestionevalstudent.js-->
  	<table id="tablevquestevalstudentprof">
	  	<thead>
	  		<tr>
          <th>ID</th>
          <th>Student</th>
	  			<th>Questions to answer</th>
	  			<th>Answers to evaluate</th>
          <th>Answered this question</th>
	  			<th>Select</th>
	  		</tr>
	  	</thead>
	  </table>
</section>
<div><br>
  <!--this buttons functions are in functionstablevquestionevalstudent.js-->
    <input type="button" name="cancel" value="Cancel" onclick='cancelChanges()'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" name="save" value="Save" onclick='saveChanges()'><br><br>
</div>
</center></div>
</body>
</html>