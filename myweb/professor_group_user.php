<html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<head>
	<meta charset="utf-8">
	<?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //if theres no idg parameter at the url this closes the iframe
      if(!isset($_GET['idg'])){
        echo '<script>self.close();</script>';
      }
      echo '<script>var idg="'.$_GET['idg'].'";</script>'
    ?>
  <?php  
  include_once "include/mydb.php";
  //if theres information sent by the form at the bottom of this file
  if(isset($_POST["ids"])){
  
  //the user ids(this users of the bot are going to become students of the user using the webpage(professor) ) are separated by comas in a string(got the data form a method at the botom), with this it makes an array of user ids
  $ids=explode(',', $_POST["ids"]);
  $i=0;
  //while theres a user id in the array it in the database as student in the group with the id of the get parameter
  while(isset($ids[$i])){
    insertStudent($ids[$i],$_GET['idg']);
    $i++;
  }
  //after adding them it reloads the parent page
  echo "<script>parent.location.reload();</script>";
}
else
{?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablegroupuser.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/optionsbuttonstable.js"></script>
</head>
<body>
<div><center>
  <?php 
    //this gets the name of the group
    $gname=getGroupName($_GET['idg'],$_SESSION['userid']);
  ?>
  <!--this writes the group name at the top of this iframe-->
<h2>Group: <b><?php echo $gname['group_name'];?></b></h2>
<div>
  <!--this buttons functions are at optionsbuttonstable.js-->
    <input type="button" name="select" value="Select all" onclick='selectAll()'>&nbsp;&nbsp;&nbsp;
    <input type="button" name="deselect" value="Unselect all" onclick='deselectAll()'><br><br>
</div>
<section>
  <!--this tables bodys data is loaded from functionstablegroupuser.js-->
  	<table id="tablegroupuserprof">
	  	<thead>
	  		<tr>
          <th>ID</th>
          <th>User first name</th>
          <th>User last name</th>
	  			<th>Select</th>
	  		</tr>
	  	</thead>
	  </table>
</section>
<div><br>
  <!--this buttons functions are in functionstablegroupuser.js-->
    <input type="button" name="cancel" value="Cancel" onclick='cancelChanges()'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" name="save" value="Save" onclick='saveChanges()'>
</div>
<div id="hidden_form_container" style="display:none;"></div>
</center></div>
</body>
</html>
<?php } ?>