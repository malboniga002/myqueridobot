<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this file is called from functionstablevquestionstudent.js with ajax. If this file does not get the parameter tag it does nothing
	if(isset($_POST["tag"])){
		try {
			//if the user hasnt start the sesion this sends him/her to the login
			session_start();
			if(!isset($_SESSION['userid'])){
        		header('Location: ../index.php');
      		}
      		//this gets the data needed for the datatable of students who have to answer a voice question and after adding it to an array it prints it as a json.
      		//functionstablevquestionstudent.js will get the data to fill the body of the datatable
			require_once "mydb.php";
			$json = array();
			$row = getTableVQuestionStudents($_POST['pgroups']);
			$i=0;
			$max=-1;
			$aux=0;
			while(isset($row[$i])){
					$json[$i] = array(
					"Idstudent" => $row[$i]["id_student"],
					"Stname" => $row[$i]["first_name"]." ".$row[$i]["last_name"],
					"Questdone" => getQuestionsStudents($row[$i]["id_student"])
					);
					if ($_POST["pidvq"]==0) {
						$json[$i]["Questionisdone"]="No";
					}
					else{
						$json[$i]["Questionisdone"]=getDoneVoiceQuestion($row[$i]["id_student"],$_POST["pidvq"]);
					}
					$aux=(int)getAllGroupQuestion($row[$i]["id_student"]);
					if ($aux>$max) {
						if($max==-1){
							$min=$aux;
						}
						$max=$aux;
					}
					if ($min>$aux) {
						$min=$aux;
					}
					$json[$i]['Quest']=$aux."";
				$i++;
			}
			$json["maxmin"]=$max-$min;
			$json["min"]=$min;
			$json["success"] = true;
			echo json_encode($json);
		} catch (PDOException $e) {
			echo "Error: " .$e->getMessage();
		}
	}

?>

