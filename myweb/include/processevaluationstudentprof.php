<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this file is called from functionstableevaluationstudent.js with ajax. If this file does not get the parameter tag it does nothing
	if(isset($_POST["tag"])){
		try {
			//if the user hasnt start the sesion this sends him/her to the login
			session_start();
			if(!isset($_SESSION['userid'])){
        		header('Location: ../index.php');
      		}
      		//if this file doesnt get the id of the voice answer it sends the user to professor_evaluations.php
      		if(!isset($_POST["getidva"])){
        		header('Location: professor_evaluations.php');
      		}
      		//this gets the data needed for the datatable of evaluations and after adding it to an array it prints it as a json.
      		//functionstableevaluationstudent.js will get the data to fill the body of the datatable
			require_once "mydb.php";
			$json = array();
			$row = getTableEvaluationstudent($_POST["getidva"]);
			$i=0;
			//after getting the data as string this converts it to a array of criterions ids
			$crits=explode(",",$_POST["crit"]);
			while(isset($row[$i])){
				//first it creates the array with the id of the student and his/her name
				$json[$i] = array(
					"Idstd" =>$row[$i]["id_student"],
					"STname" => "".$row[$i]["first_name"]." ".$row[$i]["last_name"],
				);
				$j=0;
				$text="";
				//and here it gets for all the grades given in the evaluations of the criterions
				while (isset($crits[$j])) {
					//but if the criterion was not evaluated it adds a '-'
					$grade=getEvalGrade($crits[$j],$row[$i]["id_student"],$_POST["getidva"]);
					if (isset($grade['grade'])) {
						$text.=$grade['grade'].",";
					}
					else{
						$text.='-,';
					}
					$j++;
				}
				$json[$i]['grade']=substr($text, 0,strlen($text)-1);
				//this part is to get the voice explanation of the evaluation. if theres no voice explanation it writes '-' character
				$explanation=getTableEvaluationexplanation($_POST["getidva"],$row[$i]["id_student"]);
				if (isset($explanation['file_path'])) {
					$json[$i]['explain']=".".$explanation['file_path'];
				}
				else{
					$json[$i]['explain']="-";
				}
				$i++;
			}
			$json["success"] = true;
			echo json_encode($json);
		} catch (PDOException $e) {
			echo "Error: " .$e->getMessage();
		}
	}

?>

