<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
/**
* Method to get the data of a voice question.
*
* @param string|int     $id        ID of the voice question.
* @param string|int     $session   ID of the professor who created the voice question.
*
* @return array|bool    The array with the data of the question.
*/
function getVQuestion($id,$session){
	include("../Globals.php");
	$query='select * from voice_question where id_vquestion=:id and id_prof=:session';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id', $id, PDO::PARAM_INT);
    $sth->bindParam(':session', $session, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the data of all the voice questions created by a professor.
*
* @param string|int     $id_prof        ID of the professor.
*
* @return array|bool    The array with the data of the questions.
*/
function getTableVQuestion($id_prof){
	include("../../Globals.php");
	$query='select vquestion,creation_date,deadline,id_vquestion from voice_question where id_prof=:id_prof ';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the name of the group of students who have the voice question to answer it.
*
* @param string|int     $id_vquestion        ID of the voice question.
*
* @return string    String with the name of the the groups.
*/
function getVQuestionGroup($id_vquestion){
    include("../../Globals.php");
    $query='select student_group.group_name from group_question,student_group,student where group_question.id_vquestion=:id_vquestion and group_question.id_student=student.id_student and student_group.id_group=student.id_group group by student_group.group_name ';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    $groups=$sth->fetchAll(PDO::FETCH_ASSOC);
    $i=0;
    $g="";
    //the data is returned as an string with all the names separated with comas
    while (isset($groups[$i])) {
        $g.=$groups[$i]['group_name'].",";
        $i++;
    }
    if (strlen($g)>0) {
        return substr($g, 0,strlen($g)-1);
    }
    else{
        return "";
    }
}

/**
* Method to get the names of the students who have the voice question to answer it.
*
* @param string|int     $id_vquestion        ID of the voice question.
*
* @return string    String with the names of the students.
*/
function getStudentVquestion($id_vquestion){
    include("../../Globals.php");
    $query='select user.first_name,user.last_name from group_question,student,user where group_question.id_vquestion=:id_vquestion and group_question.id_student=student.id_student and student.id_student=user.id';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    $student=$sth->fetchAll(PDO::FETCH_ASSOC);
    $i=0;
    $s="";
    //the data is returned as an string with all the names of the studens separated with comas
    while (isset($student[$i])) {
        $s.=$student[$i]['first_name']." ".$student[$i]['last_name'].",";
        $i++;
    }
    if (strlen($s)>0) {
        return substr($s, 0,strlen($s)-1);
    }
    else{
        return "";
    }
}

/**
* Method to get the number of students who have to answer the voice question with the id at the parameters.
*
* @param string|int     $id_vquestion        ID of the voice question.
*
* @return string    String with number.
*/
function getVquestionStudentCount($id_vquestion){
    include("../../Globals.php");
    $query='select count(*) as count from group_question where id_vquestion=:id_vquestion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    $student=$sth->fetch(PDO::FETCH_ASSOC);
    return $student['count'];
}

/**
* Method to get the groups of the professor with that id(at the parameters).
*
* @param string|int     $id_prof        ID of the professor.
*
* @return array|bool    The array with the data of the group.
*/
function getTableVQuestionGroup($id_prof){
	include("../../Globals.php");
	$query='select student_group.id_group,student_group.group_name,count(student.id_student) as member from student,student_group where student_group.id_prof=:id_prof and student_group.id_group=student.id_group group by student_group.id_group';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the data of a users which aren't registered in the bot.
*
* @return array|bool    The array with the data of the users.
*/
function getTableUsers(){
    include("../../Globals.php");
    $query='select id,first_name,last_name from user where id not in(select id_student from student)';
    $sth = $pdo->prepare($query);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to insert a student at the database.
*
* @param string|int     $id_student        ID of the student.
* @param string|int     $id_group          ID of the group where the student will be.
*
* @return bool    If something is wrong it returns false.
*/
function insertStudent($id_student,$id_group){
    include("../Globals.php");
    $query='insert into student(id_student,id_group) values(:id_student,:id_group)';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_group', $id_group, PDO::PARAM_INT);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to get the data of the users at the selected groups to assign them a voice question or to make them evaluate another student.
*
* @param string     $idgroup        String with the ids of the groups separated by comas.
*
* @return array|bool    The array with the data of the students at those groups.
*/
function getTableVQuestionStudents($idgroup){
    include("../../Globals.php");
    $groups=explode(",", $idgroup);
    $query='select student.id_student,user.first_name,user.last_name from student,user where student.id_student=user.id and student.id_group=:groups0 ';
    $i=1;
    while(isset($groups[$i])){
        $query.='or student.id_group=:groups'.$i.' ';
        $i++;
    }
    $sth = $pdo->prepare($query);
    $sth->bindParam(':groups0', $groups[0], PDO::PARAM_INT);
    $i=1;
    while(isset($groups[$i])){
        $sth->bindParam(':groups'.$i, $groups[$i], PDO::PARAM_INT);
        $i++;
    }
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the name of the group at the parameters.
*
* @param string|int     $id_group        ID of the group.
* @param string|int     $id_prof         ID of the professor who created the group.
*
* @return array|bool    The array with the name of the group.
*/
function getGroupName($id_group,$id_prof){
    include("../Globals.php");
    $query='select group_name from student_group where id_group=:id_group and id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_group', $id_group, PDO::PARAM_INT);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the criterions to evaluate in a voice question.
*
* @param string|int     $id_vquestion        ID of the question.
*
* @return array|bool    The array with the criterions.
*/
function getVQCriterions($id_vquestion){
    include("../../Globals.php");
    $query='select group_evaluation.id_criterion,criterion.criterion from group_evaluation,criterion where group_evaluation.id_vquestion=:id_vquestion and group_evaluation.id_criterion=criterion.id_criterion group by group_evaluation.id_criterion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    $crit=$sth->fetchAll(PDO::FETCH_ASSOC);
    $i=0;
    $text="";
    while (isset($crit[$i]['criterion'])) {
        $text.=$crit[$i]['criterion'].",";
        $i++;
    }
    if (strlen($text)>0) {
        return substr($text, 0,strlen($text)-1);
    }
    else{
        return "";
    }
}

/**
* Method to get the criterions created by the professor.
*
* @param string|int     $session        ID of the professor.
*
* @return array|bool    The array with the criterions.
*/
function getVQCriterion($session){
	include("../Globals.php");
	$query='select id_criterion,criterion from criterion where id_prof=:session';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':session', $session, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the number of questions that a student has to evaluate.
*
* @param string|int     $student        ID of the student.
*
* @return string   String whith the number of questions that the student has to evaluate(still not evaluated).
*/
function getEvalStudents($student){
    include("../../Globals.php");
    $query='select count(*) as all from group_evaluation where id_student_evaluator=:student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':student', $student, PDO::PARAM_INT);
    $sth->execute();
    $all=$sth->fetch(PDO::FETCH_ASSOC);
    $query='select count(voice_grades.*) as done from group_evaluation,voice_grades where group_evaluation.id_student_evaluator=:student and group_evaluation.id_eval=voice_grades.id_eval';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':student', $student, PDO::PARAM_INT);
    $sth->execute();
    $done=$sth->fetch(PDO::FETCH_ASSOC);
    return "".((int)$all['all']-(int)$done['done']);
}

/**
* Method to get the number of questions that a student has to answer.
*
* @param string|int     $student        ID of the student.
*
* @return string   String whith the number of questions that the student has to answer(still not answered).
*/
function getQuestionsStudents($student){
    include("../../Globals.php");
    $query='select count(*) as ans from voice_answer where id_student=:student and selected="T"';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':student', $student, PDO::PARAM_INT);
    $sth->execute();
    $ans= $sth->fetch(PDO::FETCH_ASSOC);
    $query='select count(*) as quest from group_question where id_student=:student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':student', $student, PDO::PARAM_INT);
    $sth->execute();
    $ques= $sth->fetch(PDO::FETCH_ASSOC);
    return "".((int)$ques['quest']-(int)$ans['ans']);
}

/**
* Method to get the number of questions that a student has assigned(all, not only the questions which he/she has now).
*
* @param string|int     $id_student        ID of the student.
*
* @return string   String whith the number of questions that a student has assigned(all).
*/
function getAllGroupQuestion($id_student){
    include("../../Globals.php");
    $query='select count(*) as question from group_question where id_student=:id_student ';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->execute();
    $ques=$sth->fetch(PDO::FETCH_ASSOC);
    return $ques['question'];
}

/**
* Method to get the options of the professor with the id at the parameters.
*
* @param string|int     $id_prof        ID of the professor.
* @param string         $golb           The path to the the file Globals.php.
*
* @return array|bool    The array with the options of the professor.
*/
function getProfOptions($id_prof,$golb){
    include($golb);
    $query='select * from professor where id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to insert in the database a voice question and get the ID of that question.
*
* @param string        $question        Text of the voice question to insert it.
* @param string|int    $deadline        The timestamp of the deadline of the voice question.
* @param string        $imagepath       Path to the image in the server.
* @param string|int    $maxans          Max number of answer the students can give to the voice question.
* @param string|int    $id_prof         ID of the professor who has created the question.
*
* @return array|bool    The array with the ID of the new voice question.
*/
function insertVQuestion($question,$deadline,$imagepath,$maxans,$id_prof){
    include("../Globals.php");
    //if theres no image path it inserts null
    if ($imagepath=="") {
        $query='insert into voice_question(id_prof,deadline,max_answer,vquestion) values(:id_prof,FROM_UNIXTIME(:deadline),:maxans,:question)';
    }
    else{
        $query='insert into voice_question(id_prof,deadline,image_path,max_answer,vquestion) values(:id_prof,FROM_UNIXTIME(:deadline),:imagepath,:maxans,:question)';
    }
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':maxans', $maxans, PDO::PARAM_INT);
    $sth->bindParam(':question', str_replace('\\n', "\n", $question), PDO::PARAM_STR);
    $sth->bindParam(':deadline', strtotime($deadline.":00"), PDO::PARAM_INT);
    if ($imagepath!="") {
        $sth->bindParam(':imagepath', $imagepath, PDO::PARAM_STR);
    }
    $sth->execute();
    $query='select id_vquestion from voice_question where id_prof=:id_prof and vquestion=:question';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':question', $question, PDO::PARAM_STR);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to insert the assignation of a question to a student to answer it.
*
* @param string|int     $id_question        ID of the question to assign to a student.
* @param string|int     $id_student         ID of the student who we want to assign the question to answer.
*
* @return bool    False if it fails at insertion.
*/
function insertGQuestion($id_question,$id_student){
    include("../Globals.php");
    $query='insert into group_question(id_vquestion,id_student) values(:id_question,:id_student)';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_question', $id_question, PDO::PARAM_INT);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to delete the assignation of a question to a student.
*
* @param string|int     $id_question        ID of the question to delete the assignation to a student.
* @param string|int     $id_student         ID of the student who we want to delete the assigntion of the question to answer.
*
* @return array|bool    False if it fails at insertion.
*/
function deleteGQuestion($id_question,$id_student){
    include("../Globals.php");
    $query='delete from group_question where id_vquestion=:id_question and id_student=:id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_question', $id_question, PDO::PARAM_INT);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to delete the assignation of a question to a student.
*
* @param string|int     $id_question        ID of the question to delete the assignation to a student.
* @param string|int     $id_student         ID of the student who we want to delete the assigntion of the question to answer.
*
* @return array|bool    False if it fails at insertion.
*/
function deleteGEvaluation($id_question,$id_student){
    include("../Globals.php");
    $query='delete from group_evaluation where id_vquestion=:id_question and id_student_evaluated=:id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_question', $id_question, PDO::PARAM_INT);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to insert an evaluation(not the grade).
*
* @param string|int     $evaluated        ID of the student whose answer is going to be evaluated.
* @param string|int     $evaluator        ID of the student who is going to evaluate the answer.
* @param string|int     $criterion        ID of the criterion to evaluate.
* @param string|int     $id_question      ID of the voice question which is going to be evaluated.
*
* @return array|bool    False if it fails at insertion.
*/
function insertGEvaluation($evaluated,$evaluator,$criterion,$id_question){
    include("../Globals.php");
    $query='insert into group_evaluation(id_vquestion,id_student_evaluated,id_student_evaluator,id_criterion) values(:id_question,:evaluated,:evaluator,:criterion)';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_question', $id_question, PDO::PARAM_INT);
    $sth->bindParam(':criterion', $criterion, PDO::PARAM_INT);
    $sth->bindParam(':evaluator', $evaluator, PDO::PARAM_INT);
    $sth->bindParam(':evaluated', $evaluated, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to insert a new criterion and it returns the created criterions ID.
*
* @param string         $crit        The text of the new criterion.
* @param string|int     $id_prof     ID of the professor who is creating the criterion.
*
* @return array|bool    The array with the ID of the created criterion.
*/
function insertCriterion($crit,$id_prof){
    include("../Globals.php");
    $query='insert into criterion(criterion,id_prof) values(:crit,:id_prof)';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':crit', $crit, PDO::PARAM_STR);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    $query='select id_criterion from criterion where criterion=:crit and id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':crit', $crit, PDO::PARAM_STR);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to delete a criterion from the database.
*
* @param string         $crit        ID of the criterion to delete.
* @param string|int     $id_prof     ID of the professor who created the criterion.
*
* @return int|bool    The number of rows without the deleted criterion.
*/
function deleteCriterion($crit,$id_prof){
    include("../Globals.php");
    $query='delete from criterion where id_criterion=:crit and id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':crit', $crit, PDO::PARAM_INT);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->rowCount();
}

/**
* Method to delete a voice question.
*
* @param string|int     $id_questions       ID of the voice question to delete.
* @param string|int     $id_prof            ID of the professor who created the voice question.
*
* @return bool    False if something was wrong at the deletion.
*/
function deleteQuestions($id_questions,$id_prof){
    include("../Globals.php");
    $query='delete from voice_question where id_vquestion=:id_questions and id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_questions', $id_questions, PDO::PARAM_INT);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to delete a student(it becomes a user not registered and he can't use the commands of the bot).
*
* @param string|int     $id_questions       ID of the voice question to delete.
* @param string|int     $id_prof            ID of the professor who created the voice question.
*
* @return bool    The array with the ID of the created criterion.
*/
function deleteStudent($id_student){
    include("../Globals.php");
    $query='delete from student where id_student=:id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to get the data of a voice question.
*
* @param string|int     $id_vquestion       ID of the question.
* @param string|int     $id_prof            ID of the professor who created the voice question.
*
* @return array|bool    The array with the data of the voice question.
*/
function getVQuestionData($id_vquestion,$id_prof){
    include("../Globals.php");
    $query='select vquestion,max_answer,image_path,deadline from voice_question where id_vquestion=:id_vquestion and id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to check if a student has answered a voice question.
*
* @param string|int     $id_vquestion       ID of the question.
* @param string|int     $id_student         ID of the student whose answer we are going to check.
*
* @return string    Yes or no, depending if the student gave an answer to the voice question.
*/
function getDoneVoiceQuestion($id_student,$id_vquestion){
    include("../../Globals.php");
    $query='select id_file from voice_answer where id_student=:id_student and id_vquestion=:id_vquestion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    $answer = $sth->fetch(PDO::FETCH_ASSOC);
    if (isset($answer['id_file'])) {
        return "Yes";
    }
    else{
        return "No";
    }
}

/**
* Method to get the ID's of the criterions to evaluate in a voice question.
*
* @param string|int     $id_vquestion       ID of the question.
*
* @return array|bool    The array with the ID's of the criterions.
*/
function getVQCriterionSelected($id_vquestion){
    include("../Globals.php");
    $query='select id_criterion from group_evaluation where id_vquestion=:id_vquestion group by id_criterion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the number of students in a group.
*
* @param string|int     $idgroup       ID of the group.
*
* @return array|bool    The array with the number of students of the group with that ID.
*/
function getMembers($idgroup){
    include("../../Globals.php");
    $query='select count(id_student) as member from student where id_group=:idgroup';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':idgroup', $idgroup, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to modify a voice question.
*
* @param string|int     $id_vquestion   ID of the question to modify.
* @param string         $vquestion      New text of the question.
* @param string|int     $deadline       New deadline of the question.
* @param string         $image_path     New path to an image related to the voice question.
* @param string|int     $max_answer     New number of max answer permited to that voice question.
* @param string|int     $id_prof        ID of the professor who created the voice question.
*
* @return bool    False if something was wrong at the modification.
*/
function modifyVQuestion($id_vquestion,$vquestion,$deadline,$image_path,$max_answer,$id_prof){
    include("../Globals.php");
    //if theres no image path it doesn't change the old image path
    if ($image_path=="") {
        $query='update voice_question set vquestion=:vquestion,deadline=FROM_UNIXTIME(:deadline),max_answer=:max_answer where id_vquestion=:id_vquestion and id_prof=:id_prof';
    }
    else{
        $query='update voice_question set vquestion=:vquestion,deadline=FROM_UNIXTIME(:deadline),image_path=:image_path,max_answer=:max_answer where id_vquestion=:id_vquestion and id_prof=:id_prof';
    }
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':max_answer', $max_answer, PDO::PARAM_INT);
    $sth->bindParam(':vquestion', str_replace('\\n', "\n", $vquestion), PDO::PARAM_STR);
    $sth->bindParam(':deadline', strtotime($deadline.":00"), PDO::PARAM_INT);
    if ($image_path!="") {
        $sth->bindParam(':image_path', $image_path, PDO::PARAM_STR);
    }
    return  $sth->execute();
}

/**
* Method to get the students who have to answer the voice question(with the ID at the parameters) and their evaluators.
*
* @param string|int     $id_vquestion       ID of the voice question.
*
* @return array|bool    The array with the students.
*/
function getStudentSelected($id_vquestion){
    include("../Globals.php");
    $query='select id_student_evaluated,id_student_evaluator from group_evaluation where id_vquestion=:id_vquestion order by id_student_evaluated';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the answers of students to diferents voice questions.
*
* @param string|int     $id_prof       ID of the profesor whose students answers we want to get.
*
* @return array|bool    The array with the students answers.
*/
function getTableEvaluation($id_prof){
    include("../../Globals.php");
    $query='select voice_answer.id_file,voice_answer.file_path,user.first_name,user.last_name,voice_question.vquestion,voice_question.id_vquestion,voice_answer.downloaded_at,count(voice_grades.id_file) as eval from voice_question,voice_answer,student,voice_grades,user where voice_answer.selected="T" and voice_answer.id_student=student.id_student and voice_answer.id_vquestion=voice_question.id_vquestion and voice_question.id_prof=:id_prof and voice_answer.id_file=voice_grades.id_file and student.id_student=user.id group by voice_grades.id_file';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the data of one answer given by a student to a voice question.
*
* @param string|int     $id_prof       ID of the profesor whose students answers we want to get.
* @param string|int     $id_file       ID of the voice answer.
*
* @return array|bool    The array with the students answer.
*/
function getTheAnswerEval($id_prof,$id_file){
    include("../Globals.php");
    $query='select voice_question.vquestion,voice_answer.file_path,user.first_name,user.last_name from voice_question,voice_answer,student,user where voice_answer.id_file=:id_file and voice_question.id_prof=:id_prof and voice_question.id_vquestion=voice_answer.id_vquestion and voice_answer.id_student=student.id_student and user.id=student.id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':id_file', $id_file, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the ID of a evaluation.
*
* @param string|int     $id_student_evaluated       ID of the student whose answer is going to be evaluated.
* @param string|int     $id_student_evaluator       ID of the student who is going to evaluate the answer.
* @param string|int     $criterion                  ID of the criterion which is going to be evaluated.
* @param string|int     $id_question                ID of the voice question.
*
* @return array|bool    The array with the ID of the evaluation.
*/
function getGroupEvaluation($id_student_evaluated,$id_student_evaluator,$criterion,$id_question){
    include("../Globals.php");
    $query='select id_eval from group_evaluation where id_student_evaluator=:id_student_evaluator and id_student_evaluated=:id_student_evaluated and id_vquestion=:id_question and id_criterion=:criterion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student_evaluated', $id_student_evaluated, PDO::PARAM_INT);
    $sth->bindParam(':id_student_evaluator', $id_student_evaluator, PDO::PARAM_INT);
    $sth->bindParam(':criterion', $criterion, PDO::PARAM_INT);
    $sth->bindParam(':id_question', $id_question, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the ID of a evaluation.
*
* @param string|int     $id_student_evaluated       ID of the student whose answer is going to be evaluated.
* @param string|int     $id_student_evaluator       ID of the student who is going to evaluate the answer.
* @param string|int     $criterion                  ID of the criterion which is going to be evaluated.
* @param string|int     $id_question                ID of the voice question.
*
* @return array|bool    The array with the ID of the evaluation.
*/
function getCritEval($id_file){
    include("../Globals.php");
    $query='select criterion.id_criterion,criterion.criterion from voice_grades,criterion,group_evaluation where voice_grades.id_file=:id_file and voice_grades.id_eval=group_evaluation.id_eval and group_evaluation.id_criterion=criterion.id_criterion group by criterion.id_criterion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_file', $id_file, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the average of the califications(in a criterion) of a voice answer.
*
* @param string|int     $id_criterion      ID of the criterion which was evaluated.
* @param string|int     $id_file           ID of the voice answer.
*
* @return array|bool    The array with average.
*/
function getEvalAvg($id_criterion,$id_file){
    include("../Globals.php");
    $query='select AVG(voice_grades.grade) as mean from voice_grades,group_evaluation where group_evaluation.id_criterion=:id_criterion and voice_grades.id_eval=group_evaluation.id_eval and voice_grades.id_file=:id_file';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_criterion', $id_criterion, PDO::PARAM_INT);
    $sth->bindParam(':id_file', $id_file, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the data of the user with the ID of one of his/her voice answers.
*
* @param string|int     $id_file           ID of the voice answer.
*
* @return array|bool    The array with data of the student.
*/
function getTableEvaluationstudent($id_file){
    include("../../Globals.php");
    $query='select student.id_student,user.first_name,user.last_name from group_evaluation,voice_grades,student,user where voice_grades.id_file=:id_file and group_evaluation.id_eval=voice_grades.id_eval and group_evaluation.id_student_evaluator=student.id_student and user.id=student.id_student group by student.id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_file', $id_file, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the path of the file with the voice explanation of a student.
*
* @param string|int     $id_student        ID of the student who gave the voice explanation.
* @param string|int     $id_file           ID of the voice answer which was evaluated and explained why did it get the grades it has.
*
* @return array|bool    The array with path to the file with the voice explanation.
*/
function getTableEvaluationexplanation($id_file,$id_student){
    include("../../Globals.php");
    $query='select file_path from voice_evaluation where id_student_evaluator=:id_student and id_file=:id_file';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_file', $id_file, PDO::PARAM_INT);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the grade that a student gave to a voice answer in a specific criterion.
*
* @param string|int     $id_criterion      ID of the evaluated criterion.
* @param string|int     $id_student        ID of the evaluator student.
* @param string|int     $id_file           ID of the voice answer.
*
* @return array|bool    The array with grade.
*/
function getEvalGrade($id_criterion,$id_student,$id_file){
    include("../../Globals.php");
    $query='select voice_grades.grade from group_evaluation,voice_grades where voice_grades.id_file=:id_file and group_evaluation.id_eval=voice_grades.id_eval and group_evaluation.id_student_evaluator=:id_student and group_evaluation.id_criterion=:id_criterion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_criterion', $id_criterion, PDO::PARAM_INT);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->bindParam(':id_file', $id_file, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the data of the groups created by a professor.
*
* @param string|int     $id_prof      ID of the professor.
*
* @return array|bool    The array with data of the groups.
*/
function getTableGroups($id_prof){
    include("../../Globals.php");
    $query='select group_name,id_group,creation_date from student_group where id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the ID's of the groups that have to answer the voice question(ID of the voice question at the parameters).
*
* @param string|int     $id_prof           ID of the professor.
* @param string|int     $id_vquestion      ID of the voice question.
*
* @return array|bool    The array with the ID's of the groups.
*/
function getTableGroupsWithVQ($id_prof,$id_vquestion){
    include("../Globals.php");
    $query='select student_group.id_group from student,student_group,group_evaluation where group_evaluation.id_vquestion=:id_vquestion and (student.id_student=group_evaluation.id_student_evaluator or student.id_student=group_evaluation.id_student_evaluated) and student.id_group=student_group.id_group and student_group.id_prof=:id_prof group by id_group';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the voice questions assigned to the students of a group.
*
* @param string|int     $id_group           ID of the group.
*
* @return array|bool    The array with the ID's of the questions.
*/
function getGroupQuestions($id_group){
    include("../../Globals.php");
    $query='select group_evaluation.id_vquestion from group_evaluation,student where student.id_group=:id_group and (student.id_student=group_evaluation.id_student_evaluator or student.id_student=group_evaluation.id_student_evaluated) group by group_evaluation.id_vquestion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_group', $id_group, PDO::PARAM_INT);
    $sth->execute();
    $count= $sth->fetchAll(PDO::FETCH_ASSOC);
    return count($count);
}

/**
* Method to delete a group of students.
*
* @param string|int     $id_group           ID of the group.
* @param string|int     $id_prof            ID of the professor who created the group.
*
* @return bool    False if it does not delete the group correctly.
*/
function deleteGroups($id_group,$id_prof){
    include("../Globals.php");
    $query='delete from student_group where id_group=:id_group and id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_group', $id_group, PDO::PARAM_INT);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    return $sth->execute();
}

/**
* Method to insert a new group of students.
*
* @param string         $group_name           Name of the new group.
* @param string|int     $id_prof              ID of the professor who is creating the group.
*
* @return bool    False if it don't create the group correctly.
*/
function insertGroup($group_name,$id_prof){
    include("../Globals.php");
    $query='insert into student_group(group_name,id_prof) values(:group_name,:id_prof)';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':group_name', $group_name, PDO::PARAM_STR);
    return $sth->execute();
}

/**
* Method to update the options of a professor.
*
* @param string|int     $id_prof            ID of the professor whose option you want to change.
* @param string         $language           The language option of the bot(changes the texts of the bot).
* @param string         $autoeval           'T' if the professor wants autoevaluation, else 'F'.
* @param string         $random             'T' if the professor wants to randomize the answers at the quizzes of the bot, else 'F'.
* @param string|int     $maxeval            Maximum number of evaluations that a student can do in a voice question.
* @param string         $scale              The scale that students will use to evaluate other students answers.
*
* @return bool    False if it don't modify the options correctly.
*/
function updateOptions($id_prof,$language,$autoeval,$random,$maxeval,$scale){
    include("../Globals.php");
    $query='update professor set autoeval=:autoeval,max_eval=:maxeval,quiz_answer_random=:random,language=:language,scale=:scale where id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->bindParam(':maxeval', $maxeval, PDO::PARAM_INT);
    $sth->bindParam(':language', $language, PDO::PARAM_STR);
    $sth->bindParam(':autoeval', $autoeval, PDO::PARAM_STR);
    $sth->bindParam(':random', $random, PDO::PARAM_STR);
    $sth->bindParam(':scale', $scale, PDO::PARAM_STR);
    return $sth->execute();
}

/**
* Method to get the data for the graphic "Graphic of answered questions in quizzes per student".
*
* @param string|int     $id_prof           ID of the professor.
*
* @return array|bool    The array with the data needed for the graphic.
*/
function getGraphic1Data1($id_prof){
    include("../Globals.php");
    $query='select student.id_student,user.first_name,user.last_name from student,student_group,user where student_group.id_prof=:id_prof and student.id_group=student.id_group and user.id=student.id_student group by student.id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get the data for the graphic "Graphic of correct/wrong answers in quizzes per student".
*
* @param string|int     $id_prof           ID of the professor.
*
* @return array|bool    The array with the data needed for the graphic.
*/
function getGraphic1Data2($id_student){
    include("../Globals.php");
    $query='SELECT SUM(IF(result="right",1,0)),SUM(IF(result="wrong",1,0)) from answer_history where id_student=:id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get all the quizzes.
*
* @return array|bool    The array with the data of the quizzes.
*/
function getQuizzes(){
    include("../Globals.php");
    $query='select * from quiz';
    $sth = $pdo->prepare($query);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get how many questions of a quiz were done by a student.
*
* @param string|int     $id_student           ID of the student.
* @param string|int     $id_quiz              ID of the quiz.
*
* @return array|bool    The array with number of questions done by the student.
*/
function getDoneQuizQuestions($id_student,$id_quiz){
    include("../Globals.php");
    $query='select count(answer_history.id_student) as question from answer_history,quiz_question where answer_history.id_student=:id_student and quiz_question.id_quiz=:id_quiz and quiz_question.id_question=answer_history.id_question';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->bindParam(':id_quiz', $id_quiz, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the data for the graphic "Graphic of the quizz answers given by students during the last 12 months".
*
* @param string|int     $month           The month which you want to know how many answers does the students gave to the diferent quizzes.
*
* @return array|bool    The array with the data needed for the graphic.
*/
function getGraphic3Data($month){
    include("../Globals.php");
    $query='select count(creation_date) as count,month(curdate())-:month as month from answer_history where month(creation_date)=month(curdate())-:month';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':month', $month, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get the data for the graphic "Graphic of the quizz answers given per student during the last 12 months".
*
* @param string|int     $month           The month which you want to know how many answers does a student gave to the diferent quizzes.
* @param string|int     $id_student      ID of the student.
*
* @return array|bool    The array with the data needed for the graphic.
*/
function getGraphic4Data($month,$id_student){
    include("../Globals.php");
    $query='select count(creation_date) as count,month(curdate())-:month as month from answer_history where month(creation_date)=month(curdate())-:month and id_student=:id_student';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':month', $month, PDO::PARAM_INT);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
* Method to get all the students of a professor".
*
* @param string|int     $id_prof        ID of the professor.   
*
* @return array|bool    The array with the data of the students.
*/
function getAllStudents($id_prof){
    include("../../Globals.php");
    $query='select user.first_name,user.last_name,student_group.group_name,student.id_student from student,student_group,user where student.id_student=user.id and student_group.id_group=student.id_group and student_group.id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get all the students evaluated by the student at the parameters".
*
* @param string|int     $id_student        ID of the student.   
*
* @return array|bool    The array with the data of the students.
*/
function getStudentEvaluator($id_student){
    include("../../Globals.php");
    $query='select user.first_name,user.last_name,group_evaluation.id_student_evaluated,group_evaluation.creation_date,group_evaluation.checked,group_evaluation.id_vquestion,voice_question.vquestion from user,group_evaluation,voice_question where group_evaluation.id_student_evaluator=:id_student and group_evaluation.id_student_evaluated=user.id and group_evaluation.id_vquestion=voice_question.id_vquestion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get all the students who are evaluators of the student at the parameters".
*
* @param string|int     $id_student        ID of the student.   
*
* @return array|bool    The array with the data of the students.
*/
function getStudentEvaluated($id_student){
    include("../../Globals.php");
    $query='select user.first_name,user.last_name,group_evaluation.id_student_evaluator,group_evaluation.creation_date,group_evaluation.checked,group_evaluation.id_vquestion,voice_question.vquestion from user,group_evaluation,voice_question where group_evaluation.id_student_evaluated=:id_student and group_evaluation.id_student_evaluator=user.id and group_evaluation.id_vquestion=voice_question.id_vquestion';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Method to get all the students who are evaluators of the student at the parameters".
*
* @param string|int     $id_student        ID of the student.   
*
* @return array|bool    The array with the data of the students.
*/
function updateChecked($id_prof){
    include("../Globals.php");
    $query='update group_evaluation,student,student_group set group_evaluation.checked="Yes" where group_evaluation.id_student_evaluated=student.id_student and student.id_group=student_group.id_group and student_group.id_prof=:id_prof';
    $sth = $pdo->prepare($query);
    $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
    return $sth->execute();
}

?>