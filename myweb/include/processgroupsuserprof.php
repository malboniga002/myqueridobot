<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this file is called from functionstablegroupuser.js with ajax. If this file does not get the parameter tag it does nothing
	if(isset($_POST["tag"])){
		try {
			//if the user hasn't start the sesion this sends him/her to the login
			session_start(); 
      		if(!isset($_SESSION['userid'])){
        		header('Location: ../index.php');
      		}
      		//this gets the data needed for the datatable of users and after adding it to an array it prints it as a json.
      		//functionstablegroupuser.js will get the data to fill the body of the datatable
			require_once "mydb.php";
			$json = array();
			//after getting the data it adds it the array
			$row = getTableUsers();
			$i=0;
			while(isset($row[$i])){
				$json[$i] = array(
					"Iduser" =>$row[$i]["id"],
					"Userfirstname" => $row[$i]["first_name"]
					);
				if ($row[$i]["last_name"]!=null) {
					$json[$i]["Userlastname"]=$row[$i]["last_name"];
				}	
				$i++;
			}
			$json["success"] = true;
			echo json_encode($json);
		} catch (PDOException $e) {
			echo "Error: " .$e->getMessage();
		}
	}

?>

