<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this file is called from functionstablegroups.js with ajax. If this file does not get the parameter tag it does nothing
	if(isset($_POST["tag"])){
		try {
			//if the user hasnt start the sesion this sends him/her to the login
			session_start(); 
      		if(!isset($_SESSION['userid']) || !isset($_POST["pidst"])){
        		header('Location: ../index.php');
      		}
      		//this gets the data needed for the datatable of students and after adding it to an array it prints it as a json.
      		//functionstablegroups.js will get the data to fill the body of the datatable
			require_once "mydb.php";
			$json = array();
			//after getting the data it adds it the array
			$row = getStudentEvaluator($_POST["pidst"]);
			$i=0;
			while(isset($row[$i])){
				$json[$i] = array(
					"Idstudent" =>$row[$i]["id_student_evaluated"],
					"Quest" => ''.$row[$i]["vquestion"],
					"Questid" => ''.$row[$i]["id_vquestion"],
					"Stname" => $row[$i]["first_name"]." ".$row[$i]["last_name"],
					"CDate" => $row[$i]["creation_date"],
					"DCheck" => $row[$i]["checked"]
					);
				$i++;
			}
			$json["success"] = true;
			echo json_encode($json);
		} catch (PDOException $e) {
			echo "Error: " .$e->getMessage();
		}
	}

?>

