<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this file is called from functionstableevaluationstudent.js with ajax. If this file does not get the parameter tag it does nothing
	if(isset($_POST["tag"])){
		try {
			//if the user hasnt start the sesion this sends him/her to the login
			session_start(); 
		    if(!isset($_SESSION['userid'])){
		    	header('Location: ../index.php');
		    }
		    //this gets the data needed for the datatable of students who evaluate other student and after adding it to an array it prints it as a json.
      		//functionstableevaluationstudent.js will get the data to fill the body of the datatable
			require_once "mydb.php";
			$options=getProfOptions($_SESSION['userid'],"../../Globals.php");
			$json = array();
			$row = getTableVQuestionStudents($_POST['pgroups']);
			$i=0;
			//while it adds the data it takes care of the options of the user(the user wants autoevaluation or not)
			while(isset($row[$i])){
				//if the autoevaluation is selected it adds all the students to the array
				if ($options['autoeval']=="T") {
					$json[$i] = array(
						"Idstudent" => $row[$i]["id_student"],
						"Stname" => ''.$row[$i]["first_name"]." ".$row[$i]["last_name"]
					);
					$student=getEvalStudents($row[$i]["id_student"]);
					$json[$i]['Evals']=$student;
					$student=getQuestionsStudents($row[$i]["id_student"]);
					$json[$i]['Quest']=$student;
					if ($_POST["pidvq"]==0) {
						$json[$i]["Questionisdone"]="No";
					}
					else{
						$json[$i]["Questionisdone"]=getDoneVoiceQuestion($row[$i]["id_student"],$_POST["pidvq"]);
					}
				}
				//else it takes care of not adding the student whos evaluators we are searching
				else{
					if ($row[$i]["id_student"]!=$_POST['stid']) {
						$json[$i] = array(
							"Idstudent" => $row[$i]["id_student"],
							"Stname" => ''.$row[$i]["first_name"]." ".$row[$i]["last_name"]
						);
						$student=getEvalStudents($row[$i]["id_student"]);
						$json[$i]['Evals']=$student;
						$student=getQuestionsStudents($row[$i]["id_student"]);
						$json[$i]['Quest']=$student;
						if ($_POST["pidvq"]==0) {
							$json[$i]["Questionisdone"]="No";
						}
						else{
							$json[$i]["Questionisdone"]=getDoneVoiceQuestion($row[$i]["id_student"],$_POST["pidvq"]);
						}
					}
				}
				
				$i++;
			}
			$json["success"] = true;
			echo json_encode($json);
		} catch (PDOException $e) {
			echo "Error: " .$e->getMessage();
		}
	}
?>