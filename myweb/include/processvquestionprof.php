<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
//this file is called from functionstablevquestion.js with ajax. If this file does not get the parameter tag it does nothing
	if(isset($_POST["tag"])){
		try {
			//if the user hasnt start the sesion this sends him/her to the login
			session_start(); 
      		if(!isset($_SESSION['userid'])){
        		header('Location: ../index.php');
      		}
      		//this gets the data needed for the datatable of voice questions and after adding it to an array it prints it as a json.
      		//functionstablevquestion.js will get the data to fill the body of the datatable
			require_once "mydb.php";
			$json = array();
			$row = getTableVQuestion($_SESSION['userid']);
			$i=0;
			while(isset($row[$i])){
				$json[$i] = array(
					"Idquestion" =>$row[$i]["id_vquestion"],
					"Question" => $row[$i]["vquestion"],
					"Creationdate" => $row[$i]["creation_date"],
					"Deadline" => $row[$i]["deadline"],
					"Criterions" => getVQCriterions($row[$i]["id_vquestion"]),
					'Groups'=> getVQuestionGroup($row[$i]["id_vquestion"]),
					'Students'=> getStudentVquestion($row[$i]["id_vquestion"]),
					'Count'=> getVquestionStudentCount($row[$i]["id_vquestion"])
					);

				$i++;
			}
			$json["success"] = true;
			echo json_encode($json);
		} catch (PDOException $e) {
			echo "Error: " .$e->getMessage();
		}
	}

?>

