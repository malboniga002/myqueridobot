<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<head>
</head>
<body>
<?php
//if theres no session it sends the user to the login
session_start(); 
if(!isset($_SESSION['userid'])){
    header('Location: index.php');
}
//if theres information sent by the form at the bottom of this file
if((isset($_POST["enviado"])) && ($_POST["enviado"]== "form1")){
	include_once "include/mydb.php";
	//the group ids are separated by comas in a string(got the data form a method at the botom), with this it makes an array of group ids
	$groups=explode(',', $_POST["groups"]);
	$i=0;
	//while theres a group selected on the parent window(this is an iframe) it deletes the group
	while(isset($groups[$i])){
		deleteGroups($groups[$i],$_SESSION['userid']);
		$i++;
	}
	//after deleting them it reloads the parent page
	echo "<script>parent.location='professor_group.php';</script>";
}
else
{?>
<script>
//closes this iframe
function cancel(){
parent.document.getElementById('deleteGroup').remove();
}

//this method checks the text written by the user to delete the groups(typical 'I am sure...')
function getDeleteElements(){
//if the user has write it correctly
if (document.getElementById("text").value=='I am sure of deleting this group for all the eternity.') {
parent.removeSearch();
var text="";
var i=0;
var form=document.getElementById("form1");
//it prepares a hidden data(string) with the id's separated by comas of the groups selected in the datatable of the parent window
while(parent.document.getElementById(i)){
	if (parent.document.getElementById(i).checked) {
		text=text+parent.document.getElementById("id-"+i).innerHTML+",";
	}
	i++;
}
//if theres a group selected it sends the data
if (text!="") {
var newInput1 = document.createElement('input');
newInput1.type = 'hidden';
newInput1.name = 'groups';
newInput1.value=text.slice(0,(text.length)-1);
form.appendChild(newInput1);
form.submit();
}
//else it send an alert to tell the user that theres no group selected
else{parent.alert('You have not select the group to delete');}
}
//else it sends an alert to tell the user that the text is wrong
else{
parent.alert('The text for deleting the group is wrong');	
}
}
</script>
<!--this is the form-->
<form action="delete_group.php" method="post" enctype="multipart/form-data" id="form1">
  <div><center>
  	<h2>Delete group</h2>	
  	<label>Write "I am sure of deleting this group for all the eternity." to delete the group:</label><br>
    <textarea cols=60 rows=2 id="text"></textarea><br><br>
    <input type="button" value="Cancel" onclick='cancel()'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" name="button" id="button" value="Delete group" onclick='getDeleteElements()' />
  </center></div>
  <input type="hidden" name="enviado" value="form1"/>
</form>
<?php }?>
</body>
</html>