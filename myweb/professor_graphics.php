<!doctype html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Graphics</title>
    <?php
      //if the session has not the user id it sends the user to the login 
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
    ?>
    <script src="js/graphics.js"></script>
  </head>
  <body>
  	<?php include("include/header.html") ?>
  	<div><center>
  		<div>
  			<h2>Graphics</h2>
  		</div>
      <!--the buttons call to functions of graphics.js to open a new window with the graphics-->
      <div><input type="button" name="creategraphic1" value="Graphic of correct/wrong answers in quizzes per student" onclick="createGraphic1()"><br><br>
        <input type="button" name="creategraphic2" value="Graphic of answered questions in quizzes per student" onclick='createGraphic2()'><br><br>
        <input type="button" name="creategraphic2" value="Graphic of the quizz answers given by students during the last 12 months" onclick='createGraphic3()'><br><br>
        <input type="button" name="creategraphic2" value="Graphic of the quizz answers given per student during the last 12 months" onclick='createGraphic4()'>
      </div>
  	</center></div>
  </body>
</html>