<!doctype html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Options</title>
    <?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //get the options of the database and loads in all the page
      require_once "include/mydb.php";
      $options=getProfOptions($_SESSION['userid'],'../Globals.php');
    ?>
    <script src="js/numericvaliadtion.js"></script>
    <script src="js/options.js"></script>
  </head>
  <body>
  	<?php include("include/header.html") ?>
  	<div><center>
  		<h2>Options</h2>
      <div>
        <!--all the data from the options is in this html code, the php code is to load the ald options the user has-->
        <h4>Language of the bot</h4>
          Spanish <input type="radio" name="language" value="Spanish" <?php if ($options['language']=="Spanish") {echo 'checked="checked"';} ?>><br>
          English <input type="radio" name="language" value="English" <?php if ($options['language']=="English") {echo 'checked="checked"';}?>><br>
      </div>
      <div>
        <h4>Voice answers evaluation and quizzes</h4>
           Autoevaluation <input id="autoeval" type="checkbox" <?php if ($options['autoeval']=="T") {echo 'checked="checked"';}?>/><br><br>
           Randomize the anwers in quizzes <input id="random" type="checkbox" <?php if ($options['quiz_answer_random']=="T") {echo 'checked="checked"';}?>/><br><br>
          Maximun number of evaluations per student in a question: 
          <input type="text" id='maxeval' value=<?php echo "'".$options['max_eval']."'"; ?> size="2" style='text-align: center; ' onkeypress="return valida(event)" ><br><br>
          What scale do you prefer to be used by your students?<br>
          From 0 to 10 <input type="radio" name="scale" value="0-10" <?php if ($options['scale']=="0-10") {echo 'checked="checked"';}?>><br>
          From 0 to 100 <input type="radio" name="scale" value="0-100" <?php if ($options['scale']=="0-100") {echo 'checked="checked"';}?>><br>
          Fatal, Very bad, Bad, Regular, Good, Very good or Excelent <input type="radio" name="scale" value="fatal" <?php if ($options['scale']=="fatal") {echo 'checked="checked"';}?>><br>
          Fail, Approved, Notable or Distinction <input type="radio" name="scale" value="fail" <?php if ($options['scale']=="fail") {echo 'checked="checked"';}?>><br>
          From ⭐ to ⭐⭐⭐⭐⭐ <input type="radio" name="scale" value="stars" <?php if ($options['scale']=="stars") {echo 'checked="checked"';}?>><br><br><br>
      </div>
      <!--this button calls a function from options.js to open an iframe and send all the modifications to the database-->
      <div><input type="button" name="save" value="Save changes" onclick='saveChanges()'></div>
  	</center></div>
  </body>
</html>