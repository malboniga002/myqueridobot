<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
</head>
<body>
<?php
//if theres no session it sends the user to the login
session_start(); 
if(!isset($_SESSION['userid'])){
    header('Location: index.php');
}
//if theres information sent by the form at the bottom of this file
if((isset($_POST["enviado"])) && ($_POST["enviado"]== "form1")){	
	include_once "include/mydb.php";
  //it inserts the group in the database and reloads the page behind this iframe
	insertGroup($_POST["group"],$_SESSION['userid']);
	echo "<script>".
	"parent.location='professor_group.php';</script>";
}
else
{?>
<script>
//closes this iframe
function cancel(){
parent.document.getElementById('createGroup').remove();
}

//sends the data to this file if the user has write something in the textarea
function sendgroup(){
if (document.getElementById("group").value!="") {
var form=document.getElementById("form1");
form.submit();
}
//else it sends an alert to tell the problem to the user
else{
parent.alert('You havent write the name for the group');
}
}
</script>
<!--this is the form-->
<form action="create_group.php" method="post" enctype="multipart/form-data" id="form1">
  <div><center>
  	<h2>Create group</h2>
  	<label>Group name:</label>	
    <textarea cols=60 rows=5 id="group" name="group"></textarea><br><br>
    <input type="button" value="Cancel" onclick='cancel()'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" name="button" id="button" onclick="sendgroup()" value="Create group" />
  </center></div>
  <input type="hidden" name="enviado" value="form1"/>
</form>
<?php }?>
</body>
</html>