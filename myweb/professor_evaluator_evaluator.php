<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Student info</title>
    <?php 
      //if the session has not the user id or any of the parameters needed in this file, it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid']) || !isset($_GET['stdid']) || !isset($_GET['stdname'])){
        header('Location: index.php');
      }
      else{
        echo "<script>var idst=".$_GET['stdid'].";</script>";
      }
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstableevaluatorevaluator.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
  </head>
  <body>
  	<div><center>
  		<div>
  			<h2><?php echo $_GET['stdname']." is the evaluator of:";?></h2>
  		</div>
  		<section>
        <!--this is the structure of the datatable. The data of the body is loaded in functionstableevaluators.js-->
  			<table id="tableevaluatorevaluator">
	  			<thead>
	  				<tr>
              <th>ID</th>
              <th>Student name</th>
              <th>Question ID</th>
              <th>Question</th>
              <th>Date</th>
              <th>Checked</th>
	  				</tr>
	  			</thead>
	  		</table>
  		</section>
  		<footer></footer>
      <div><br><input type="button" name="close" value="Close" onclick='closeiframe()'></div>
  	</center></div>
  </body>
</html>