<!doctype html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Log In</title>
    <script type="text/javascript" src="js/login.js"></script>
  </head>
  <body>
    <?php 
      //start the sesion
      session_start();
      //this method search the user in the database. If theres a user with that username and password it returns 1, else it returns -1
      function verify_login($usuario,$pass){
        try {
          include("../Globals.php");
          $query = 'SELECT professor.id_prof FROM professor,user WHERE professor.id_prof=:pass';
          if ($usuario!=NULL) {
            $query.=' AND professor.id_prof=user.id AND user.first_name=:usuario ';
          }
          $sth = $pdo->prepare($query);
          if ($usuario!=NULL) {
            $sth->bindParam(':usuario', $usuario, PDO::PARAM_STR);
          }
          $sth->bindParam(':pass', $pass, PDO::PARAM_INT);
          $sth->execute();
          $user=$sth->fetch(PDO::FETCH_ASSOC);
          if (isset($user['id_prof'])) {
            return 1;
          }
          return -1;
        } catch (PDOException $e) {
          echo "Error: " .$e->getMessage();
        }
      
    }
    ?>
    <div>
      <center>
        <h1>Log In</h1>
        <div class="panel_login">
          <!--In this form theres only the user and the password that are sent to this file. The button sends the data to a js function to check the data -->
          <!--http://stackoverflow.com/questions/905222/enter-key-press-event-in-javascript-->
          <form name="form_login" method="post" action="">
            <div><label>User:</label><br>
              <input type="text" name="usuario" onkeypress="return runScript(event)"></div><br>
              <div><label>Password:</label><br>
              <input type="password" name="pass" onkeypress="return runScript(event)"></div><br>
            <div><input type="button" name="login" value="Log in" onclick="logincheck()"></div>
          </form>
        </div>
      </center>
    </div>
    <?php
      /*If theres no session started*/
      if(!isset($_SESSION['userid'])){
        /*it checks if theres a password sent to this file*/
        if(isset($_POST['pass'])){
          /*this calls to the function at the top of this file. If it returns 1 it starts the sesion and sends the user toprofessor_vquestion.php*/
          $typeuser=verify_login($_POST['usuario'],$_POST['pass']);
          if($typeuser==1){
            $_SESSION['userid']=$_POST['pass'];
            header("location:professor_vquestion.php");
          }
          /*if the password or the username was wrong it shows a message telling it to the user*/
          else{
            echo '<div class="error"> Incorrect user or password. </div>';
          }
        }
      }
      /*If the session was started when the user enters to this page this code sends him/her to professor_vquestion.php*/
      else{
        //but first it checks that the user is correct
        $typeuser=verify_login(NULL,$_SESSION['userid']);
        if($typeuser==1){
          header("location:professor_vquestion.php");
        }
      }
    ?>
  </body>
</html>