<!doctype html>
<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Groups</title>
    <?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstablegroups.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/optionsbuttonstable.js"></script>
  </head>
  <body>
  	<?php include("include/header.html") ?>
  	<div><center>
  		<div>
  			<h2>Groups</h2>
  		</div>
  		<div>
        <!--create and delete group calls functions from functionstablegroups.js to open iframes and the other buttons call functions from optionsbuttonstable.js-->
  			<input type="button" name="creategroup" value="Create group" onclick="createGroup()">&nbsp;&nbsp;&nbsp;
  			<input type="button" name="deletegroup" value="Delete group" onclick='deleteGroups()'>&nbsp;&nbsp;&nbsp;
        <input type="button" name="select" value="Select all" onclick='selectAll()'>&nbsp;&nbsp;&nbsp;
        <input type="button" name="deselect" value="Unselect all" onclick='deselectAll()'><br><br>
  		</div>
  		<section>
        <!--this is the structure of the datatable. The data of the body is loaded in functionstablegroups.js-->
  			<table id="tablegroupprof">
	  			<thead>
	  				<tr>
              <th>ID</th>
	  					<th>Group name</th>
	  					<th>Number of students</th>
	  					<th>Number of questions</th>
              <th>Creation date</th>
              <th>Select</th>
	  				</tr>
	  			</thead>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Group name</th>
              <th>Number of students</th>
              <th>Number of questions</th>
              <th>Creation date</th>
              <th>Select</th>
            </tr>
        </tfoot>
	  		</table>
  		</section>
  		<footer></footer>
  	</center></div>
  </body>
</html>