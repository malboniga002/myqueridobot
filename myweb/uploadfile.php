<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
</head>
<body>
<?php
//if the session has not the user id it sends the user to the login
session_start(); 
if(!isset($_SESSION['userid'])){
    header('Location: index.php');
}
//i based in this tutorials to make this file:
//https://norfipc.com/inf/como-subir-fotos-imagenes-servidor-web.php
//https://www.youtube.com/watch?v=4DxmDRoStNc
//if the data was sent by the form at the bottom of this file
if((isset($_POST["enviado"])) && ($_POST["enviado"]== "form1")){
//if theres a file to upload
if (isset($_FILES['uploadedfile']['size'])) {
//it gets the size of the file. If it is bigger than 1MB it tells the user to upload a smaller file
$uploadedfileload="true";
$uploadedfile_size=$_FILES['uploadedfile']['size'];
echo $_FILES['uploadedfile']['name']."<BR>";
if ($_FILES['uploadedfile']['size']>1000000)
{$msg="The image must be smaller than 1MB <BR>";
$uploadedfileload="false";}
//else it downloads the file at the uploads folder(servers public folder)
$file_name=$_FILES['uploadedfile']['name'];
$add="uploads/".$file_name;
if($uploadedfileload=="true"){

if(move_uploaded_file ($_FILES['uploadedfile']['tmp_name'], $add)){
$file_name=__DIR__."/".$add;
//after the upload it changes this iframes parents label, showing the path of the uploaded file at the server and then it closes this iframe 
echo "<script>parent.document.getElementById('imagepath').innerHTML='".str_replace("\\", '/', $file_name)."';parent.document.getElementById('uploadImage').remove();</script>";
}
//if theres any error it tells it to the user with an alert and after that it closes the iframe
else{echo "<script>parent.alert('Error at uploading the image');parent.document.getElementById('uploadImage').remove();</script>";}

}else{echo "<script>parent.alert('".$msg."');parent.document.getElementById('uploadImage').remove();</script>";}
}
}
else
{?>
<script>
//this method closes this iframe
function cancel(){
parent.document.getElementById('uploadImage').remove();
}
//this form sends the data to this file (the code above this)
</script>	
<form action="uploadfile.php" method="post" enctype="multipart/form-data" id="form1">
  <p>
  	<!--when the user is selecting the file it only shows the type of file permited here-->
    <input id="uploadedfile" name="uploadedfile" type="file" accept="image/x-png, image/png, image/gif, image/pjpeg, image/jpeg, image/jpg">
    </p>
  <p>
  	<input type="button" value="Cancel" onclick='cancel()'/>&nbsp;&nbsp;&nbsp;
    <input type="submit" name="button" id="button" value="Upload image" />
  </p>
  <input type="hidden" name="enviado" value="form1"/>
</form>
<?php }?>
</body>
</html>