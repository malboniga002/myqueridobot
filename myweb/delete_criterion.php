<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<html>
<head>
</head>
<body>
<?php
//if theres no session it sends the user to the login
session_start(); 
if(!isset($_SESSION['userid'])){
    header('Location: index.php');
}
//if theres information sent by the form at the bottom of this file
if((isset($_POST["enviado"])) && ($_POST["enviado"]== "form1")){
	include_once "include/mydb.php";
	//it deletes the criterion from the database and removes it from the options of this iframes parent, after that it removes the iframe
	$crit=deleteCriterion($_POST["id"],$_SESSION['userid']);
	if ($crit>0) {
		echo "<script>".
		"parent.document.getElementById('crit-".$_POST["id"]."').remove();".
		"parent.multi(parent.select);".
		"parent.document.getElementById('deleteCrit').remove();".
		"</script>";
	}
	//if the ID of the criterion was wrong the parent of the iframe shows an alert telling it
	else{
		$flag=true;
		echo "<script>parent.alert('Incorrect criterion ID');</script>";
	}
	//if there was an error it reloads the form at the bottom of this file
	if (isset($flag)) {
		echo "<script>location='delete_criterion.php';</script>";
	}
	
}
else
{?>
<script>
//closes this iframe
function cancel(){
parent.document.getElementById('deleteCrit').remove();
}

//sends the data to this file if the user has write the specified text in the text area
function delCrit(){
if (document.getElementById("text").value=='I am sure of deleting this criterion for all the eternity.') {
var form=document.getElementById("form1");
form.submit();
}
//else its parent shows an alert telling it
else{parent.alert('The text for deleting the criterion is wrong');}
}
</script>
<!--this is the form with the call to a js with the method to check that the user has pressed a number at the keyboard-->
<script src="js/numericvaliadtion.js"></script>
<form action="delete_criterion.php" method="post" enctype="multipart/form-data" id="form1">
  <div><center>
  	<h2>Delete criterion</h2>	
  	<label>Write the ID of the criterion you want to delete (the number next to the criterion):</label><br>
  	<input type="text" name='id' onkeypress="return valida(event)" ><br><br>
  	<label>Write "I am sure of deleting this criterion for all the eternity." to delete the criterion:</label><br>
    <textarea cols=60 rows=2 id="text"></textarea><br><br>
    <input type="button" value="Cancel" onclick='cancel()'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" name="button" id="button" onclick='delCrit()' value="Delete criterion" />
  </center></div>
  <input type="hidden" name="enviado" value="form1"/>
</form>
<?php }?>
</body>
</html>