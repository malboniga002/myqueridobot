<!--Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.-->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Evaluations - Student</title>
    <?php 
      //if the session has not the user id it sends the user to the login
      session_start(); 
      if(!isset($_SESSION['userid'])){
        header('Location: index.php');
      }
      //if theres no id of the answer evaluated it sends the user to professor_evaluations.php
      if(!isset($_GET['idva'])){
        header('Location: professor_evaluations.php');
      }
      require_once "include/mydb.php";
      //if the id of the answer isnt from a student of the user it sends him/her to professor_evaluations.php
      $eval=getTheAnswerEval($_SESSION['userid'],$_GET['idva']);
      if (!isset($eval['vquestion'])) {
        header('Location: professor_evaluations.php');
      }
      echo '<script>var idva="'.$_GET['idva'].'"</script>';
    ?>
    <link rel="stylesheet" href="css/datatables.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/functionstableevaluationstudent.js"></script>
    <script src="js/datatables.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script> var crits="";</script>
  </head>
  <body>
  	<?php include("include/header.html") ?>
  	<div><center>
  		<div>
  			<h2>Evaluation</h2>
      </div>
      <div>
        <!--here it charges the question to show it and the answer in ogg format(this doesnt work in some navigators, go to this webpage for more information: https://www.w3schools.com/html/html5_audio.asp )-->
        <h4>Question: <?php echo "".$eval['vquestion']; ?></h4>
        <label>Answered by <b><?php echo "".$eval['first_name']." ".$eval['last_name']; ?></b></label><br>
        <audio controls><source src=<?php echo "'.".$eval['file_path']."'"; ?> type='audio/ogg'></audio><br><br>
  		</div>
      <div style="overflow-x:auto;width:800px;">
        <table>
          <tr>
            <th>ID</th>
            <?php 
              //this gets all the criterions the evaluators has to evaluate for the answer and it makes a horizontal table with the ID of the criterion, the criterion and the mean of points given to the answer in criterion 
              $crit=getCritEval($_GET['idva']);
              $i=0;
              $text="";
              $avg="";
              $m="";
              while (isset($crit[$i])) {
                echo '<td class="id">'.$crit[$i]['id_criterion'].'</td>';
                //this saves all the text in varibles to use then later to make a horizontal table
                $avg.= '<td class="id">'.$crit[$i]['id_criterion'].'</td>';
                $text.='<td title="'.$crit[$i]['criterion'].'">'.substr($crit[$i]['criterion'],0,22).'</td>';
                echo '<script>crits=crits+"'.$crit[$i]['id_criterion'].',";</script>';
                $mean=getEvalAvg($crit[$i]['id_criterion'],$_GET['idva']);
                $m.= '<td class="id">'.$mean['mean'].'</td>';
                $i++;
              }
            ?>
          </tr>
          <tr>
            <th>Criterion</th>
            <?php
              echo $text;
            ?>
          </tr>
          <tr>
            <th>Mean</th>
            <?php
              echo $m;
            ?>
          </tr>
        </table>
      </div><br><br>
      <!--here is the datatable with the id of the evaluator, his name, the list of the ids of the criterions to evaluate and the voice explanation of the student(if theres no data for one of the fields it writes "-" character). The body data is loaded by functionstableevaluationstudent.js-->
  		<section>
  			<table id="tablevals">
	  			<thead>
	  				<tr>
              <th>ID</th>
	  					<th>Student</th>
	  					<?php
                $avg= str_replace('<td class="id">', '<th class="id">', $avg);
                echo ''.str_replace('</td>', '</th>', $avg);
              ?>
              <th>Explanation</th>
	  				</tr>
	  			</thead>
	  		</table>
  		</section>
  	</center></div>
  </body>
</html>