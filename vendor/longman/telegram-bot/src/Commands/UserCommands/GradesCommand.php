<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Commands\UserCommands;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Login;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;

class GradesCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'grades';

    /**
     * @var string
     */
    protected $description = 'Comando para ver la media que se tiene en cada criterio en cada pregunta';

    /**
     * @var string
     */
    protected $usage = '/grades';

    /**
     * @var string
     */
    protected $version = '0.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    public function execute()
    {
        try{
            $message = $this->getMessage();
            $chat = $message->getChat();
            $user = $message->getFrom();
            $text = trim($message->getText(true));
            $chat_id = $chat->getId();
            $user_id = $user->getId();
            

            $data = [
                'chat_id' => $chat_id,
            ];

            if ($chat->isGroupChat() || $chat->isSuperGroup()) {
                //reply to message id is applied by default
                //Force reply is applied by default so it can work with privacy on
                $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
            }

            //Conversation start
            $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
            $notes = &$this->conversation->notes;
            !is_array($notes) && $notes = [];

            //cache data from the tracking session if any
            $state = 0;
            if (isset($notes['state'])) {
                $state = 1;
            }
            //this state sends the grades and the voice explanations for those grades given by the evaluators 
            if ($state == 1) {
                $messages=Language::getGradesMessages($this->conversation->notes['language']);
                //first it changes the string with voice questions ID's in an array
                $ids=explode(",", $notes['ids']);
                //this checks the answer of the user. If he/she wrote a correct id this sends him/her his/her answer
                if (is_numeric($text) && isset($ids[((int)$text)-1])) {
                    $answer=MYDB::getVAnswerSelected( $ids[((int)$text)-1], $user_id);
                    $data['text'] = $messages['answer'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $data['text'] = "";
                    $result = Request::sendVoice($data,$answer['file_path'] );
                    $crits=MYDB::getEvalCriterions($ids[((int)$text)-1]);
                    $i=0;
                    //after sending the answer with this while it sends the evaluated criterion and all the grades the answer recived from the evaluators(the second while)
                    while (isset($crits[$i])) {
                        $crit=MYDB::getCriterion($crits[$i]['id_criterion']);
                        $data['text'] = $crit['criterion'];
                        $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                        $result = Request::sendMessage($data);
                        $data['text'] = $messages['notes'];
                        $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                        $result = Request::sendMessage($data);
                        $califications=MYDB::getGradesCriterion( $ids[((int)$text)-1], $user_id,$crits[$i]['id_criterion']);
                        $j=0;
                        while ( isset($califications[$j])) {
                            $data['text'] = $califications[$j]["grade"];
                            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                            $result = Request::sendMessage($data);
                            $j++;
                        }
                        $i++;
                    }
                    $data['text'] = $messages['comments'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $data['text'] = '';
                    $explanations=MYDB::getExplanations($answer['id_file']);
                    $i=0;
                    //after that if the answer has voice explanations it sends them to the user 
                    while (isset($explanations[$i])) {
                        $result = Request::sendVoice($data,$explanations[$i]['file_path'] );
                        $i++;
                    }
                    //then it sends an end message and ends the conversation
                    $data['text'] = $messages['end'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                }
                //if the id wasn't correct it tells it to the user and waits for another message
                else{
                    $data['text'] = $messages['no_id'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                }
            }
            //this state send to the user all the voice question which he/she has answered(the deadline of the question<now)
            if ($state == 0) {
                $student=Login::userExist($user_id);
                $language=MYDB::getLanguage($student['id_prof']);
                $notes['language'] = $language['language'];
                $messages=Language::getGradesMessages($language['language']);
                $data['text'] = $messages['questions'];
                $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                $result = Request::sendMessage($data);
                $questions=MYDB::getQuestion($user_id);
                $i=0;
                $ids="";
                //while theres a question this sends the question to the user and writes an ID first
                while (isset($questions[$i])) {
                    $data['text'] = ($i+1).".-".$questions[$i]['vquestion'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    //the ID's are going to be related with the position in this array(its an string but i'll use explode in the next state to convert to an array) with the ID's pf the voice questions
                    $ids.=$questions[$i]['id_vquestion'].",";
                    $i++;
                }
                //if the string has an ID of a question it deletes the last coma
                if (strlen($ids)>0) {
                    $ids=substr($ids, 0,strlen($ids)-1);
                }
                //if theres no voice question it tells it to the user and it ends the conversation
                else{
                    $data['text'] = $messages['no_question'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                    return $result;
                }
                //this adds the string to the notes of the conversation, sends the user to the next state and sends him/her a message telling him/her to write the id of the question he/she want to know his/her grades
                $notes['ids'] = $ids;
                $notes['state'] = 1;
                $this->conversation->update();
                $data['text'] = $messages['selec_id'];
                $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                $result = Request::sendMessage($data);
            }
            return;
        }catch(TelegramException $e){
            //if the user isn't registered it sends a message telling it.
            $this->conversation->stop();
            $data['text'] = $e->getMessage();
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $result = Request::sendMessage($data);
            return $result;
        }
    }
}
