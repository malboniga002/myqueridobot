<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Gamer;
use Longman\TelegramBot\Entities\Login;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Exception\TelegramException;

class TestCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'test';

    /**
     * @var string
     */
    protected $description = 'Mostrar tests disponibles y comenzar a realizar tests';

    /**
     * @var string
     */
    protected $usage = '/test';

    /**
     * @var string
     */
    protected $version = '0.1.6';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    /**
     * Indicates the scale to be used.
     *
     * @param array    $data  Data of the message.
     * @param Longman\TelegramBot\Entities\Gamer    $gamer  Gamer whits the data of the student and the game.
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     */
    private function gameOver(Gamer $gamer,array $data){
        $gamer->finish();
        unset($gamer);
        $messages=Language::getTestMessages($this->conversation->notes['language']);
        $data['reply_markup'] = Keyboard::remove(['selective' => true]);
        $data['text'] = $messages['bye'];
        $result = Request::sendMessage($data);
        $this->conversation->stop();
        return $result;
    }

    /**
     * Prepares the keyboard before the answer of a question.
     *
     * @param array    $messages  Messages of the buttons.
     *
     * @return \Longman\TelegramBot\Entities\Keyboard
     */
    private function prepareKeyboard(array $messages){
        $why=MYDB::getWhy($this->conversation->notes['question']);
        $why_not=MYDB::getWhyNot($this->conversation->notes['question']);
        if ($why['why']!=NULL && $why_not['why_not']!=NULL) {
            $keyboard=(new Keyboard([$messages['button_exit'],$messages['button_next_question']],[$messages['button_why'],$messages['button_why_not']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
        }
        elseif ($why['why']==NULL && $why_not['why_not']!=NULL) {
            $keyboard=(new Keyboard([$messages['button_exit'],$messages['button_next_question']],[$messages['button_why_not']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
        }
        elseif ($why['why']!=NULL && $why_not['why_not']==NULL) {
            $keyboard=(new Keyboard([$messages['button_exit'],$messages['button_next_question']],[$messages['button_why']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
        }
        else{
            $keyboard=(new Keyboard([$messages['button_exit'],$messages['button_next_question']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
        }
        return $keyboard;
    }

    /**
     * Prepares the keyboard before the answer of a question at the end of a quiz.
     *
     * @param array    $messages  Messages of the buttons.
     *
     * @return \Longman\TelegramBot\Entities\Keyboard
     */
    private function prepareKeyboard2(array $messages){
        $why=MYDB::getWhy($this->conversation->notes['question']);
        $why_not=MYDB::getWhyNot($this->conversation->notes['question']);
        if ($why['why']!=NULL && $why_not['why_not']!=NULL) {
            $keyboard=(new Keyboard([$messages['button_why'],$messages['button_why_not']],[$messages['fin']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
        }
        elseif ($why['why']==NULL && $why_not['why_not']!=NULL) {
            $keyboard=(new Keyboard([$messages['button_why_not']],[$messages['fin']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);;
        }
        elseif ($why['why']!=NULL && $why_not['why_not']==NULL) {
            $keyboard=(new Keyboard([$messages['button_why']],[$messages['fin']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
        }
        else{
            $keyboard=(new Keyboard([$messages['fin']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
        }
        return $keyboard;
    }

    /**
     * Sends text in the case of the message sent to the bot is something that the bot doesn't recognize.
     *
     * @param array                                   $data   Data of the message.
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     */
    private function dontUnderstand(array $data){
        $messages=Language::getTestMessages($this->conversation->notes['language']);
        $data['text'] = $messages['understand'];
        $result = Request::sendMessage($data);
        return $result;
    }

    /**
     * Sends text with the quizzes data and the inline buttons.
     *
     * @param array                                   $data   Data of the message.
     * @param Longman\TelegramBot\Entities\Gamer      $gamer  Gamer with all the data of the student.
     *
     * @return bool If there are inline buttons TRUE else FALSE.
     */
    private function getInlineButtons(array $data,Gamer $gamer){
        $quizzes = MYDB::getQuizzes();
        $messages=Language::getTestMessages($this->conversation->notes['language']);
        $i = 0;
        $buttons = [];
        //search available quizzes and creates a inline button
        while (isset($quizzes[$i])){
            $button=['text' => $quizzes[$i]['theme'].' ('.$quizzes[$i]['questions'].')',
                'callback_data' => 'test_'.$quizzes[$i]['id_quiz']];
            array_push($buttons, $button);
            $i++;
        }
        //if theres no button it sends a message
        if(!isset($buttons[0])){
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $data['text'] = $messages['no_quiz'];
            $result = Request::sendMessage($data);
            $this->conversation->stop();
            $gamer->finish();
            return FALSE;
        }
        $inline_keyboard = new InlineKeyboard($buttons[0]);
        $i=0;
        while (isset($buttons[$i])) {
            $inline_keyboard->addRow($buttons[$i]);
            $i++;
        }
        return $inline_keyboard;
    }

    public function execute()
    {   try{
            $message = $this->getMessage();
            $callback_query = $this->getUpdate()->getCallbackQuery();

            //if the command is called by the user by a message it takes the information from the message, else it takes from a callback(see ../SystemCommands/CallbackQuery.php)
            if ($message) {
                $chat = $message->getChat();
                $user = $message->getFrom();
                $text = trim($message->getText(true));
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }elseif ($callback_query) {
                $message = $callback_query->getMessage();
                $chat = $message->getChat();
                $user = $callback_query->getFrom();
                $text = $callback_query->getData();
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }
            
            //Preparing Response
            $data = [
                'chat_id' => $chat_id,
            ];

            if ($chat->isGroupChat() || $chat->isSuperGroup()) {
                //reply to message id is applied by default
                //Force reply is applied by default so it can work with privacy on
                $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
            }

            //Conversation start
            $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
            $gamer = new Gamer($user_id);

            $notes = &$this->conversation->notes;
            !is_array($notes) && $notes = [];

            //cache data from the tracking session if any
            $state = 0;
            if (isset($notes['state'])) {
                $state = $notes['state'];
            }

            //this is the state machine. If the student press a button it goes to the next state
            if($state == 1){
                if (isset($notes['quiz'])) {
                    $gamer->setQuiz($notes['quiz']);
                    $state = 2;
                }
                else{
                    $state = 0;
                }
            }
            //checks if the user is registered to use the bot(see ../../Entities/Login.php), if he/she isn't registered goes to the catch at the bottom. If thats not the case it sends te inline keyboard with the quizzes
            if($state == 0){
                $student=Login::userExist($user_id);
                $language=MYDB::getLanguage($student['id_prof']);
                $notes['language'] = $language['language'];
                $this->conversation->update();
                $inline_keyboard = $this->getInlineButtons($data,$gamer);
                if(!$inline_keyboard){
                    return;
                }
                $messages=Language::getTestMessages($this->conversation->notes['language']);
                $data['text'] = $messages['quiz'];
                $data['reply_markup'] = $inline_keyboard;
                $notes['state'] = 1;
                $this->conversation->update();
                $result = Request::sendMessage($data);
            }
            //this state is for the case of finishing a quiz an the user press any button
            if ($state == 5) {
                $messages=Language::getTestMessages($this->conversation->notes['language']);
                if (!in_array($text, [$messages['fin'],$messages['button_why'],$messages['button_why_not']], true)) {
                    return $this->dontUnderstand($data);
                }
                if(strcmp($text, $messages['fin'])==0){
                    $state = 7;
                }
                elseif(strcmp($text, $messages['button_why'])==0){
                    $why=MYDB::getWhy($notes['question']);
                    $data['text'] = $why['why'];    
                    $data['reply_markup'] = $this->prepareKeyboard2($messages);
                    $result = Request::sendMessage($data);
                    return $result;
                }
                else{
                    $why_not=MYDB::getWhyNot($notes['question']);
                    $data['text'] = $why_not['why_not'];    
                    $data['reply_markup'] = $this->prepareKeyboard2($messages);
                    $result = Request::sendMessage($data);
                    return $result;
                }
            }
            //this state is for the case of finishing a quiz an the user press any button
            if ($state == 6) {
                $messages=Language::getTestMessages($this->conversation->notes['language']);
                if (!in_array($text, [$messages['button_next_quizz'], $messages['button_exit'],$messages['button_repeat']], true)) {
                    return $this->dontUnderstand($data);
                }
                if(strcmp($text, $messages['button_next_quizz'])==0){
                    $next_quiz=MYDB::getnextQuiz($gamer->getIdQuiz());
                    $gamer->setQuiz($next_quiz['id_quiz']);
                    $state = 2;
                }
                elseif (strcmp($text, $messages['button_repeat'])==0) {
                    $gamer->setQuestion();
                    $state = 2;
                }
                else{
                    return $this->gameOver($gamer,$data);
                }
            }
            //this state check if the student has pressed a button anfter a question
            if ($state == 4) {
                $messages=Language::getTestMessages($this->conversation->notes['language']);
                if (!in_array($text, [$messages['button_next_question'], $messages['button_exit'],$messages['button_why'],$messages['button_why_not']], true)) {
                    return $this->dontUnderstand($data);
                }
                if(strcmp($text, $messages['button_next_question'])==0){
                    $state = 2;
                }
                elseif(strcmp($text, $messages['button_why'])==0){
                    $why=MYDB::getWhy($notes['question']);
                    $data['text'] = $why['why'];    
                    $data['reply_markup'] = $this->prepareKeyboard($messages);
                    $result = Request::sendMessage($data);
                    return $result;
                }
                elseif(strcmp($text, $messages['button_why_not'])==0){
                    $why_not=MYDB::getWhyNot($notes['question']);
                    $data['text'] = $why_not['why_not'];    
                    $data['reply_markup'] = $this->prepareKeyboard($messages);
                    $result = Request::sendMessage($data);
                    return $result;
                }
                else{
                    return $this->gameOver($gamer,$data);
                }
            }
            //this state sends the question and prepares the keyboard. The keyboard can have 2-4 answers
            if($state == 2){
                $question = $gamer->getCurrentQuestion();
                if ($question) {
                    $question_text =$question['question']."\n".$question['answerA']."\n".$question['answerB'];
                    if(!is_null($question['answerC'])){
                        $question_text .= "\n".$question['answerC'];
                    }
                    if(!is_null($question['answerD'])){
                        $question_text .= "\n".$question['answerD'];
                    }

                    if(is_null($question['answerD']) && is_null($question['answerC'])){
                        $reply_keyboard_markup = (new Keyboard(['A','B']))
                            ->setResizeKeyboard(true)
                            ->setOneTimeKeyboard(true)
                            ->setSelective(false);
                    }
                    elseif(is_null($question['answerD'])){
                        $reply_keyboard_markup = (new Keyboard(['A','B'],['C']))
                            ->setResizeKeyboard(true)
                            ->setOneTimeKeyboard(true)
                            ->setSelective(false);
                    }
                    else{
                        $reply_keyboard_markup = (new Keyboard(['A','B'],['C','D']))
                            ->setResizeKeyboard(true)
                            ->setOneTimeKeyboard(true)
                            ->setSelective(false);
                    }
                    $data['parse_mode'] = 'HTML';
                    $data['text'] = $question_text;
                    $data['reply_markup'] = $reply_keyboard_markup;

                    $result = Request::sendMessage($data);
                    //it search an image and sends an image associated to the question
                    if ($question['image_path']!=NULL) {
                        $data['text'] = '';
                        $result = Request::sendPhoto($data,$question['image_path']);
                    }
                    $notes['question'] = $question['id'];
                    $notes['state'] = 3;
                    $this->conversation->update();
                }
                else {
                    $messages=Language::getTestMessages($this->conversation->notes['language']);
                    $data['text'] = $messages['no_question'];
                        $data['reply_markup'] = (new Keyboard([$messages['button_exit'],$messages['button_repeat']]))
                            ->setResizeKeyboard(true)
                            ->setOneTimeKeyboard(true)
                            ->setSelective(false);
                    $result = Request::sendMessage($data);
                    $notes['state'] = 6;
                    $this->conversation->update();
                }
            }
            //this state checks if the answer given is correct and sends the buttons to do the next action
            if ($state == 3) {
                if (!in_array($text, ['A', 'B','C','D'], true)) {
                    return $this->dontUnderstand($data);
                }
                $messages=Language::getTestMessages($this->conversation->notes['language']);
                $data['text'] = $gamer->saveAnswer($text,$this->conversation->notes['language']);
                $data['reply_markup'] = $this->prepareKeyboard($messages);
                $notes['state'] = 4;
                $ques = $gamer->getCurrentQuestion();
                if (!isset($ques['question'])){
                    $data['reply_markup'] = $this->prepareKeyboard2($messages);
                    $notes['state'] = 5;
                }
                $this->conversation->update();
                $result = Request::sendMessage($data);
            }
            //this state checks if there are more quizzes to do and gives the user some options
            if ($state == 7) {
                $messages=Language::getTestMessages($this->conversation->notes['language']);
                $next_quiz=MYDB::getnextQuiz($gamer->getIdQuiz());
                if (!isset($next_quiz['id_quiz'])) {
                    $data['text'] = $messages['no_more_quiz'];
                    $data['reply_markup'] = (new Keyboard([$messages['button_exit'],$messages['button_repeat']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
                } else {
                    $data['text'] = $messages['quiz_finish'];
                    $data['reply_markup'] = (new Keyboard([$messages['button_exit'],$messages['button_next_quizz']],[$messages['button_repeat']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
                }
                $result = Request::sendMessage($data);
                $notes['state'] = 6;
                $this->conversation->update();
            }

            return $result;

        }catch(TelegramException $e){
            //if the user isn't registered it sends a message telling it.
            $this->conversation->stop();
            $data['text'] = $e->getMessage();
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $result = Request::sendMessage($data);
            return $result;
        }
    }
}
