<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Commands\UserCommands;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Login;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Exception\TelegramException;

class ChangeCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'change';

    /**
     * @var string
     */
    protected $description = 'Comando para cambiar la respuesta de voz de una pregunta';

    /**
     * @var string
     */
    protected $usage = '/change';

    /**
     * @var string
     */
    protected $version = '0.1.1';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    /**
     * Sends text with the questions and answers data and the inline buttons.
     *
     * @param array    $data        Data of the message.
     * @param string|int      $id_student  ID of the student.
     *
     * @return bool If there are inline buttons TRUE else FALSE.
     */
    private function getInlineButtons(array $data,$id_student){
        $questions = MYDB::getGroupQuestions($id_student);
        $messages=Language::getChangeMessages($this->conversation->notes['language']);
        $i = 0;
        $j = 1;
        $buttons = [];
        $data['text'] = $messages['available_answers'];
        $result = Request::sendMessage($data);
        //get available questions to change the answer
        while (isset($questions[$i])){
            //if the question has more than 1 answer
            $num_answers=MYDB::getVAnswersForQuestion( $questions[$i]['id_vquestion'], $id_student);
            if(isset($num_answers['answers']) && ((int)$num_answers['answers'])>1 && ((int)$num_answers['answers'])<10){
                //if the deadline hasn't passed
                $question=MYDB::getVQuestion($questions[$i]['id_vquestion']);
                if(isset($question['deadline'])){
                    //sends a message and creates the button
                    $data['text'] = ($j).'. ('.substr($question['deadline'], 0,10).') ('.$num_answers['answers'].') '.$question['vquestion'];
                    $result = Request::sendMessage($data);
                    $button=['text' => ($j).'',
                        'callback_data' => 'change_'.$questions[$i]['id_vquestion']];
                    array_push($buttons, $button);
                    $j++;
                }
            }
            $i++;
        }
        //if theres no button it sends a message
    	if(!isset($buttons[0])){
    	    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $data['text'] = $messages['no_answers'];
            $result = Request::sendMessage($data);
            $this->conversation->stop();
            return FALSE;
    	}
        //make rows of 3 inline buttons
        $inline_keyboard = new InlineKeyboard($buttons[0]);
        $i = 0;
        $row = [];
        while (isset($buttons[$i])) {
            array_push($row, $buttons[$i]);
            $mod = ($i + 1) % 3;
            if ($mod == 0) {
                $inline_keyboard->addRow($row[0],$row[1],$row[2]);
                $row=[];
            }
            $i++;
        }
        if(isset($row[1])){
            $inline_keyboard->addRow($row[0],$row[1]);
        }
        else if(isset($row[0])){
            $inline_keyboard->addRow($row[0]);
        }
        //sends a message and the inline keyboard
        $data['text'] = $messages['answer'];
        $data['reply_markup'] = $inline_keyboard;
        $result = Request::sendMessage($data);
        $this->conversation->notes['done']='yes';
        $this->conversation->update();
        return TRUE;
    }

    public function execute()
    {
        try{
            $message = $this->getMessage();
            $callback_query = $this->getUpdate()->getCallbackQuery();

            //if the command is called by the user by a message it takes the information from the message, else it takes from a callback(see ../SystemCommands/CallbackQuery.php)
            if ($message) {
                $chat = $message->getChat();
                $user = $message->getFrom();
                $text = trim($message->getText(true));
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }elseif ($callback_query) {
                $message = $callback_query->getMessage();
                $chat = $message->getChat();
                $user = $callback_query->getFrom();
                $text = $callback_query->getData();
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }

            $data = [
                'chat_id' => $chat_id,
            ];

            if ($chat->isGroupChat() || $chat->isSuperGroup()) {
                //reply to message id is applied by default
                //Force reply is applied by default so it can work with privacy on
                $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
            }

            //Conversation start
            $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
            $notes = &$this->conversation->notes;
            !is_array($notes) && $notes = [];

            //cache data from the tracking session if any
            $state = 0;
            $state = 0;
            if (isset($notes['state'])) {
                $state = $notes['state'];
            }

            //in this state, if the student has pressed an inline button gets all his answers for the question selected and sends them with a message
            if ($state == 1) {
                if (isset($notes['question'])) {
                    $messages=Language::getChangeMessages($this->conversation->notes['language']);
                    $question = MYDB::getVQuestion($notes['question']);
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $data['text'] = $question['vquestion'];
                    $result = Request::sendMessage($data);
                    //it search an image and sends an image associated to the voice question
                    if ($question['image_path']!=NULL) {
                        $data['text'] = '';
                        $result = Request::sendPhoto($data,$question['image_path']);
                    }
                    $answers = MYDB::getVAnswersStudent( $notes['question'], $user_id);
                    $i=0;
                    while (isset($answers[$i])) {
                        $data['text'] = 'ID: '.$answers[$i]['id_file'];
                        //if the answer is selected it tells it in the message
                        if(strcmp($answers[$i]['selected'], 'T')==0){
                            $data['text'] .= $messages['selected'];
                        }
                        $result = Request::sendMessage($data);
                        $result = Request::sendVoice($data,$answers[$i]['file_path']);
                        $i++;
                    }
                    $data['text'] = $messages['getid'];
                    $result = Request::sendMessage($data);
                    $notes['state'] = 2;
                    $this->conversation->update();
                }
                else{
                    $state = 0;
                }
            }
            //this state checks the ID of the answer. If the ID isn't correct it tells it and if its correct it changes it
            if ($state==2) {
                $messages=Language::getChangeMessages($this->conversation->notes['language']);
                if (!ctype_digit($text)) {
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $data['text'] = $messages['id_not_found'];
                    $result = Request::sendMessage($data);
                    return;
                }
                $file=MYDB::getVAnswersExist( $text,$notes['question'], $user_id);
                if (!isset($file['id_file'])) {
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $data['text'] = $messages['id_not_found'];
                    $result = Request::sendMessage($data);
                }
                else{
                    MYDB::setVAnswerSelected( $text,$notes['question'], $user_id);
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $data['text'] = $messages['done'];
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                }
            }
            //checks if the user is registered to use the bot(see ../../Entities/Login.php), if he/she isn't registered goes to the catch at the bottom. If thats not the case it sends te inline keyboard with the available voice questions which the student can change the answer
            if ($state == 0) {
                if(!isset($notes['done'])){
                    $student=Login::userExist($user_id);
                    $language=MYDB::getLanguage($student['id_prof']);
                    $notes['language'] = $language['language'];
                    $notes['state'] = 1;
                    $this->conversation->update();
                    $inline_keyboard = $this->getInlineButtons($data,$user_id);
                }
            }
            return;
        }catch(TelegramException $e){
            //if the user isn't registered it sends a message telling it.
            $this->conversation->stop();
            $data['text'] = $e->getMessage();
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $result = Request::sendMessage($data);
            return $result;
        }
    }
}