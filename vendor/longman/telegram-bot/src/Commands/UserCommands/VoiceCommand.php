<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Commands\UserCommands;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Login;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Exception\TelegramException;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Audio\Vorbis;

class VoiceCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'voice';

    /**
     * @var string
     */
    protected $description = 'Comando para responder preguntas a base de un mensaje de voz';

    /**
     * @var string
     */
    protected $usage = '/voice';

    /**
     * @var string
     */
    protected $version = '0.1.3';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    /**
     * Sends text with the voice questions data and the inline buttons.
     *
     * @param array    $data        Data of the message.
     * @param string|int      $id_student  ID of the student.
     *
     * @return bool If there are inline buttons TRUE else FALSE.
     */
    private function getInlineButtons(array $data,$id_student){
        $questions = MYDB::getGroupQuestions($id_student);
        $messages=Language::getVoiceMessages($this->conversation->notes['language']);
        $i = 0;
        $j = 1;
        $buttons = [];
        $data['text'] = $messages['available_questions'];
        $result = Request::sendMessage($data);
        //it gets the available questions and create a message and the inline buttons
        while (isset($questions[$i])){
            $question=MYDB::getVQuestion($questions[$i]['id_vquestion']);
            //it checks if the deadline hasn't passed
            if(isset($question['deadline'])){
                //if the number of answers for that question hasn't exceed the maximum number of answers for that question
                $num_answers=MYDB::getVAnswersForQuestion( $questions[$i]['id_vquestion'], $id_student);
                if(isset($num_answers['answers']) && ((int)$num_answers['answers'])<(int)$question['max_answer']){
                    //it creates the message and the buttons
                    $data['text'] = ($j).'. ('.substr($question['deadline'], 0,10).') ('.$num_answers['answers'].') '.$question['vquestion'];
                    $result = Request::sendMessage($data);
                    $button=['text' => ($j).'',
                        'callback_data' => 'voice_'.$questions[$i]['id_vquestion']];
                    array_push($buttons, $button);
                    $j++;
                }
            }
            $i++;
        }
        //if theres no button it sends a message
    	if(!isset($buttons[0])){
    	    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $data['text'] = $messages['no_question'];
            $result = Request::sendMessage($data);
            $this->conversation->stop();
            return FALSE;
    	}
        //make rows of 3 inline buttons
        $inline_keyboard = new InlineKeyboard($buttons[0]);
        $i = 0;
        $row = [];
        while (isset($buttons[$i])) {
            array_push($row, $buttons[$i]);
            $mod = ($i + 1) % 3;
            if ($mod == 0) {
                $inline_keyboard->addRow($row[0],$row[1],$row[2]);
                $row=[];
            }
            $i++;
        }
        if(isset($row[1])){
            $inline_keyboard->addRow($row[0],$row[1]);
        }
        else if(isset($row[0])){
            $inline_keyboard->addRow($row[0]);
        }
        //send the inline keyboard with a message
        $data['text'] = $messages['question'];
        $data['reply_markup'] = $inline_keyboard;
        $result = Request::sendMessage($data);
        $this->conversation->update();
        return TRUE;
    }

    public function execute()
    {
        try{
            $message = $this->getMessage();
            $callback_query = $this->getUpdate()->getCallbackQuery();

            //if the command is called by the user by a message it takes the information from the message, else it takes from a callback(see ../SystemCommands/CallbackQuery.php)
            if ($message) {
                $chat = $message->getChat();
                $user = $message->getFrom();
                $text = trim($message->getText(true));
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }elseif ($callback_query) {
                $message = $callback_query->getMessage();
                $chat = $message->getChat();
                $user = $callback_query->getFrom();
                $text = $callback_query->getData();
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }

            $data = [
                'chat_id' => $chat_id,
            ];

            if ($chat->isGroupChat() || $chat->isSuperGroup()) {
                //reply to message id is applied by default
                //Force reply is applied by default so it can work with privacy on
                $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
            }

            //Conversation start
            $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
            $notes = &$this->conversation->notes;
            !is_array($notes) && $notes = [];

            //cache data from the tracking session if any
            $state = 0;
            if (isset($notes['question'])) {
                $state = 2;
            }

            //here starts the state machine. This state downloads the voice answer,converts the .oga file into .ogg file, creates the folders where the .ogg file will be stored and store all the data in the database
            if ($state == 2) {
                $ans=MYDB::getNumAnswerGiven( $user_id,$notes['question']);
                if (isset($ans['answers'])) {
                    $this->conversation->stop();
                    return;
                }
                $voice = $message->getVoice();
                if (isset($voice)) {
                    $messages=Language::getVoiceMessages($this->conversation->notes['language']);
                    $data['text'] = $messages['sec_pls'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    //this part is to get all the data to store it later in the database
                    $file_id = $voice->getFileId();
                    $duration = $voice->getDuration();
                    $student = MYDB::getStudent($user_id);
                    //this is the part of the download
                    $file_location = $this->telegram->getDownloadPath() .'/';
                    $download = Request::getFile(['file_id' => $file_id]);
                    if ($download->isOk() && Request::downloadFile($download->getResult())) {
                        $file_location .= $download->getResult()->getFilePath();
                    } else {
                        $data['text'] = $messages['no_download'];
                        $result = Request::sendMessage($data);
                        return $result;
                    }
                    //the next line stores the data of the answer in the database
                    MYDB::insertVAnswer( $user_id,$notes['question'],$file_location,$duration);
                    $data['text'] = $messages['download'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                }
                //if the message was not a voice message it goes to the state 1
                else{
                    $state=1;
                }
            }
            //this state sends the voice question selected by the student, but first it checks the deadline of the question and if the user hasn't gave the maxmimun of answer permited
            if ($state == 1) {
                $student=Login::userExist($user_id);
                $language=MYDB::getLanguage($student['id_prof']);
                $notes['language'] = $language['language'];
                $this->conversation->update();
                $messages=Language::getVoiceMessages($this->conversation->notes['language']);
                $question=MYDB::getVQuestion($notes['question']);
                //if all is correct it sends the question to the user and tells him/her to answer
                if (isset($question['deadline'])) {
                    $num_answers=MYDB::getVAnswersForQuestion( $notes['question'], $user_id);
                    if(isset($num_answers['answers']) && ((int)$num_answers['answers'])<(int)$question['max_answer']){
                        $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                        $data['text'] = $question['vquestion'];
                        $result = Request::sendMessage($data);
                        $data['text'] = $messages['pls_voice'];
                        $result = Request::sendMessage($data);
                        //it search an image and sends an image associated to the voice question
                        if ($question['image_path']!=NULL) {
                            $data['text'] = '';
                            $result = Request::sendPhoto($data,$question['image_path']);
                        }
                        $notes['state'] = 2;
                        $this->conversation->update();
                    }
                    //if the user has gave the maximun of answers it sends him/her a message telling it and ends the conversation
                    else{
                        $data['text'] = $messages['no_more_answer'];
                        $result = Request::sendMessage($data);
                        $this->conversation->stop();
                    }
                }
                //if the questions deadline has passed it tells it to the user and ends the conversation
                else{
                    $data['text'] = $messages['question_dead'];
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                }
            }
            //checks if the user is registered to use the bot(see ../../Entities/Login.php), if he/she isn't registered goes to the catch at the bottom. If thats not the case it sends te inline keyboard with the available voice questions
            if ($state == 0) {
                $student=Login::userExist($user_id);
                $language=MYDB::getLanguage($student['id_prof']);
                $notes['language'] = $language['language'];
                $this->conversation->update();
                $this->getInlineButtons($data,$user_id); 
            }
            return;
        }catch(TelegramException $e){
            //if the user isn't registered it sends a message telling it.
            $this->conversation->stop();
            $data['text'] = $e->getMessage();
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $result = Request::sendMessage($data);
            return $result;
        }
    }
}
