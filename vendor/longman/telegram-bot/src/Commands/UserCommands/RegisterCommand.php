<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Commands\UserCommands;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;

class RegisterCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'register';

    /**
     * @var string
     */
    protected $description = 'Registro para los estudiantes';

    /**
     * @var string
     */
    protected $usage = '/register';

    /**
     * @var string
     */
    protected $version = '0.1.1';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Show in Help
     *
     * @var bool
     */
    protected $show_in_help = false;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    /**
     * Sends text with the groups data and the buttons to select the group.
     *
     * @param array           $data        Data of the message.
     * @param string|int      $id_prof     ID of the students professor.
     *
     * @return bool If there are buttons TRUE else FALSE.
     */
    private function sendGroups(array $data,$id_prof){
        $groups = MYDB::getAllGroups($id_prof);
        $language=MYDB::getLanguage($id_prof);
        $this->conversation->notes['language'] = $language['language'];
        $this->conversation->update();
        $messages=Language::getRegisterMessages($language['language']);
        $i = 0;
        $buttons = [];
        $data['text'] = $messages['available_group'];
        $result = Request::sendMessage($data);
        //get the available groups, creates a message and creates the buttons
        while (isset($groups[$i])){
            $data['text'] = $groups[$i]['id_group'].'. '.$groups[$i]['group_name'];
            $result = Request::sendMessage($data);
            $button = $groups[$i]['id_group'];
            array_push($buttons, $button);
            $i++;
        }
        //if theres no button it sends a message
        if(!isset($buttons[0])){
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $data['text'] = $messages['no_group'];
            $result = Request::sendMessage($data);
            $this->conversation->stop();
            return FALSE;
        }
        //send the keyboard
        $data['text'] = $messages['group'];
        $data['reply_markup'] = (new Keyboard($buttons))->setResizeKeyboard(true)
                            ->setOneTimeKeyboard(true)
                            ->setSelective(false);
        $result = Request::sendMessage($data);
        return TRUE;
    }

    public function execute()
    {
        $message = $this->getMessage();
        $callback_query = $this->getUpdate()->getCallbackQuery();

        //if the command is called by the user by a message it takes the information from the message, else it takes from a callback(see ../SystemCommands/CallbackQuery.php)
        if ($message) {
            $chat = $message->getChat();
            $user = $message->getFrom();
            $text = trim($message->getText(true));
            $chat_id = $chat->getId();
            $user_id = $user->getId();
        }elseif ($callback_query) {
            $message = $callback_query->getMessage();
            $chat = $message->getChat();
            $user = $callback_query->getFrom();
            $text = $callback_query->getData();
            $chat_id = $chat->getId();
            $user_id = $user->getId();
        }

        $data = [
            'chat_id' => $chat_id,
        ];

        if ($chat->isGroupChat() || $chat->isSuperGroup()) {
            //reply to message id is applied by default
            //Force reply is applied by default so it can work with privacy on
            $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
        }

        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }
        //this state checks if the ID of the professor given in the message is correct(the ID is not the professors Telegram identifier).
        if ($state == 1) {
            $prof=MYDB::getProfGroups($text);
            if (isset($prof['id_prof'])) {
                $notes['state'] = 2;
                $this->conversation->update();
                return $this->sendGroups($data,$prof['id_prof']);
            }
            else{
                $state=0;
            }
        }
        //this state sends a message telling the student to write his/her professors identifier. If he/she was registered it tells him/her and stops the conversation.
        if ($state == 0) {
            $student=MYDB::getStudent( $user_id);
            if (isset($student['id_student'])) {
                $language=MYDB::getLanguage($student['id_prof']);
                $messages=Language::getRegisterMessages($language['language']);
                $data['text'] = $messages['registered'];
                $result = Request::sendMessage($data);
                $this->conversation->stop();
                return $result;
            }
            $language=MYDB::getLanguage();
            $messages=Language::getRegisterMessages($language['language']);
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $data['text'] = $messages['prof_id'];
            $result = Request::sendMessage($data);
            $notes['language'] = $language['language'];
            $notes['state'] = 1;
            $this->conversation->update();
        }
        //if all the data is correct this state stores it in the database and sends a message to tell the student that he/she is registered.
        if($state==2){
            if(is_numeric($text)){
                $group=MYDB::getGroup($text);
                if(isset($group['id_prof'])){
                    MYDB::insertStudent( $user_id, $text);
                    $messages=Language::getRegisterMessages($notes['language']);
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $data['text'] = $messages['ok'];
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                }
            }
        }
        return;
    }
}
