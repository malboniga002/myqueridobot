<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Commands\UserCommands;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Login;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Exception\TelegramException;

class EvalCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'eval';

    /**
     * @var string
     */
    protected $description = 'Comando para evaluar una respuesta de voz de un compañero';

    /**
     * @var string
     */
    protected $usage = '/eval';

    /**
     * @var string
     */
    protected $version = '0.1.5';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    /**
     * Converts the grade to numeric type.
     *
     * @param string    $text     Text with the grade.
     *
     * @return int|bool The numeric grade. If the text is not in the scale returs FALSE.
     */
    private function conversionF($text){
        $messages=Language::getEvalMessages($this->conversation->notes['language']);
    	if(strcmp($text, $messages['fatal'])==0){
    		return 0;
    	}
    	elseif(strcmp($text, $messages['very_bad'])==0){
    		return 2;
    	}
    	elseif(strcmp($text, $messages['bad'])==0){
    		return 4;
    	}
    	elseif(strcmp($text, $messages['regular'])==0){
    		return 5;
    	}
    	elseif(strcmp($text, $messages['good'])==0){
    		return 6;
    	}
    	elseif(strcmp($text, $messages['very_good'])==0){
    		return 8;
    	}
    	elseif(strcmp($text, $messages['excelent'])==0){
    		return 10;
    	}
    	else{
    		return FALSE;
    	}
    }

    /**
     * Converts the grade to numeric type.
     *
     * @param string    $text     Text with the grade.
     *
     * @return int|bool The numeric grade. If the text is not in the scale returs FALSE.
     */
    private function conversionS($text){
        $messages=Language::getEvalMessages($this->conversation->notes['language']);
    	if(strcmp($text, $messages['fail'])==0){
    		return 3;
    	}
    	elseif(strcmp($text, $messages['approved'])==0){
    		return 5;
    	}
    	elseif(strcmp($text, $messages['notable'])==0){
    		return 7;
    	}
    	elseif(strcmp($text, $messages['distinction'])==0){
    		return 10;
    	}
    	else{
    		return FALSE;
    	}
    }

    /**
     * Converts the grade to numeric type.
     *
     * @param string    $text     Text with the grade.
     *
     * @return int|bool The numeric grade. If the text is not in the scale returs FALSE.
     */
    private function conversionE($text){
    	if(strcmp($text, "\xE2\xAD\x90")==0){
    		return 2;
    	}
    	elseif(strcmp($text, "\xE2\xAD\x90\xE2\xAD\x90")==0){
    		return 4;
    	}
    	elseif(strcmp($text, "\xE2\xAD\x90\xE2\xAD\x90\xE2\xAD\x90")==0){
    		return 6;
    	}
    	elseif(strcmp($text, "\xE2\xAD\x90\xE2\xAD\x90\xE2\xAD\x90\xE2\xAD\x90")==0){
    		return 8;
    	}
    	elseif(strcmp($text, "\xE2\xAD\x90\xE2\xAD\x90\xE2\xAD\x90\xE2\xAD\x90\xE2\xAD\x90")==0){
    		return 10;
    	}
    	else{
    		return FALSE;
    	}
    }

    /**
     * Checks if the text is correct(its in the scale the professor has selected) and converts it to numeric type.
     *
     * @param string         $text     Text with the grade.
     * @param string|int     $scale    Scale that the professor has selected.
     *
     * @return int|bool The numeric grade. If the text is not in the scale returs FALSE.
     */
    private function answerCorrect($text,$scale){
    	if(strcmp($scale, '0-10')==0){
    		if(!ctype_digit($text)){
    			return FALSE;
    		}
    		$calification=(int)$text;
    		if($calification>10 || $calification<0){
    			return FALSE;
    		}
    		else{
    			return $calification;
    		}
    	}
    	elseif(strcmp($scale, '0-100')==0){
    		if(!ctype_digit($text)){
    			return FALSE;
    		}
    		$calification=(int)$text;
    		if($calification>100 || $calification<0){
    			return FALSE;
    		}
    		else{
    			$calification = round($calification/10);
    			return $calification;
    		}
    	}
    	elseif(strcmp($scale, 'fatal')==0){
    		return $this->conversionF($text);
    	}
    	elseif(strcmp($scale, 'fail')==0){
    		return $this->conversionS($text);
    	}
    	else{
    		return $this->conversionE($text);
    	}
    }

    /**
     * Indicates the scale to be used.
     *
     * @param string|int     $scale  Scale that the professor has selected.
     *
     * @return string A message which tells whats the scale the student have to use.
     */
    private function prepareScale($scale){
        $messages=Language::getEvalMessages($this->conversation->notes['language']);
    	if(strcmp($scale, '0-10')==0){
    		return $messages['1-10'];
    	}
    	elseif(strcmp($scale, '0-100')==0){
    		return $messages['1-100'];
    	}
    	elseif(strcmp($scale, 'fatal')==0){
    		return $messages['text_fatal'];
    	}
    	elseif(strcmp($scale, 'fail')==0){
    		return $messages['text_fail'];
    	}
    	else{
    		return $messages['text_stars'];
    	}

    }

    /**
     * Sends text with the evaluation data and the inline buttons.
     *
     * @param array    $data        Data of the message.
     * @param string|int      $id_student  ID of the student.
     *
     * @return bool If there are inline buttons TRUE else FALSE.
     */
    private function getInlineButtons(array $data,$id_student){
        $questions = MYDB::getAllGroupEvaluation($id_student);
        $messages=Language::getEvalMessages($this->conversation->notes['language']);
        $i = 0;
        $j = 1;
        $buttons = [];
        $data['text'] = $messages['avaliable_eval'];
        $result = Request::sendMessage($data);
        //this while search the posible evaluations for a student and creates a message with an ID, the ID of the criterion, the name of the student who is going to evaluate and the question and it creates a inline button
        while (isset($questions[$i])){
            //this search an answer to evaluate, if theres no answer it pass to the next evaluation
            $answered = MYDB::getVAnswerSelected( $questions[$i]['id_vquestion'], $questions[$i]['id_student_evaluated']);
            if (isset($answered['id_file'])) {
                //now it search if the student has done the evaluation, if it finds it it pass to the next evaluation
                $grade = MYDB::getEvaluation( $questions[$i]['id_eval']);
                if(!isset($grade['grade'])){
                    //and the last step is to search the information of the evaluated student(to search his name) and then it creates the message with the information and the button
                    $student = MYDB::getStudent( $questions[$i]['id_student_evaluated']);
                    $data['text'] = ($j).' .('.$questions[$i]['id_criterion'].'): '.$questions[$i]['vquestion'];
                    $result = Request::sendMessage($data);
                    //the button only has the ID and the callback has an identifier of the command, the ID of the question and the ID of the student who is going to be evaluated
                    $button = ['text' => ($j).'',
                        'callback_data' => 'eval_'.$questions[$i]['id_vquestion'].'_'.$questions[$i]['id_student_evaluated']];
                   	array_push($buttons, $button);
                    $j++;
               }
            }
            $i++;
        }
        //if theres no button it means that theres no posible evaluation for the student
    	if(!isset($buttons[0])){
    	    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $data['text'] = $messages['no_eval'];
            $result = Request::sendMessage($data);
            $this->conversation->stop();
            return FALSE;
    	}
        //make rows of 3 inline buttons
        $inline_keyboard = new InlineKeyboard($buttons[0]);
        $i = 0;
        $row = [];
        while (isset($buttons[$i])) {
            array_push($row, $buttons[$i]);
            $mod = ($i + 1) % 3;
            if ($mod == 0) {
                $inline_keyboard->addRow($row[0],$row[1],$row[2]);
                $row=[];
            }
            $i++;
        }
        if(isset($row[1])){
            $inline_keyboard->addRow($row[0],$row[1]);
        }
        else if(isset($row[0])){
            $inline_keyboard->addRow($row[0]);
        }
        //else it sends a message and the inline keyboard
        $data['text'] = $messages['eval'];
        $data['reply_markup'] = $inline_keyboard;
        $result = Request::sendMessage($data);
        $this->conversation->update();
        return TRUE;
    }

    public function execute()
    {
        try{
            $message = $this->getMessage();
            $callback_query = $this->getUpdate()->getCallbackQuery();
            //if the command is called by the user by a message it takes the information from the message, else it takes from a callback(see ../SystemCommands/CallbackQuery.php)
            if ($message) {
                $chat = $message->getChat();
                $user = $message->getFrom();
                $text = trim($message->getText(true));
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }elseif ($callback_query) {
                $message = $callback_query->getMessage();
                $chat = $message->getChat();
                $user = $callback_query->getFrom();
                $text = $callback_query->getData();
                $chat_id = $chat->getId();
                $user_id = $user->getId();
            }

            $data = [
                'chat_id' => $chat_id,
            ];

            if ($chat->isGroupChat() || $chat->isSuperGroup()) {
                //reply to message id is applied by default
                //Force reply is applied by default so it can work with privacy on
                $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
            }

            //Conversation start
            $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
            $notes = &$this->conversation->notes;
            !is_array($notes) && $notes = [];

            //cache data from the tracking session if any
            $state = 0;
            if (isset($notes['state'])) {
                $state = $notes['state'];
            }
            //this is the state machine, in the state 0 it checks if the user has press a inline button
            if ($state == 0) {
                //the first time it'll not enter in this if, this is to go to the next state
            	if (isset($notes['id_question'])) {
                	$state=1;
            	}
                //checks if the user is registered to use the bot(see ../../Entities/Login.php), if he/she isn't registered goes to the catch at the bottom
                else{
    	            $student=Login::userExist($user_id);
                    $language=MYDB::getLanguage($student['id_prof']);
                    $scale = MYDB::getOptions( $student['id_prof']);
                    $notes['language'] = $language['language'];
                    $notes['scale']= $scale['scale'];
                    $this->conversation->update();
    	            $this->getInlineButtons($data,$user_id);
            	}
            }
            //this state checks if the calification given in the message is correct, if it's correct it insert it in the database
            if ($state == 3) {
                if (FALSE!==$calification=$this->answerCorrect($text,$notes['scale'])) {
                    $eval=MYDB::getGroupEvaluation($user_id,$notes['id_student_evaluated'],$notes['id_question'],$notes['criterion']);
                    MYDB::insertVGrades( $eval['id_eval'], $notes['id_file'], $calification);
                    $state=2;
                }
                else{
                    $messages=Language::getEvalMessages($this->conversation->notes['language']);
                    $data['text'] = $messages['understand'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                }
            }
            //this states sends the selected answer of the student evaluated with the question.
            if ($state == 1) {
                $student=Login::userExist($user_id);
                $language=MYDB::getLanguage($student['id_prof']);
                $scale = MYDB::getOptions( $student['id_prof']);
                $notes['language'] = $language['language'];
                $notes['scale']= $scale['scale'];
                $this->conversation->update();
                $messages=Language::getEvalMessages($this->conversation->notes['language']);
                $question=MYDB::getVQuestionCriterion($notes['id_question']);
                $answer=MYDB::getVAnswerSelected( $notes['id_question'], $notes['id_student_evaluated']);
                $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                $data['text'] = $question['vquestion'].$messages['answer'];
                $result = Request::sendMessage($data);
                //it search an image and sends an image associated to the voice question
                if ($question['image_path']!=NULL) {
                    $data['text'] = '';
                    $result = Request::sendPhoto($data,$question['image_path']);
                }
                $data['text'] = '';
                $result = Request::sendVoice($data,$answer['file_path'] );
                $state = 2;
                $this->conversation->notes['id_file'] = $answer['id_file'];
            }
            //in this state it search for the criterions to evaluate. It will enter here while there's a criterion to evaluate for that answer.If theres no more criterion it sends the user to the next state
            if ($state == 2) {
            	$id_criterion = 0;
            	if(isset($notes['criterion'])){
            		$id_criterion = $notes['criterion'];
            	}
            	$eval = MYDB::getNextGroupEvaluation( $user_id,$notes['id_student_evaluated'],$notes['id_question'],$id_criterion);
                if (isset($eval['id_criterion'])) {
                    $criterion = MYDB::getCriterion($eval['id_criterion']);
                    $data['text'] = $criterion['criterion']."\n".$this->prepareScale($notes['scale']);
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $notes['state'] = 3;
                    $notes['criterion'] = $eval['id_criterion'];
                	$this->conversation->update();
                }
                else{
                    $state=4;
                    $text="";
                }
            }
            //this state gets the voice explanation of the user(in the voice file the user is going to explain why does he/she gave those califications to the voice answer he/she is evaluating)
            if ($state == 5) {
                //if theres a voice answer it saves it downloads it at the Download folder of the server, else it sends the user to the anterior state
                $messages=Language::getEvalMessages($this->conversation->notes['language']);
                $voice = $message->getVoice();
                if (isset($voice)) {
                    $data['text'] = $messages['sec_pls'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $file_id = $voice->getFileId();
                    //this is the part of the download
                    $file_location = $this->telegram->getDownloadPath() .'/';
                    $download = Request::getFile(['file_id' => $file_id]);
                    if ($download->isOk() && Request::downloadFile($download->getResult())) {
                        $file_location .= $download->getResult()->getFilePath();
                    } else {
                        $data['text'] = $messages['no_download'];
                        $result = Request::sendMessage($data);
                        return $result;
                    }
                    //the next line stores the data of the answer in the database
                    MYDB::insertVExplanation( $user_id,$notes['id_file'],$file_location);
                    $data['text'] = $messages['finish'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                }
                else if($text!=$messages["yes"] || $text!=""){
                    $state=4;
                    $text="";
                }
            }
            //in this state the user has two buttons to answer a question which ask him/her if he/she want to give a voice explanation for the califications given to the voice answer
            if ($state == 4) {
                $messages=Language::getEvalMessages($this->conversation->notes['language']);
                //if the answer isn't any of the text at the buttons it asks again the question
                if ($text!=$messages["yes"] && $text!=$messages["no"]) {
                    $data['text'] = $messages['voice_eval'];
                    $data['reply_markup'] = (new Keyboard([$messages['yes'],$messages['no']]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(false);
                    $result = Request::sendMessage($data);
                    $notes['state'] = 4;
                    $this->conversation->update();
                }
                //if the user presses the button yes it sends a message to explain what to do and sends the user to the next state
                elseif ($text==$messages["yes"]) {
                    $data['text'] = $messages['voice_answer'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $notes['state'] = 5;
                    $this->conversation->update();
                }
                //if the user presses the button no it ends the conversation and sends the final message
                elseif ($text==$messages["no"]) {
                    $messages=Language::getEvalMessages($this->conversation->notes['language']);
                    $data['text'] = $messages['finish'];
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    $this->conversation->stop();
                }
            }
            
            return;
        }catch(TelegramException $e){
            //if the user isn't registered it sends a message telling it.
            $this->conversation->stop();
            $data['text'] = $e->getMessage();
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $result = Request::sendMessage($data);
            return $result;
        }
    }
}