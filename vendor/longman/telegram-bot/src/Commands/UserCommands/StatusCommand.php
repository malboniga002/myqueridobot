<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Commands\UserCommands;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Login;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Entities\Keyboard;

class StatusCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'status';

    /**
     * @var string
     */
    protected $description = 'Muestra las estadísticas del usuario';

    /**
     * @var string
     */
    protected $usage = '/status';

    /**
     * @var string
     */
    protected $version = '0.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    public function execute()
    {
    	try{
	        $message = $this->getMessage();

	        $chat = $message->getChat();
	        $user = $message->getFrom();
	        $text = trim($message->getText(true));
	        $chat_id = $chat->getId();
	        $user_id = $user->getId();
            //if the user isn't registered he/she cant use the command
	        $student=Login::userExist($user_id);
            $language=MYDB::getLanguage($student['id_prof']);
            $messages=Language::getStatusMessages($language['language']);
            //this lines search the students statistics in the database and sends him a message telling him/her the number of right answers, the number of wrong answers and his/her accuracy.
	        $statistics=MYDB::getStatus($user_id);
            $data['chat_id'] = $chat_id;
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $data['text'] = $user->getFirstName().' '.$user->getLastName().$messages['right'].$statistics['right'].$messages['wrong'].$statistics['wrong'].$messages['accuracy'].$statistics['precision'];
	        $result = Request::sendMessage($data);
	        return $result;
    	}catch(TelegramException $e){
            $data['text'] = $e->getMessage();
            $data['reply_markup'] = Keyboard::remove(['selective' => true]);
            $result = Request::sendMessage($data);
            return $result;
    	}
    }
}