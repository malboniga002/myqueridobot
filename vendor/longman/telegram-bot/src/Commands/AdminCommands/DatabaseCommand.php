<?php

namespace Longman\TelegramBot\Commands\AdminCommands;

use Longman\TelegramBot\Commands\AdminCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Exception\TelegramException;


class DatabaseCommand extends AdminCommand
{
    /**
     * @var string
     */
    protected $name = 'database';

    /**
     * @var string
     */
    protected $description = 'Executes the query into the database';

    /**
     * @var string
     */
    protected $usage = '/database';

    /**
     * @var string
     */
    protected $version = '1.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Show in Help
     *
     * @var bool
     */
    protected $show_in_help = false;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    public function execute()
    {
        $message = $this->getMessage();
        $chat = $message->getChat();
        $user = $message->getFrom();
        $text = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();

        $data['chat_id'] = $chat_id;
        
        $data['reply_markup'] = Keyboard::remove(['selective' => true]);

        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        //this state ends a message telling to write a SQL query to execute it in the database
        if ($state == 0) {
            $language=MYDB::getLanguage($user_id);
            $notes['language'] = $language['language'];
            $this->conversation->update();
            $messages=Language::getDatabaseMessages($this->conversation->notes['language']);
            $data['text'] = $messages['query'];
            $result = Request::sendMessage($data);
            $notes['state'] = 1;
            $this->conversation->update();
        }
        //this states executes the query in the database. If its wrong it tells to write it again else it tells "done!"
        else {
            $messages=Language::getDatabaseMessages($this->conversation->notes['language']);
        	if(!MYDB::AdminC($text)){
            	$data['text'] = $messages['wrong'];
            	$result = Request::sendMessage($data);
           	}
           	else{
           		$data['text'] = $messages['done'];
            	$result = Request::sendMessage($data);
            	$this->conversation->stop();
           	}
        } 
        return $result; 
    }
}
