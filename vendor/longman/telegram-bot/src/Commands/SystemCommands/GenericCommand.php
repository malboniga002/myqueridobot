<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

/**
 * Generic command
 */
class GenericCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'Generic';

    /**
     * @var string
     */
    protected $description = 'Handles generic commands or is executed by default when a command is not found';

    /**
     * @var string
     */
    protected $version = '1.1.0';

    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();
        $callback_query = $this->getUpdate()->getCallbackQuery();
        
        //if the command is called by the user by a message it takes the information from the message, else it takes from a callback(see ../SystemCommands/CallbackQuery.php)
        if ($message) {
            $chat = $message->getChat();
            $user = $message->getFrom();
            $text = trim($message->getText(true));
            $chat_id = $chat->getId();
            $user_id = $user->getId();
        }elseif ($callback_query) {
            $message = $callback_query->getMessage();
            $chat = $message->getChat();
            $user = $callback_query->getFrom();
            $text = $callback_query->getData();
            $chat_id = $chat->getId();
            $user_id = $user->getId();
        }
        $command = $message->getCommand();

        //If the user is and admin and the command is in the format "/whoisXYZ", call the /whois command
        if (stripos($command, 'whois') === 0 && $this->telegram->isAdmin($user_id)) {
            return $this->telegram->executeCommand('whois');
        }

        $data = [
            'chat_id' => $chat_id,
            'text'    => 'Command /' . $command . ' not found.. :(',
        ];

        return Request::sendMessage($data);
    }
}
