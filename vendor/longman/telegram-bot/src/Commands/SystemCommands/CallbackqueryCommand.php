<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

/**
 * Callback query command
 */
class CallbackqueryCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'callbackquery';

    /**
     * @var string
     */
    protected $description = 'Reply to callback query';

    /**
     * @var string
     */
    protected $version = '1.1.0';

    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        //get the data to execute the command again
        $update            = $this->getUpdate();
        $callback_query    = $update->getCallbackQuery();
        $callback_query_id = $callback_query->getId();
        $callback_data     = $callback_query->getData();
        $user_id = $callback_query->getFrom()->getId();
        $chat_id = $callback_query->getMessage()->getChat()->getId();
        //this makes an array of the ID of the inline button. The first position in the array has an identifier of the command which has called the callback query
        $command = explode('_', $callback_data);

        //this are the diferent things that commands do, but all of them are similar, the only diference is the data and the command execution
        if(strcmp($command[0], "test") == 0){
            //gets the conversation of the command with the student and creates a note with information of the button
            $this->conversation = new Conversation($user_id, $chat_id, $command[0]);
            $this->conversation->notes['quiz'] = $command[1];
            $this->conversation->update();
            //at the end it executes the command which called the callback query
            return $this->getTelegram()->executeCommand($command[0], $update);
        }
        elseif(strcmp($command[0], "voice") == 0){
            $this->conversation = new Conversation($user_id, $chat_id, $command[0]);
            $this->conversation->notes['question'] = $command[1];
            $this->conversation->update();
            return $this->getTelegram()->executeCommand($command[0], $update);
        }
        elseif(strcmp($command[0], "change") == 0){
            $this->conversation = new Conversation($user_id, $chat_id, $command[0]);
            $this->conversation->notes['question'] = $command[1];
            $this->conversation->update();
            return $this->getTelegram()->executeCommand($command[0], $update);
        }
        elseif(strcmp($command[0], "eval") == 0){
            $this->conversation = new Conversation($user_id, $chat_id, $command[0]);
            $this->conversation->notes['id_question'] = $command[1];
            $this->conversation->notes['id_student_evaluated'] = $command[2];
            $this->conversation->update();
            return $this->getTelegram()->executeCommand($command[0], $update);
        }

    }
}
