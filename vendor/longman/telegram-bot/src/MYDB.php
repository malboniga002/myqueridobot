<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot;

use Longman\TelegramBot\DB;
use Longman\TelegramBot\Exception\TelegramException;
use PDO;
use PDOException;

class MYDB
{
	 /**
     * Method to get a question of a quiz.
     *
     * @param string|int     $quiz       ID of the quiz where we want to search the question.
     * @param string|int     $position   The position of the question.
     *
     * @return array|bool    The array with the data of the question.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getQuizQuestion( $quiz,  $position){
        try{
        	$pdo = DB::getPdo();
            $query='SELECT * FROM quiz_question WHERE id_quiz=:quiz ORDER BY id_question ASC';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':quiz', $quiz, PDO::PARAM_INT);
            $sth->execute();
            $row = $sth->fetchAll(PDO::FETCH_ASSOC);
            if (!isset($row[$position])) {
                return FALSE;
            }
            return $row[$position];
        }catch (PDOException $e) {
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get all the quizzes in the database.
     *
     * @return array|bool    The array with the data all the quizzes. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getQuizzes(){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT quiz.id_quiz,quiz.theme,quiz_question.id_quiz,COUNT(quiz_question.question) AS questions FROM quiz,quiz_question WHERE quiz_question.id_quiz=quiz.id_quiz GROUP BY quiz.id_quiz,quiz.theme';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the next quiz.
     *
     * @param string|int     $id_quiz       ID of the done quiz.
     *
     * @return array|bool    The array with the ID of the next quizz. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getnextQuiz($id_quiz){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT id_quiz  FROM quiz WHERE id_quiz>:id_quiz ORDER BY id_quiz ASC';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_quiz', $id_quiz, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to insert the data of an answer in the table answer_history.
     *
     * @param string|int     $id_question   ID of the question the student has answered.
     * @param string|int     $id_student    ID of the student who has answered the question.
     * @param string         $result        The result of the answer(right|wrong).
     *
     * @return bool     If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function insertAnswerHistory( $id_question,  $id_student,  $result){
        try{
        	$pdo = DB::getPdo();
            $query = 'INSERT INTO answer_history( id_question, id_student, result)
             VALUES( :id_question, :id_student, :result)';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_question', $id_question, PDO::PARAM_INT);
            $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
            $sth->bindParam(':result', $result, PDO::PARAM_STR);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the scale the teacher has selected.
     *
     * @param string|int     $id_prof       ID of the professor.
     *
     * @return array|bool    The array with the scale the teacher uses. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getOptions( $id_prof){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT scale FROM professor WHERE id_prof=:id_prof';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the data of a student.
     *
     * @param string|int     $id_student       ID of the student.
     *
     * @return array|bool    The array with the data of the student. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getStudent( $id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT student.*,student_group.id_prof FROM student,student_group WHERE student.id_student=:id_student AND student_group.id_group=student.id_group';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to insert a student.
     *
     * @param string|int     $id_student       ID of the student.
     * @param string|int     $id_group         ID of the group where the student want to be.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function insertStudent( $id_student, $id_group){
        try{
        	$pdo = DB::getPdo();
            $query = 'INSERT INTO student(id_student,id_group) 
            VALUES(:id_student,:id_group)';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
            $sth->bindParam(':id_group', $id_group, PDO::PARAM_INT);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the data of a group.
     *
     * @param string|int     $id_prof_group       ID of the professor for his/her groups.
     *
     * @return array|bool    Array with the ID of the professor.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getProfGroups( $id_prof_group){
        try{
        	$pdo = DB::getPdo();
        	$query = 'SELECT id_prof FROM professor WHERE id_group_selection=:id_prof_group';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_prof_group', $id_prof_group, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of all groups of a professor.
     *
     * @param string|int     $id_prof       ID of the professor.
     *
     * @return array|bool    Array with the data of the groups.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getAllGroups( $id_prof){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT * FROM student_group WHERE id_prof=:id_prof';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_prof', $id_prof, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of a group searching by ID.
     *
     * @param string|int     $id_group       ID of the group.
     *
     * @return array|bool    Array with the data of the group.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getGroup( $id_group){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT * FROM student_group WHERE id_group=:id_group';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_group', $id_group, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of the voice questions for a student.
     *
     * @param string|int     $id_student       ID of the student whos voice questions we are searching.
     *
     * @return array|bool    Array with the data of the voice questions.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getGroupQuestions($id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT * FROM group_question WHERE id_student=:id_student';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_student', $id_student, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of a criterion.
     *
     * @param string|int     $id_criterion       ID of the criterion we are searching.
     *
     * @return array|bool    Array with the data of the criterion.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getCriterion( $id_criterion){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT criterion FROM criterion WHERE id_criterion=:id_criterion';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_criterion', $id_criterion, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of a voice question where the deadline havent pass.
     *
     * @param string|int     $id_vquestion       ID of the voice question we are searching.
     *
     * @return array|bool    Array with the data of the voice question.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getVQuestion( $id_vquestion){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT vquestion,deadline,max_answer,image_path FROM voice_question WHERE id_vquestion=:id_vquestion AND deadline>=CURRENT_TIMESTAMP';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of a voice question.
     *
     * @param string|int     $id_vquestion       ID of the voice question we are searching.
     *
     * @return array|bool    Array with the data of the voice question.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getVQuestionCriterion( $id_vquestion){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT vquestion,deadline,image_path FROM voice_question WHERE id_vquestion=:id_vquestion';
            $sth = $pdo->prepare($query);
            $sth->bindParam(':id_vquestion', $id_vquestion, PDO::PARAM_INT);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of a voice answers to evaluate by a student.
     *
     * @param string|int     $id_student_evaluator       ID of the student who is going to evaluate the answers.
     *
     * @return array|bool    Array with the data of the voice question and evaluation.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getAllGroupEvaluation( $id_student_evaluator){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT group_evaluation.*,voice_question.vquestion FROM group_evaluation,voice_question WHERE group_evaluation.id_student_evaluator='.$id_student_evaluator.' AND group_evaluation.id_vquestion=voice_question.id_vquestion AND voice_question.deadline<CURRENT_TIMESTAMP';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data the evaluation, whith this we get the next criterion the student has to evaluate in the same answer.
     *
     * @param string|int     $id_student_evaluator       ID of the student who is going to evaluate the answers.
     * @param string|int     $id_student_evaluated       ID of the student whos answers are going to be evaluated.
     * @param string|int     $id_vquestion               ID of the question which answer is going to be evaluated.
     * @param string|int     $id_criterion               ID of the criterion which was evaluated(to search the next criterion).
     *
     * @return array|bool    Array with the data of the next evaluation.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getNextGroupEvaluation( $id_student_evaluator,$id_student_evaluated,$id_vquestion,$id_criterion){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT id_criterion FROM group_evaluation WHERE id_student_evaluator='.$id_student_evaluator.' AND id_student_evaluated='.$id_student_evaluated.' AND id_vquestion='.$id_vquestion.' AND id_criterion>'.$id_criterion.' ORDER BY id_criterion ASC';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of the evaluation.
     *
     * @param string|int     $id_student_evaluator       ID of the student who is going to evaluate the answers.
     * @param string|int     $id_student_evaluated       ID of the student whos answers are going to be evaluated.
     * @param string|int     $id_vquestion               ID of the question which answer is going to be evaluated.
     * @param string|int     $id_criterion               ID of the criterion which is going to be evaluated.
     *
     * @return array|bool    Array with the data of the evaluation.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getGroupEvaluation( $id_student_evaluator,$id_student_evaluated,$id_vquestion,$id_criterion){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT id_eval FROM group_evaluation WHERE id_student_evaluator='.$id_student_evaluator.' AND id_student_evaluated='.$id_student_evaluated.' AND id_vquestion='.$id_vquestion.' AND id_criterion='.$id_criterion;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get data of the evaluation(it is used to search if it exists).
     *
     * @param string|int     $id_eval       ID of the evaluation.
     *
     * @return array|bool    Array with the data of the evaluation.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getEvaluation( $id_eval){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT grade FROM voice_grades WHERE id_eval='.$id_eval;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the answer selected by a student as the correct answer for a voice question.
     *
     * @param string|int     $id_vquestion       ID of the voice question which was answered.
     * @param string|int     $id_student         ID of the student who answered the voice question.
     *
     * @return array|bool    Array with the data of the voice answer.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getVAnswerSelected( $id_vquestion, $id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT id_file,file_path FROM voice_answer WHERE id_vquestion='.$id_vquestion.' AND id_student='.$id_student.' AND selected="T"';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the answer selected by a student as the correct answer for a voice question.
     *
     * @param string|int     $id_vquestion       ID of the voice question which was answered.
     * @param string|int     $id_student         ID of the student who answered the voice question.
     *
     * @return array|bool    Array with the data of the voice answer.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getVAnswersForQuestion( $id_vquestion, $id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT count(*) as answers FROM voice_answer WHERE id_vquestion='.$id_vquestion.' AND id_student='.$id_student;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the ID of the voice answer to check if it exists.
     *
     * @param string|int     $id_file            ID of the voice file.
     * @param string|int     $id_vquestion       ID of the voice question which was answered.
     * @param string|int     $id_student         ID of the student who answered the voice question.
     *
     * @return array|bool    Array with the data of the ID.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getVAnswersExist( $id_file,$id_vquestion, $id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT id_file FROM voice_answer WHERE id_vquestion='.$id_vquestion.' AND id_student='.$id_student.' AND id_file='.$id_file;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to insert an answer for a voice question.
     *
     * @param string|int     $id_student       ID of the student who answered the voice question.
     * @param string|int     $id_vquestion     ID of the voice question which was answered.
     * @param string         $file_path        Path where the file is stored.
     * @param int            $duration         Duration of the file.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function insertVAnswer( $id_student,$id_vquestion,$file_path,$duration){
        try{
        	$pdo = DB::getPdo();
            $query = 'UPDATE voice_answer SET selected="F" WHERE id_student='.$id_student.' AND id_vquestion='.$id_vquestion.' AND selected="T"';
            $sth = $pdo->prepare($query);
            $sth->execute();
            $query = 'INSERT INTO voice_answer(id_student,id_vquestion, file_path,downloaded_at,duration,selected) VALUES('.$id_student.','.$id_vquestion.',"'.$file_path.'",CURRENT_TIMESTAMP,'.$duration.',"T")';
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get all the voice answers which a student made for a voice question.
     *
     * @param string|int     $id_vquestion       ID of the voice question.
     * @param string|int     $id_student         ID of the student who answered the voice question.
     *
     * @return array|bool    Array with the data of the voice answer.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getVAnswersStudent( $id_vquestion, $id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT id_file,file_path,selected FROM voice_answer WHERE id_vquestion='.$id_vquestion.' AND id_student='.$id_student;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to set the correct answer to a voice question(the student can create more than 1 answers for the same question but he has to select one which is going to be evaluated).
     *
     * @param string|int     $id_file            ID of the answer.
     * @param string|int     $id_vquestion       ID of the voice question.
     * @param string|int     $id_student         ID of the student who answered the voice question.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function setVAnswerSelected( $id_file,$id_vquestion, $id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'UPDATE voice_answer SET selected="F" WHERE id_student='.$id_student.' AND id_vquestion='.$id_vquestion.' AND selected="T"';
            $sth = $pdo->prepare($query);
            $sth->execute();
            $query = 'UPDATE voice_answer SET selected="T" WHERE id_file='.$id_file;
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to insert the grade of an evaluation.
     *
     * @param string|int    $id_eval     ID of the evaluation.
     * @param string|int    $id_file     ID of the answer.
     * @param string|int    $grade       Grade of the answer for the criterion.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function insertVGrades( $id_eval, $id_file, $grade){
        try{
        	$pdo = DB::getPdo();
            $query = 'INSERT INTO voice_grades(id_eval,id_file,grade) VALUES('.$id_eval.','.$id_file.','.$grade.')';
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the statistics of a student in quizzes.
     *
     * @param string|int     $id_student         ID of the student.
     *
     * @return array|bool    Array with the data of the statistics(number of right answers, number of wrong answers and the accuracy).If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getStatus( $id_student){
        try{
        	$pdo = DB::getPdo();
            $right = 0;
            $wrong = 0;
            $precision = 0;
            $query = 'SELECT id_student, result, COUNT(*) AS `c` FROM answer_history WHERE id_student='.$id_student.' GROUP BY id_student,result';
            $sth = $pdo->prepare($query);
            $sth->execute();
            $rows=$sth->fetchAll(PDO::FETCH_ASSOC);
            if(isset($rows[0])){
                if(strcmp($rows[0]['result'],'right')==0){
                    $right = intval($rows[0]['c']);
                    if(isset($rows[1])){
                        $wrong = intval($rows[1]['c']);
                    }
                }
                else{
                    $wrong = intval($rows[0]['c']);
                    if(isset($rows[1])){
                        $right = intval($rows[1]['c']);
                    }
                }
            }
            if($wrong + $right==0){
                $precision = 0.0;
            }
            else{
                $precision = $right / ($wrong + $right);
            }
            $precision= $precision*100;
            return array('right' => $right,'wrong' => $wrong, 'precision' => $precision);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to insert a gamer.
     *
     * @param string|int     $id_student           ID of the student.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function createGamer( $id_student){
        try{
        	$pdo = DB::getPdo();
        	$query = 'SELECT id_quiz,id_question FROM quiz_question ';
            $sth = $pdo->prepare($query);
            $sth->execute();
            $row=$sth->fetch(PDO::FETCH_ASSOC);
            $query = 'INSERT INTO gamer(id_student,id_quiz,id_question) VALUES('.$id_student.', '.$row['id_quiz'].','.$row['id_question'].')';
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to update a gamer.
     *
     * @param string|int     $id_student         ID of the student.
     * @param string|int     $posquestion        Position of the question in the quiz.
     * @param string         $answer_ok          The correct answer for the question.
     * @param string|int     $id_question        ID of the question.
     * @param string|int     $id_quiz            ID of the quiz.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function modifyGamer( $id_student,$posquestion=NULL,$answer_ok=NULL,$id_question=NULL, $id_quiz=NULL ){
        try{
        	$pdo = DB::getPdo();
        	$query = 'UPDATE gamer SET ';
            if (!is_null($id_quiz)) {
                $query .= 'id_quiz='.$id_quiz.',';
            }
            if (!is_null($id_question)) {
                $query .= 'id_question='.$id_question.',';
            }
            if (!is_null($answer_ok)) {
                $query .= " answer_ok='".$answer_ok."',";
            }
            if (!is_null($posquestion)) {
                $query .= 'num_question='.$posquestion.',';
            }
        $query = substr($query,0,(strlen($query)-1));
            $query .= ' WHERE id_student='.$id_student;
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to delete a gamer of a student.
     *
     * @param string|int     $id_student         ID of the student.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function deleteGamer( $id_student){
        try{
        	$pdo = DB::getPdo();
        	$query = 'DELETE FROM gamer WHERE id_student='.$id_student;
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the gamer of a student.
     *
     * @param string|int     $id_student         ID of the student.
     *
     * @return array|bool    Array with the data of the gamer.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getGamer($id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT * FROM gamer WHERE id_student='.$id_student;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to execute a query in the database.
     *
     * @param string    $query         SQL query.
     *
     * @return bool    If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function AdminC($query){
        try{
        	$pdo = DB::getPdo();
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the language option of the professor. If the ID in the parameter is null it gets the first(in the database) professors language option.
     *
     * @param string|int     $id_prof         ID of the professor of a student.
     *
     * @return array|bool    Array with the language.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getLanguage($id_prof=NULL){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT language FROM professor';
            if(!is_null($id_prof)){
                $query.=' WHERE id_prof='.$id_prof;
            }
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the the string wich explains why an answer of a quiz question is correct.
     *
     * @param string|int     $id_question         ID of the quiz question.
     *
     * @return array|bool    Array with the string.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getWhy($id_question){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT why FROM quiz_question WHERE id_question='.$id_question;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the the string wich explains why an answer of a quiz question is wrong.
     *
     * @param string|int     $id_question         ID of the quiz question.
     *
     * @return array|bool    Array with the string.If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getWhyNot($id_question){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT why_not FROM quiz_question WHERE id_question='.$id_question;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the option of randomize the teacher has selected.
     *
     * @param string|int     $id_student       ID of the student.
     *
     * @return array|bool    The array with the randomize option that the teacher uses. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getRandomize( $id_student){
        try{
        	$pdo = DB::getPdo();
            $query = 'SELECT professor.quiz_answer_random FROM professor,student_group,student WHERE student.id_student='.$id_student.' AND student_group.id_group=student.id_group AND professor.id_prof=student_group.id_prof';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the number of answers for a voice question to check that the student hasnt passed the limit of answers.
     *
     * @param string|int     $id_student       ID of the student.
     * @param string|int     $id_vquestion     ID of the voice question.
     *
     * @return array|bool    The array with count of answers given by the student. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getNumAnswerGiven( $id_student,$id_vquestion){
        try{
            $pdo = DB::getPdo();
            $query = 'SELECT count(voice_answer.id_file) as answers,voice_answer.id_student,voice_question.max_answer FROM voice_answer,voice_question WHERE voice_answer.id_student='.$id_student.' AND voice_answer.id_vquestion='.$id_vquestion.' AND voice_question.id_vquestion='.$id_vquestion.' GROUP BY voice_answer.id_student HAVING answers=voice_question.max_answer';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to insert students explanation after the evaluation of an answer.
     *
     * @param string|int     $id_student       ID of the student.
     * @param string|int     $id_file          ID of the evaluated file.
     * @param string         $file_path        Path to the file with the voice explanation of the student.
     *
     * @return bool   If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function insertVExplanation( $id_student,$id_file,$file_path){
        try{
            $pdo = DB::getPdo();
            $query = 'INSERT INTO voice_evaluation(id_student_evaluator,id_file,file_path) VALUES('.$id_student.','.$id_file.',"'.$file_path.'")';
            $sth = $pdo->prepare($query);
            return $sth->execute();
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get students answered questions and students selected answer for those questions.
     *
     * @param string|int     $id_student       ID of the student.
     *
     * @return array|bool    The array with questions and answers of the student. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getQuestion($id_student){
        try{
            $pdo = DB::getPdo();
            $query = 'SELECT voice_question.vquestion,voice_answer.id_vquestion FROM voice_grades,voice_answer,voice_question WHERE voice_answer.id_student='.$id_student.' AND voice_answer.id_vquestion=voice_question.id_vquestion AND voice_answer.selected="T" AND voice_question.deadline<CURRENT_TIMESTAMP AND voice_grades.id_file=voice_answer.id_file';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * Method to get the criterions for a voice question.
     *
     * @param string|int     $id_vquestion      ID of the voice question.
     *
     * @return array|bool    The array with ID's of the criterions to evaluate for the voice question. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getEvalCriterions($id_vquestion){
        try{
            $pdo = DB::getPdo();
            $query = 'SELECT id_criterion FROM group_evaluation WHERE id_vquestion='.$id_vquestion.' group by id_criterion';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * This method gets the array of grades of one of the students voice answer in a specific criterion.
     *
     * @param string|int     $id_vquestion              ID of the voice question.
     * @param string|int     $id_student_evaluated      ID of the student.
     * @param string|int     $id_criterion              ID of the criterion.
     *
     * @return array|bool    The array with grades for the voice answer in that criterion. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getGradesCriterion( $id_vquestion, $id_student_evaluated,$id_criterion){
        try{
            $pdo = DB::getPdo();
            $query = 'SELECT voice_grades.grade FROM voice_grades,group_evaluation where group_evaluation.id_vquestion='.$id_vquestion.' AND group_evaluation.id_criterion='.$id_criterion.' AND group_evaluation.id_student_evaluated='.$id_student_evaluated.' AND group_evaluation.id_eval=voice_grades.id_eval';
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }

    /**
     * This method gets voice explanations(the explanations of why the voice answer has those califications for the evaluated criterions).
     *
     * @param string|int     $id_file              ID of the voice answer.
     *
     * @return array|bool    The array with voice explanations for that voice answer. If there is any error it returns false.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public static function getExplanations($id_file){
        try{
            $pdo = DB::getPdo();
            $query = 'SELECT file_path FROM voice_evaluation WHERE id_file='.$id_file;
            $sth = $pdo->prepare($query);
            $sth->execute();
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw new TelegramException($e->getMessage());
        }
    }
}