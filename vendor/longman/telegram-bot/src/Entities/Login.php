<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Entities;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Exception\TelegramException;

class Login extends Entity
{
	/**
     * Checks if a user is registered as student. If he/she isn't registered it throws a exception.
     *
     * @param string|int       $id_student  The id of the user who we want to check if he/she is registered.
     *
     * @return array Returns the data of the student if he/she is registered.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
	public static function userExist($id_student){
		$student = MYDB::getStudent($id_student);
    	if(!isset($student['id_student'])){
            $language=MYDB::getLanguage();
            $messages=Language::getLoginMessages($language['language']);
    		throw new TelegramException($messages['login']);
    	}
    	return $student;
    }
}