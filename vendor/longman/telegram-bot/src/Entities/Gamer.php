<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Entities;
use Longman\TelegramBot\Entities\Login;
use Longman\TelegramBot\MYDB;
use Longman\TelegramBot\DB;
use Longman\TelegramBot\Entities\Language;
use Longman\TelegramBot\Exception\TelegramException;

class Gamer extends Entity
{
	/**
     * Gloval variables
     *
     * @var string|int 	$id_quiz 		ID of the quiz
     * @var string|int 	$id_question	ID of the current question
     * @var string|int 	$id_student		ID of the student
     * @var string|int 	$id_group		ID of the group where the student is
     * @var string|int 	$id_prof		ID of the professor of the student
     * @var string|int 	$posquestion	Position of the question
     * @var string 	    $correct_answer	The correct answer of the question
     */
	private $id_quiz;
	private $id_question;
	private $id_student;
	private $posquestion;
	private $correct_answer;

	/**
     * Constructor of the Gamer. If theres no gamer in the database of this user it creates it, else it search for the Gamer
     *
     * @param string|int    $id_student   ID of the student.
     */
	public function __construct($id_student)
    {
    	$this->id_student = $id_student;
    	$student = MYDB::getGamer($id_student);
    	if(!isset($student['id_student'])){
	    	$student = Login::userExist($id_student);
	    	$this->id_student = $student['id_student'];
	    	MYDB::createGamer($id_student);
	    }
	    else{
	    	$this->id_student = $student['id_student'];
	    	$this->id_quiz = $student['id_quiz'];
	    	$this->id_question = $student['id_question'];
	    	$this->posquestion = intval($student['num_question']);
	    	$this->correct_answer = $student['answer_ok'];
	    }
    }

    /**
     * Sets the quiz the student want to do and it modifies the gamer in the database.
     *
     * @param string|int    $id_quiz   ID of the quiz.
     */
	public function setQuiz( $id_quiz){
		$id=(int)$id_quiz;
		$this->id_quiz=$id;
		$question = MYDB::getQuizQuestion($id_quiz,'0');
		MYDB::modifyGamer( $this->getIdStudent(),'0',NULL,$question['id_question'], $id);
		$this->setQuestion();
	}

	/**
     * Sets the position of the question.
     *
     * @param string|int    $posquestion  Position of the question.
     */
	public function setQuestion($posquestion = 0){
		$this->posquestion = $posquestion;
	}

	/**
     * Returns the ID of the quiz stored in the global variable.
     *
     * @return string|int    ID of the quiz stored in the global variable.
     */
	public function getIdQuiz(){
		return $this->id_quiz;
	}

	/**
     * Returns the position of the question wich is stored in the global variable.
     *
     * @return string|int    The position of the question wich is stored in the global variable.
     */
	private function getQuestion(){
		return $this->posquestion;
	}

	/**
     * Returns the ID of the student stored in the global variable.
     *
     * @return string|int    ID of the student stored in the global variable.
     */
	private function getIdStudent(){
		return $this->id_student;
	}


	/**
     * Returns the ID of the question stored in the global variable.
     *
     * @return string|int    ID of the question stored in the global variable.
     */
	private function getIdQuestion(){
		return $this->id_question;
	}

	/**
     * Returns the correct answer stored in the global variable.
     *
     * @return string    The correct answer stored in the global variable.
     */
	private function getCorrectAnswer(){
		return $this->correct_answer;
	}

	/**
     * Get current question from the database and prepare the answers.
     *
     * @return array    Array with the question, all it's answers and the image path and the ID of the question.
     */
	public function getCurrentQuestion(){
		$question = MYDB::getQuizQuestion($this->getIdQuiz(),$this->getQuestion());
		if(!isset($question['id_question'])){
			return FALSE;
		}
		$random = MYDB::getRandomize($this->getIdStudent());
		if(strcmp($random['quiz_answer_random'], 'T')==0){
			$allanswers = $this->randomizeAnswers(
				array(
					$question['answer_ok'],
					$question['answer1'],
					$question['answer2'],
					$question['answer3']
				),
				$question['answer_ok']
			);
		}
		else{
			$allanswers = $this->dontRandomizeAnswers(
				array(
					$question['answer_ok'],
					$question['answer1'],
					$question['answer2'],
					$question['answer3']
				),$question['abcd']);
		}
		MYDB::modifyGamer( $this->getIdStudent(),$this->getQuestion(),$this->getCorrectAnswer(),$this->getIdQuestion());
		return array('id' => $question['id_question'],
			'question' => $question['question'],
			'answerA' => $allanswers[0],
			'answerB' => $allanswers[1],
			'answerC' => $allanswers[2],
			'answerD' => $allanswers[3],
			'image_path' => $question['image_path']
		);

	}

	/**
     * Prepares the answers. It sets the the correct answer as the character of the database.
     *
     * @param array    $answer           Array with the answers of a question.
     * @param string   $correct_answer   The correct answer in text.
     *
     * @return array    Array with the answers.
     */
	private function dontRandomizeAnswers(array $answer,$correct){
		$this->correct_answer = $correct;
		$answers = $answer;
		$answerA = 'A) ';
		$answerB = 'B) ';
		$answerC = 'C) ';
		$answerD = 'D) ';
		if (strcmp($correct, 'A')==0) {
			$answerA .= $answers[0];
			$answerB .= $answers[1];
			$answerC .= $answers[2];
			$answerD .= $answers[3];
		}
		elseif (strcmp($correct, 'B')==0) {
			$answerA .= $answers[1];
			$answerB .= $answers[0];
			$answerC .= $answers[2];
			$answerD .= $answers[3];
		}
		elseif (strcmp($correct, 'C')==0) {
			$answerA .= $answers[1];
			$answerB .= $answers[2];
			$answerC .= $answers[0];
			$answerD .= $answers[3];
		}
		else{
			$answerA .= $answers[3];
			$answerB .= $answers[1];
			$answerC .= $answers[2];
			$answerD .= $answers[0];
		}
		if ($answers[2]==NULL) {
			$answerC = NULL;
		}
		if ($answers[3]==NULL) {
			$answerD = NULL;
		}
		return array($answerA, $answerB, $answerC, $answerD);
	}

	/**
     * Randomize the answers. It sets the the correct answer as a character.
     *
     * @param array    $answer           Array with the answers of a question.
     * @param string   $correct_answer   The correct answer in text.
     *
     * @return array    Array with the answers.
     */
	private function randomizeAnswers(array $answer, $correct_answer){
		$answers = $answer;
		$answerA = 'A) ';
		$answerB = 'B) ';
		$answerC = 'C) ';
		$answerD = 'D) ';
		$rand1 = rand(0,3);
		$rand2 = rand(0,2);
		$rand3 = rand(0,1);

		//this ifs are made because a question can have 2-4 answers
		if($answers[3]!=NULL){
			//first it gets an answer of the array with a random position
			$answerA .= $answers[$rand1];
			//after that the selected answer is deleted of the array and the keys of the array are changed
			array_splice($answers, $rand1,1);
			$answerB .= $answers[$rand2];
			array_splice($answers, $rand2,1);
			$answerC .= $answers[$rand3];
			array_splice($answers, $rand3,1);
			$answerD .= $answers[0];
		}
		elseif ($answers[2]!=NULL) {
			$answerA .= $answers[$rand2];
			array_splice($answers, $rand2,1);
			$answerB .= $answers[$rand3];
			array_splice($answers, $rand3,1);
			$answerC .= $answers[0];
			$answerD = NULL;
		}
		else{
			$answerA .= $answers[$rand3];
			array_splice($answers, $rand3,1);
			$answerB .= $answers[0];
			$answerC = NULL;
			$answerD = NULL;
		}
		//at the end it search what is the correct answer and it sets in a global variable
		if (strcmp(substr($answerA, 3), $correct_answer) == 0) {
			$this->correct_answer = 'A';
		}
		elseif (strcmp(substr($answerB, 3), $correct_answer) == 0) {
			$this->correct_answer = 'B';
		}
		elseif (strcmp(substr($answerC, 3), $correct_answer) == 0) {
			$this->correct_answer = 'C';
		}
		else{
			$this->correct_answer = 'D'; 
		}

		return array($answerA, $answerB, $answerC, $answerD);
	}

	/**
     * Checks if the answer given by the student was correct and stores it in the database.
     *
     * @param string   $user_answer   Students answer.
     *
     * @return string    The message saying if the answer was right or wrong.
     */
	public function saveAnswer( $user_answer,$language){
		$right='';
		$messages=Language::getGamerMessages($language);
		if(strcmp($user_answer , $this->getCorrectAnswer()) == 0){
			MYDB::insertAnswerHistory(
				$this->getIdQuestion(),
				$this->getIdStudent(),
				'right');
			$right = $messages['right'];
		}
		else{
			MYDB::insertAnswerHistory(
				$this->getIdQuestion(),
				$this->getIdStudent(),
				'wrong');
			$right .= $messages['wrong'].$this->getCorrectAnswer();
		}
		$this->posquestion++;
		MYDB::modifyGamer( $this->getIdStudent(),$this->getQuestion(),$this->getCorrectAnswer(),$this->getIdQuestion());
		return $right;
	}

	/**
     * It deletes the gamer from the database.
     */
	public function finish(){
		MYDB::deleteGamer($this->getIdStudent());
		
	}

}
