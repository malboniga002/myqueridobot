<?php
/*Myqueridobot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.*/
namespace Longman\TelegramBot\Entities;

class Language extends Entity
{
	/**
     * Returns an array with the message of the start command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with the message.
     */
	public static function getStartMessages($language){
		include("Languages/".$language.".php");
    	return $start;
    }

    /**
     * Returns an array with the messages which are in login entity in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getLoginMessages($language){
		include("Languages/".$language.".php");
        return $login;
    }

	/**
     * Returns an array with all the messages which are in the gamer entity in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getGamerMessages($language){
		include("Languages/".$language.".php");
        return $gamer;
    }

	/**
     * Returns an array with all the messages which are in test command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getTestMessages($language){
		include("Languages/".$language.".php");
        return $test;
    }

	/**
     * Returns an array with all the messages which are in change command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getChangeMessages($language){
		include("Languages/".$language.".php");
        return $change;
    }

	/**
     * Returns an array with all the messages which are in database command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messagest.
     */
	public static function getDatabaseMessages($language){
    	include("Languages/".$language.".php");
        return $database;
    }
	
	/**
     * Returns an array with all the messages which are in voice command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getVoiceMessages($language){
    	include("Languages/".$language.".php");
        return $voice;
    }

	/**
     * Returns an array with all the messages which are in eval command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getEvalMessages($language){
    	include("Languages/".$language.".php");
        return $eval;
    }

	/**
     * Returns an array with all the messages which are in status command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getStatusMessages($language){
    	include("Languages/".$language.".php");
        return $status;
    }

	/**
     * Returns an array with all the messages which are in register command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
	public static function getRegisterMessages($language){
    	include("Languages/".$language.".php");
        return $register;
    }

    /**
     * Returns an array with all the messages which are in help command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
    public static function getHelpMessages($language){
        include("Languages/".$language.".php");
        return $help;
    }

    /**
     * Returns an array with all the messages for the notifications in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
    public static function getNotificationMessages($language){
        include("Languages/".$language.".php");
        return $notification;
    }

    /**
     * Returns an array with all the messages for the grades command in the language specified in the parameter.
     *
     * @param string       $language  The languaje the professor has et in his options.
     *
     * @return array Returns an array with all the messages.
     */
    public static function getGradesMessages($language){
        include("Languages/".$language.".php");
        return $grades;
    }
}