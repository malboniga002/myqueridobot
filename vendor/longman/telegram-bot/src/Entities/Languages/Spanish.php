<?php

$start = [
	'start' => "¡Hola!\xF0\x9F\x98\x81\n" . PHP_EOL . 
    "Soy un bot con el que gracias a mis tests y a las preguntas que recibirás eventualmente, te ayudaré a estudiar para que apruebes fácilmente tus asignaturas.\xF0\x9F\xA4\x93\n".
    "Esfuerzate un poco y junto con mi ayuda obtendrás muy buenas calificaciones. ¡Te lo prometo!\xF0\x9F\x98\x89".
    "Mandame el mensaje /help para ver mis comandos y empezar a familiarizarte con ellos después de haberte registrado.".
    " ¡Ánimo y mucha suerte!"
    ];

$login = [
	'login' => "Lo siento, pero no puedes usar mis comandos si no te has registrado."
	];

$gamer = [
	'right' => "\xF0\x9F\x98\x81 Correcto!",
	'wrong' => "\xF0\x9F\x98\x96 Incorrecto. La respuesta correcta es "
	];

$test = [
	'bye' => "Nos vemos\xE2\x98\xBA\xEF\xB8\x8F\nEscribe de nuevo /test cuando quieras volver a hacer un test.",
    'understand' => "\xF0\x9F\x98\xA3No te entiendo.\nPor favor, escoge uno de los botones para responder.\xF0\x9F\x98\x8A",
    'no_question' => "Ups, el siguiente test no tiene preguntas.\xF0\x9F\x98\xA8\n Escoge una de las siguientes opciones por favor.\xF0\x9F\x98\x8A",
    'no_quiz' => 'No hay tests disponibles.',
    'quiz' => 'Tests disponibles(tema y número de preguntas que tiene el tema):',
    'button_why' => '¿Por qué?',
    'button_why_not' => '¿Por qué no?',
    'button_next_quizz' => 'Siguiente test',
	'button_exit' => 'Salir',
    'fin' => "Finalizar",
	'button_repeat' => 'Repetir este test',
	'button_next_question'=>'Siguiente pregunta',
	'quiz_finish' => "El test ha terminado y ya no te quedan más por hacer. !Buen trabajo!\n¿Que quieres hacer ahora?\x09\xF0\x9F\xA4\x94",
	'no_more_quiz' => "El test ha terminado. !Buen trabajo!\n¿Qué quieres hacer ahora?\x09\xF0\x9F\xA4\x94"
    ];

$change = [
	'available_answers' => 'Preguntas disponibles para cambiar respuesta:',
    'no_answers' => 'No hay ninguna pregunta a la que se le pueda cambiar la respuesta. Las preguntas deben tener más de dos respuestas para poder cambiarlas.',
    'answer' => 'Pulsa el botón con el ID de la pregunta a la que le quieras cambiar la respuesta(los mensajes se componen de: ID, deadline, número de veces que has respondido y la pregunta):',
    'selected' => ' - Esta es tu respuesta seleccionada.',
    'getid' => 'Por favor, escribe el ID de la respuesta que quieras seleccionar:',
    'id_not_found' => 'El ID que has escrito no concuerda con ninguno de los que hay. Por favor, vuelve a escribir el ID de la respuesta que quieras seleccionar:',
    'done' => "¡Se ha cambiado correctamente de respuesta!\xF0\x9F\x98\x8A"
    ];

$database = [
	'query' => "Escribe la query:",
    'wrong' => "Algo has puesto mal.Escribe la query de nuevo:",
    'done' => "!Hecho!"
    ];

$voice = [
	'available_questions' => 'Preguntas disponibles:',
    'no_question' => 'No hay preguntas disponibles.',
    'question' => 'Pulsa el botón con el ID de la pregunta a la que le quieras responder (los mensajes se componen de: ID, deadline, número de veces que has respondido - máximo de veces que puedes responder y la pregunta):',
    'sec_pls' => 'Dame unos segundos para que guarde tu respuesta...',
    'no_download' => "\xF0\x9F\x98\x94No he podido guardar la respuesta, ¿Puedes volver a enviarmela?.",
    'download' => "!Listo! Gracias por tu repuesta.\xE2\x98\xBA\xEF\xB8\x8F",
    'pls_voice' => "\nPor favor, responde con un mensaje de voz.",
    'question_dead' => "Ha terminado el tiempo para responder a esta pregunta.\xF0\x9F\x98\x96",
    'no_more_answer' => "Has llegado al limite de respuestas a esta pregunta.\xF0\x9F\x98\x94"
    ];

$eval = [
	'fatal' => 'Fatal',
    'very_bad' => 'Muy mal',
    'bad' => 'Mal',
    'regular' => 'Regular',
    'good' => 'Bien',
    'very_good' => 'Muy bien',
    'excelent' => 'Excelente',
    'fail' => 'Suspenso',
    'approved' => 'Aprobado',
    'notable' => 'Notable',
    'distinction' => 'Sobresaliente',
    '1-10' => 'Por favor escribe un número del 0 al 10 para evaluar.',
    '1-100' => 'Por favor escribe un número del 0 al 100 para evaluar.',
    'text_fatal' => 'Por favor escribe uno de las siguientes expresiones para evaluar (el primer carácter en mayúscula): Fatal, Muy mal, Mal, Regular, Bien, Muy bien o Excelente.',
    'text_fail' => 'Por favor escribe uno de las siguientes expresiones para evaluar (el primer carácter en mayúscula): Suspenso, Aprobado, Notable o Sobresaliente.',
    'text_stars' => "Por favor escribe de 1 a 5 \xE2\xAD\x90 para evaluar.",
    'avaliable_eval' => 'Evaluaciones disponibles:',
    'no_eval' => 'No hay respuestas a evaluar.',
    'eval' => 'Selecciona el botón con el ID de la respuesta que quieres evaluar:',
    'understand' => "Lo siento, no te entiendo.\xF0\x9F\x98\xA3\nPor favor, evalúa según la escala indicada.",
    'answer' => "\nHe aquí la respuesta de tu compañero:",
    'finish' => "¡Has terminado!Gracias por tus repuestas.\xE2\x98\xBA\xEF\xB8\x8F",
    'yes'=>"Sí",
    'no'=>"No",
    'voice_eval'=>"¿Deseas grabar un mensaje de voz explicando por qué has dado esas puntuaciones en esta respuesta?\xF0\x9F\xA4\x94",
    'voice_answer'=>"Ok, enviame  una explicación de voz.\xF0\x9F\x98\x8A\nSi no estás seguro puedes escribir lo que quieras para que te vuelva a preguntar si quieres dar una explicacón de voz.",
    'sec_pls' => 'Dame unos segundos para que guarde tu explicación...',
    'no_download' => "\xF0\x9F\x98\x94No he podido guardar la explicación, ¿Puedes volver a enviarmela?.",
    ];

$status = [
	'right'=>":\nRespuestas correctas: ",
    'wrong'=>"\nRespuestas incorrectas: ",
    'accuracy'=>"\nPrecisión: "
    ];

$register = [
	'registered' => "Ya estabas registrado\xF0\x9F\x98\x85",
    'prof_id' => 'Por favor escribe el identificador de tu profesor:',
    'available_group' => 'Grupos disponibles:',
    'no_group' => 'No hay grupos disponibles.',
    'group' => 'Pulsa el botón con el ID del grupo en el que desees estar:',
    'ok' => "Te has registrado correctamente!\nYa tienes acceso a mis comandos. Escribe /help para verlos.\xF0\x9F\x98\x89"
    ];

$help = [
    'cancel' => "Cancela nuestra conversación mientras tienes un comando activo.",
    'change' => "Comando para cambiar tu respuesta de voz seleccionada en una pregunta.",
    'eval' => "Comando para evaluar a tus compañeros.",
    'help' => "Ayuda que muestra todos los comandos.",
    'status' => "Comando para mostrarte tus estadísticas generales de los tests.",
    'test' => "Comando para realizar los tests disponibles.",
    'voice' => "Comando para responder las preguntas de voz que tengas disponibles.",
    'whoami' => "Comando para saber quien eres.",
    'grades' => "Comando para saber tu calificación en una pregunta de voz."
    ];

$notification = [
    'voice' => "Tienes preguntas de voz por responder.",
    'eval' => "Tienes evaluaciones por hacer."
    ];

$grades = [
    'questions' => "Estas son las preguntas evaluadas:",
    'selec_id' => "Por favor, escribe el ID (ID.- Pregunta...) de la pregunta que deseas conocer tus calificaciones.",
    'no_id' => "ID incorrecto, por favor escribe de nuevo el ID.\xF0\x9F\x98\x96",
    'answer' => "Tu respuesta:",
    'notes' => "Tus calificiones son:",
    'end' => "Eso ha sido todo. Si quieres ver mas calificaciones enviame /grades.\xE2\x98\xBA\xEF\xB8\x8F",
    'no_question' => "No hay preguntas evaluadas.",
    'comments' => "He aquí los commentarios que tus compañeros han dado a tu respuesta:"
    ];