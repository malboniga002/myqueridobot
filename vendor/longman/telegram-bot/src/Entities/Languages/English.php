<?php

$start = [
	'start' => "¡Hi!\xF0\x9F\x98\x81\n" . PHP_EOL . 
    "I am a bot with which, thanks to my tests and the questions that you will eventually receive, I will help you to study so that you can easily pass your subjects.\xF0\x9F\xA4\x93\n".
    "Work a little and together with my help you will get very good grades. I promise!\xF0\x9F\x98\x89".
    "Send me the message /help to see my commands and start getting familiar with them after registering.".
    " Cheer up and good luck!"
    ];

$login = [
	'login' => "Sorry, but you can not use my commands if you aren't registered."
	];

$gamer = [
	'right' => "\xF0\x9F\x98\x81 Correct!",
	'wrong' => "\xF0\x9F\x98\x96 Wrong. The correct answer is "
	];

$test = [
	'bye' => "Bye\xE2\x98\xBA\xEF\xB8\x8F\nSend me /test again when you want to do another quiz.",
    'understand' => "\xF0\x9F\x98\xA3I can't understand you.\nPlease, press one of the buttons to answer.\xF0\x9F\x98\x8A",
    'no_question' => "Ups, the next quiz has no questions.\xF0\x9F\x98\xA8\n Select one of the next options please.\xF0\x9F\x98\x8A",
    'no_quiz' => 'No quiz available.',
    'quiz' => 'Available quizzes(theme and number of questions that the quiz has):',
    'button_why' => '¿Why?',
    'button_why_not' => '¿Why not?',
    'button_next_quizz' => 'Next quiz',
	'button_exit' => 'Exit',
    'fin' => "Finish",
	'button_repeat' => 'Repeat this quiz',
	'button_next_question' => 'Next question',
	'quiz_finish' => "The quiz is over and there's no more quiz to do. Good job!\nWhat do you want to do now?\x09\xF0\x9F\xA4\x94",
	'no_more_quiz' => "The quiz is over. Good job!\nWhat do you want to do now?\x09\xF0\x9F\xA4\x94"
    ];

$change = [
	'available_answers' => 'Available questions to change the answer:',
    'no_answers' => "There's no question to change the answer. Questions must have more than two answers to be able to change them.",
    'answer' => 'Press the button with the ID of the question you want to change the answer(the messages has: the ID, deadline, number of times you have answer that question and the question):',
    'selected' => ' - This is your selected answer.',
    'getid' => 'Please, write the ID of the answer which you want to select:',
    'id_not_found' => 'The ID you sent me is not correct. Please, rewrite the ID of the answer you want to select:',
    'done' => "You changed the selected answer correctly!\xF0\x9F\x98\x8A"
    ];

$database=[
	'query' => "Write the query:",
    'wrong' => "Something is wrong. Please, rewrite the query:",
    'done' => "Done!"
    ];

$voice = [
	'available_questions' => 'Available questions:',
    'no_question' => 'There is no available question.',
    'question' => 'Press the button with the ID of the question you want to answer (the message has: ID, deadline, number of times you answered the question - maximum number of times you can answer the question and the question):',
    'sec_pls' => 'Please, give me some seconds to save your answer...',
    'no_download' => "\xF0\x9F\x98\x94I could not save your answer, please send it again.",
    'download' => "Done! Thanks for your answer.\xE2\x98\xBA\xEF\xB8\x8F",
    'pls_voice' => "\nAnswer the question with a voice message please.",
    'question_dead' => "The time to answer this question is over.\xF0\x9F\x98\x96",
    'no_more_answer' => "You have reached the limit of answers to this question.\xF0\x9F\x98\x94"
    ];

$eval = [
	'fatal' => 'Fatal',
    'very_bad' => 'Very bad',
    'bad' => 'Bad',
    'regular' => 'Regular',
    'good' => 'Good',
    'very_good' => 'Very good',
    'excelent' => 'Excelent',
    'fail' => 'Fail',
    'approved' => 'Approved',
    'notable' => 'Notable',
    'distinction' => 'Distinction',
    '1-10' => 'Write a number from 0 to 10 to evaluate the answer please.',
    '1-100' => 'Write a number from 0 to 100 to evaluate the answer please.',
    'text_fatal' => 'Write one of the next expresions to evaluate the answer please (the first character in capital letter): Fatal, Very bad, Bad, Regular, Good, Very good or Excelent.',
    'text_fail' => 'Write one of the next expresions to evaluate the answer please (the first character in capital letter): Fail, Approved, Notable or Distinction.',
    'text_stars' => "Please, write form 1 to 5 \xE2\xAD\x90 to evaluate the answer.",
    'avaliable_eval' => 'Available evaluations:',
    'no_eval' => 'There is no answer to evaluate.',
    'eval' => 'Press the button with the ID of the answer you want to evaluate:',
    'understand' => "Sorry,  i can't unerstand you.\xF0\x9F\x98\xA3\nPlease, evaluate according to the indicated scale.",
    'answer' => "\nHere is your classmates answer:",
    'finish' => "¡You have finish! Thanks for your answers.\xE2\x98\xBA\xEF\xB8\x8F",
    'yes'=>"Yes",
    'no'=>"No",
    'voice_eval'=>"Do you want to give a voice explanation about why do you gave those grades to this voice answer?\xF0\x9F\xA4\x94",
    'voice_answer'=>"Ok, send me your voice explanation.\xF0\x9F\x98\x8A\nIf you aren't sure write something and i'll ask you if you want to give the voice answer again.",
    'sec_pls' => 'Please, give me some seconds to save your explanation...',
    'no_download' => "\xF0\x9F\x98\x94I could not save your explanation, please send it again.",
    ];

$status = [
	'right' => ":\nRight answers: ",
    'wrong' => "\nWrong answers: ",
    'accuracy' => "\nAccuracy: "
    ];

$register = [
	'registered' => "You were already registered\xF0\x9F\x98\x85",
    'prof_id' => 'Write your professors identifier:',
    'available_group' => 'Available groups:',
    'no_group' => 'There is no available group.',
    'group' => 'Press the button with the ID of the group where you want to be:',
    'ok' => "You are registered now!\nNow you have access to my commands. Send /help to see them.\xF0\x9F\x98\x89"
    ];

$help = [
    'cancel' => "This command cancels our conversation during another command.",
    'change' => "With this command you can change your selected answer of a voice question.",
    'eval' => "Command to evaluate your classmates.",
    'help' => "Show bot commands.",
    'status' => "Show your general statistics of the quizzes you've done.",
    'test' => "Command to do a quiz.",
    'voice' => "Command to answer the voice questions you have assigned.",
    'whoami' => "Command to know who you are.",
    'grades' => "Command to know your calification in a voice question."
    ];

$notification = [
    'voice' => "You have voice questions to answer.",
    'eval' => "You have voice answers to evaluate."
    ];

$grades = [
    'questions' => "This are the evaluated questions:",
    'selec_id' => "Please write de ID (ID.- Question...) of the question  which you want to know your grades.",
    'no_id' => "Incorrect ID, please write the ID again.\xF0\x9F\x98\x96",
    'answer' => "Your answer:",
    'notes' => "Your califications are:",
    'end' => "Thats all. If you want to see another questions calification send me /grades.\xE2\x98\xBA\xEF\xB8\x8F",
    'no_question' => "There's no evaluated question.",
    'comments' => "Here are some comments that your companions gave to your answer:"
    ];